/////////////////////////////////////////////////////////////////////////////
// Name:        containercollapsible.cpp
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     01/06/2011 16:25:48
// RCS-ID:      
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.    
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes

#include "widgets_base/containercollapsible.h"

////@begin XPM images
////@end XPM images

namespace widgets_base {
/*
 * ContainerCollapsible type definition
 */

IMPLEMENT_DYNAMIC_CLASS( ContainerCollapsible, wxGenericCollapsiblePane )


/*
 * ContainerCollapsible event table definition
 */

BEGIN_EVENT_TABLE( ContainerCollapsible, wxGenericCollapsiblePane )

////@begin ContainerCollapsible event table entries
    EVT_COLLAPSIBLEPANE_CHANGED( ID_COLLAPSIBLEPANE, ContainerCollapsible::OnCollapsiblepanePaneChanged )

////@end ContainerCollapsible event table entries

END_EVENT_TABLE()


/*
 * ContainerCollapsible constructors
 */

ContainerCollapsible::ContainerCollapsible()
{
    Init();
}

ContainerCollapsible::ContainerCollapsible(wxWindow* parent, wxWindowID id, const wxString& label, const wxPoint& pos, const wxSize& size, long style, const wxValidator& validator)
{
    Init();
    Create(parent, id, label, pos, size, style, validator);
}


/*
 * ContainerCollapsible creator
 */

bool ContainerCollapsible::Create(wxWindow* parent, wxWindowID id, const wxString& label, const wxPoint& pos, const wxSize& size, long style, const wxValidator& validator)
{
////@begin ContainerCollapsible creation
    wxGenericCollapsiblePane::Create(parent, id, label, pos, size, style, validator);
    CreateControls();
////@end ContainerCollapsible creation
    return true;
}


/*
 * ContainerCollapsible destructor
 */

ContainerCollapsible::~ContainerCollapsible()
{
////@begin ContainerCollapsible destruction
////@end ContainerCollapsible destruction
}


/*
 * Member initialisation
 */

void ContainerCollapsible::Init()
{
////@begin ContainerCollapsible member initialisation
////@end ContainerCollapsible member initialisation
}


/*
 * Control creation for ContainerCollapsible
 */

void ContainerCollapsible::CreateControls()
{    
////@begin ContainerCollapsible content construction
////@end ContainerCollapsible content construction
}


/*
 * Should we show tooltips?
 */

bool ContainerCollapsible::ShowToolTips()
{
    return true;
}

/*
 * Get bitmap resources
 */

wxBitmap ContainerCollapsible::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin ContainerCollapsible bitmap retrieval
    wxUnusedVar(name);
    return wxNullBitmap;
////@end ContainerCollapsible bitmap retrieval
}

/*
 * Get icon resources
 */

wxIcon ContainerCollapsible::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin ContainerCollapsible icon retrieval
    wxUnusedVar(name);
    return wxNullIcon;
////@end ContainerCollapsible icon retrieval
}


/*
 * wxEVT_COMMAND_COLLPANE_CHANGED event handler for ID_COLLAPSIBLEPANE
 */

void ContainerCollapsible::OnCollapsiblepanePaneChanged( wxCollapsiblePaneEvent& event )
{
	if (GetParent()) {
		wxSizeEvent ev;
		wxPostEvent (GetParent(), ev);
	}
    event.Skip(false);
}

}
