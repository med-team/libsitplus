/////////////////////////////////////////////////////////////////////////////
// Name:        wwiimotesconfiguration.h
// Author:      Cesar Mauri Loba (cesar at crea-si dot com)
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////

#ifndef _WWIIMOTESCONFIGURATION_H_
#define _WWIIMOTESCONFIGURATION_H_


/*!
 * Includes
 */

#include "mod_wiimotes.h"

////@begin includes
////@end includes
#include <wx/panel.h>
#include <wx/stattext.h>
#include <wx/choice.h>
#include <wx/checkbox.h>
#include <boost/thread/mutex.hpp>

namespace mod_wiimotes {

/*!
 * Forward declarations
 */

////@begin forward declarations
class Wiimotesproperties;
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_WIIMOTESCONFIGURATION 10060
#define ID_WIIMOTESPROPERTIES1 10006
#define ID_WIIMOTESPROPERTIES2 10000
#define ID_WIIMOTESPROPERTIES3 10001
#define ID_WIIMOTESPROPERTIES4 10002
#define ID_BUTTON_RECONNECT 10007
#define SYMBOL_WIIMOTESCONFIGURATION_STYLE wxCAPTION|wxTAB_TRAVERSAL
#define SYMBOL_WIIMOTESCONFIGURATION_TITLE _("Wiimotes Configuration")
#define SYMBOL_WIIMOTESCONFIGURATION_IDNAME ID_WIIMOTESCONFIGURATION
#define SYMBOL_WIIMOTESCONFIGURATION_SIZE wxDefaultSize
#define SYMBOL_WIIMOTESCONFIGURATION_POSITION wxDefaultPosition
////@end control identifiers
//#define SYMBOL_WIIMOTESCONFIGURATION_STYLE wxCAPTION|wxRESIZE_BORDER|wxSYSTEM_MENU|wxCLOSE_BOX|wxTAB_TRAVERSAL

/*!
 * WiimotesConfiguration class declaration
 */

class WiimotesConfiguration: public wxPanel, protected WiimoteListener
{    
    DECLARE_DYNAMIC_CLASS( WiimotesConfiguration )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    WiimotesConfiguration();
    WiimotesConfiguration( wxWindow* parent, wxWindowID id = SYMBOL_WIIMOTESCONFIGURATION_IDNAME, const wxPoint& pos = SYMBOL_WIIMOTESCONFIGURATION_POSITION, const wxSize& size = SYMBOL_WIIMOTESCONFIGURATION_SIZE, long style = SYMBOL_WIIMOTESCONFIGURATION_STYLE, const wxString& name= SYMBOL_WIIMOTESCONFIGURATION_TITLE );

    /// Creation
    bool Create( wxWindow* parent, wxWindowID id = SYMBOL_WIIMOTESCONFIGURATION_IDNAME, const wxPoint& pos = SYMBOL_WIIMOTESCONFIGURATION_POSITION, const wxSize& size = SYMBOL_WIIMOTESCONFIGURATION_SIZE, long style = SYMBOL_WIIMOTESCONFIGURATION_STYLE, const wxString& name= SYMBOL_WIIMOTESCONFIGURATION_TITLE );

    /// Destructor
    ~WiimotesConfiguration();

protected:
//	virtual void CameraCaptureCallback (SmartPtr<const mod_camera::CTypeIplImage> img);
	virtual void StatusNotification (const CTypeWiimotesStatus &);
	virtual void WiimoteNotification (wiimote *) {  /* void  */	};

private:
    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

////@begin WiimotesConfiguration event handler declarations

    /// wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BUTTON_RECONNECT
    void OnButtonReconnectClick( wxCommandEvent& event );

////@end WiimotesConfiguration event handler declarations

////@begin WiimotesConfiguration member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end WiimotesConfiguration member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

	void StatusNotificationGUI( wxCommandEvent& );

////@begin WiimotesConfiguration member variables
    Wiimotesproperties* m_panProperties_1;
    Wiimotesproperties* m_panProperties_2;
    Wiimotesproperties* m_panProperties_3;
    Wiimotesproperties* m_panProperties_4;
////@end WiimotesConfiguration member variables

	SmartPtr<CTypeWiimotesStatus> m_sharedStatus, m_privateStatus;
	boost::mutex m_mutex;
};

}

#endif
    // _WWIIMOTESCONFIGURATION_H_
