/**
* @file		basictypes.h
* @brief	Definition int, bool, float, string and composite core types.
* @author	Cesar Mauri Loba (cesar at crea-si dot com)
*
* -------------------------------------------------------------------------
*
* Copyright:   (C) 2010-11 Cesar Mauri Loba - CREA Software Systems
* 
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SPCORE_BASICTYPES_H
#define SPCORE_BASICTYPES_H

#include "spcore/basetypeimpl.h"
#include "spcore/coreruntime.h"
#include "spcore/module.h"
#include <string>
#include <vector>

namespace spcore {

//
// Basic types definitions
//

// int
class CTypeIntContents : public ScalarTypeContents<int> {
public:
	CTypeIntContents(int id) : ScalarTypeContents<int>(id) {}
	static inline const char* getTypeName() { return "int"; }
};

/**
	@brief Integer type for lib-sitplus.
	
	See class ScalarTypeContents and SimpleTypeBasicOperations for
	available operations.
*/
typedef SimpleType< CTypeIntContents > CTypeInt;


// float
class CTypeFloatContents : public ScalarTypeContents<float> {
public:
	CTypeFloatContents(int id) : ScalarTypeContents<float>(id) {}
	static inline const char* getTypeName() { return "float"; }
};

/**
	@brief Float type for lib-sitplus.
	
	See class ScalarTypeContents and SimpleTypeBasicOperations for
	available operations.
*/
typedef SimpleType< CTypeFloatContents > CTypeFloat;


// bool
class CTypeBoolContents : public ScalarTypeContents<bool> {
public:
	CTypeBoolContents(int id) : ScalarTypeContents<bool>(id) {}
	static inline const char* getTypeName() { return "bool"; }
};

/**
	@brief Boolean type for lib-sitplus.
	
	See class ScalarTypeContents and SimpleTypeBasicOperations for
	available operations.
*/
typedef SimpleType< CTypeBoolContents > CTypeBool;


// string
class CTypeStringContents : public CTypeAny
{
public:
	CTypeStringContents(int id) : CTypeAny(id) {}
	static inline const char* getTypeName() { return "string"; }
	virtual const char* getValue() const { return m_string.c_str(); }
	virtual void setValue(const char* s) {
		if (s== NULL) m_string.clear();
		else m_string= s; 
	}
	virtual bool CopyTo ( CTypeAny& dst, bool ) const {
		assert (dst.GetTypeID()== this->GetTypeID());
		sptype_static_cast<CTypeStringContents>(&dst)->m_string= m_string;
		return true;
	}
private:
	std::string m_string;
};

/**
	@brief String type for lib-sitplus.
	
	See class SimpleTypeBasicOperations for	additional operations.
*/
typedef spcore::SimpleType< CTypeStringContents > CTypeString;


// composite
class CTypeCompositeContents : public CompositeTypeAdapter
{
public:
	CTypeCompositeContents(int id) : CompositeTypeAdapter(id) {}
	static inline const char* getTypeName() { return "composite"; }

	// Does not provide a specialised CopyTo method because
	// this class has no data an so the base class method is enough
};

/**
	@brief Composite type for lib-sitplus.
	
	See class CompositeTypeAdapter and SimpleTypeBasicOperations for
	available operations.
*/
typedef spcore::SimpleType< CTypeCompositeContents > CTypeComposite;

//
// Module for basic types. Implemented in basictypesimpl.cpp
// FOR INTERNAL USE ONLY!
//
class CBasicTypesModule : public CModuleAdapter {
public:
    CBasicTypesModule();
    virtual const char * GetName() const;
};

} // namespace spcore
#endif
