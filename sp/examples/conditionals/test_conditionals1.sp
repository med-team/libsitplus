type test_conditionals1
name "test_contitionals1"

if "test" == "test"
	# should work fine
	create widget_slider int_sld --min -100 --max 100 -v 0 -l "Set int value" -i
	if "test" != "test"
		if "test" == "test"
			error
		endif
		error
	else
		create widget_slider int_sld2 --min -100 --max 100 -v 0 -l "THIS SHOULD NOT APPEAR" -i
	endif
endif

begin_gui_layout
	layout_begin vbox
		if "test" == "test"
			component int_sld
		endif

		if "test" != "test"
			component int_sld2
		endif
	layout_end
end_gui_layout