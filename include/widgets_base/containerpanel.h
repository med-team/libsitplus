/////////////////////////////////////////////////////////////////////////////
// Name:        wxcontainerpanel.h
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     10/05/2011 21:00:30
// RCS-ID:      
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
/////////////////////////////////////////////////////////////////////////////

#ifndef _WXCONTAINERPANEL_H_
#define _WXCONTAINERPANEL_H_


/*!
 * Includes
 */

////@begin includes
////@end includes
#include "widgets_base/libimpexp_widgetsbase.h"

/*!
 * Forward declarations
 */

////@begin forward declarations
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_CONTAINERPANEL 10004
#define SYMBOL_CONTAINERPANEL_STYLE wxTAB_TRAVERSAL
#define SYMBOL_CONTAINERPANEL_TITLE _("ContainerPanel")
#define SYMBOL_CONTAINERPANEL_IDNAME ID_CONTAINERPANEL
#define SYMBOL_CONTAINERPANEL_SIZE wxDefaultSize
#define SYMBOL_CONTAINERPANEL_POSITION wxDefaultPosition
////@end control identifiers


namespace widgets_base {

/*!
 * ContainerPanel class declaration
 */

class SPIMPEXP_CLASS_WIDGETSBASE ContainerPanel: public wxPanel
{    
    DECLARE_DYNAMIC_CLASS( ContainerPanel )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    ContainerPanel();
    ContainerPanel( wxWindow* parent, wxWindowID id = SYMBOL_CONTAINERPANEL_IDNAME, const wxPoint& pos = SYMBOL_CONTAINERPANEL_POSITION, const wxSize& size = SYMBOL_CONTAINERPANEL_SIZE, long style = SYMBOL_CONTAINERPANEL_STYLE, const wxString& caption = SYMBOL_CONTAINERPANEL_TITLE );

    /// Creation
    bool Create( wxWindow* parent, wxWindowID id = SYMBOL_CONTAINERPANEL_IDNAME, const wxPoint& pos = SYMBOL_CONTAINERPANEL_POSITION, const wxSize& size = SYMBOL_CONTAINERPANEL_SIZE, long style = SYMBOL_CONTAINERPANEL_STYLE, const wxString& caption = SYMBOL_CONTAINERPANEL_TITLE );

    /// Destructor
    ~ContainerPanel();

    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

////@begin ContainerPanel event handler declarations

    /// wxEVT_SIZE event handler for ID_CONTAINERPANEL
    void OnSize( wxSizeEvent& event );

////@end ContainerPanel event handler declarations

////@begin ContainerPanel member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end ContainerPanel member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin ContainerPanel member variables
////@end ContainerPanel member variables
};

};

#endif
    // _WXCONTAINERPANEL_H_
