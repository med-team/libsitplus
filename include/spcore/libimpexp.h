/**
* @file		libimpexp.h
* @brief	Preprocessor macros when bulding dynamic library.
* @author	Cesar Mauri Loba (cesar at crea-si dot com)
*
* -------------------------------------------------------------------------
*
* Copyright:   (C) 2010-11 Cesar Mauri Loba - CREA Software Systems
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef LIBEXPORTS_H
#define LIBEXPORTS_H

#ifdef WIN32
	#define SPEXPORT_FUNCTION extern "C" __declspec(dllexport)
	#define SPIMPORT_FUNCTION extern "C" __declspec(dllimport)
	#define SPEXPORT_CLASS __declspec(dllexport)
	#define SPIMPORT_CLASS __declspec(dllimport)
#else
	#define SPEXPORT_FUNCTION extern "C" 
	#define SPIMPORT_FUNCTION extern "C" 
	#define SPEXPORT_CLASS
	#define SPIMPORT_CLASS
#endif	// WIN32

////////////////////////

#ifdef SPMAKING_DLL_SPCORE
	#define SPIMPEXP_CLASS_CORE SPEXPORT_CLASS
#else
	#define SPIMPEXP_CLASS_CORE SPIMPORT_CLASS
#endif	// SPMAKING_DLL_SPCORE

#endif	// LIBEXPORTS_H
