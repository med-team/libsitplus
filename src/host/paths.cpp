/////////////////////////////////////////////////////////////////////////////
// File:        paths.cpp
// Author:      Cesar Mauri Loba (cesar at crea-si dot com)
// Copyright:   (C) 2010-12 Cesar Mauri Loba - CREA Software Systems
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////
#include "sphost/paths.h"
#include "config.h"
#include <string.h>
#include <string>
#ifdef WIN32
#include <windows.h>
#endif

#define BOOST_FILESYSTEM_VERSION 3
//#define BOOST_FILESYSTEM_NO_DEPRECATED 1
#include <boost/filesystem.hpp>
#include <boost/filesystem/exception.hpp>
using namespace boost::filesystem;

namespace sphost {

// Read the full path of the binary of the calling process
const std::string Paths::GetExecPath () {	
	std::string retPath;
	char buff[1024];	// Assume a reasonable buffer size

#ifdef WIN32
	DWORD retval= GetModuleFileNameA(NULL, buff, 1024);
	for(int i= 0; buff[i]; i++)
		if (buff[i]== '\\') buff[i]= '/';
	if (retval) retPath= buff;
#else
	// This code only works on linux...
	int retval= readlink ("/proc/self/exe", buff, 1023);
	if (retval!= -1) {
		buff[retval]= 0;
		retPath= buff;
	}
#endif
	return retPath;	
}

const std::string Paths::GetBasePathFromExec(const std::string& exePath, bool& devel) {
	devel= false;

	if (!exePath.empty())  {
		path p= exePath;
		path pp= p.parent_path ();
		path filename(pp.filename());
		if (!filename.string().compare("bin")) {
			return pp.parent_path().string();
		}
		else if (!filename.string().compare("Debug") || !filename.string().compare("Release")) {
			devel= true;
			return pp.parent_path().parent_path().string();
		}
	}

	// Return void result
	return exePath;
}

const std::string Paths::GetDirName(const std::string& fullPath) {
	if (!fullPath.empty()) {
		path p= fullPath;
		path pp= p.parent_path ();
		return pp.string();
	}

	// Return void result
	return fullPath;
}

const std::string Paths::GetUserDataDir() {
	std::string result;
#ifdef WIN32
	result= getenv("APPDATA");	
#else
	result= getenv("HOME");	
#endif
	return result;
}

} // namespace sphost

