/////////////////////////////////////////////////////////////////////////////
// Name:        test_mod_camera_viewer.cpp
// Author:      Cesar Mauri Loba
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////


#include "spcore/coreruntime.h"
#include "sphost/testcommon.h"
#include "nvwa/debug_new.h"
#include "mod_camera/roitype.h"

#if defined(__WXGTK__) || defined(__WXMOTIF__) || defined(__WXX11__)
	// Include Xlib for latter use on main
	#include <X11/Xlib.h>
#endif
#include <wx/app.h>
#include <wx/image.h>
#include <wx/msgdlg.h>
#include <wx/frame.h>
#include <wx/panel.h>
#include <wx/sizer.h>

using namespace spcore;
using namespace mod_camera;

/*
	Check threads enabled
*/
#if !wxUSE_THREADS
     #error "This program requires thread support."
#endif // wxUSE_THREADS

/*
	main app class
*/
class TestWXApp: public wxApp
{
	DECLARE_CLASS( TestWXApp )
public:
	TestWXApp();	// Constructor

private:
	virtual bool OnInit();	// Initialises the application
	virtual int OnExit();	// Called on exit

	SmartPtr<spcore::IComponent> m_comp;
};

/*
	main frame class
*/
class MyFrame : public wxFrame
{
public:
    MyFrame();
	void OnCloseWindow( wxCloseEvent& event );
	static MyFrame* CreateAddPanel ( spcore::IComponent& component );
private:
	void OnSize(wxSizeEvent& event);
    // any class wishing to process wxWidgets events must use this macro
    DECLARE_EVENT_TABLE()
};

/*
	myframe event table
*/
BEGIN_EVENT_TABLE(MyFrame, wxFrame)
	EVT_CLOSE( MyFrame::OnCloseWindow )
	EVT_SIZE ( MyFrame::OnSize)
END_EVENT_TABLE()

void MyFrame::OnCloseWindow(wxCloseEvent &event)
{
	event.Skip(); // Equivalent to: wxFrame::OnCloseWindow(event);
}

void MyFrame::OnSize(wxSizeEvent& event)
{
	Layout();
	Fit();

	event.Skip (false);
}


/*!
 * Application instance declaration
 */
DECLARE_APP(TestWXApp)

/*!
 * TestWXApp type definition
 */
IMPLEMENT_CLASS( TestWXApp, wxApp )

/*
	Application instance implementation

	we use IMPLEMENT_APP_NO_MAIN instead of IMPLEMENT_APP
	because we define our own main
 */
IMPLEMENT_APP_NO_MAIN(TestWXApp)

// ============================================================================
// implementation
// ============================================================================

// ----------------------------------------------------------------------------
// main frame
// ----------------------------------------------------------------------------
MyFrame::MyFrame()
: wxFrame(NULL, wxID_ANY, _T(""), wxDefaultPosition, wxDefaultSize, wxCAPTION|wxSYSTEM_MENU|wxCLOSE_BOX)
{

}

MyFrame* MyFrame::CreateAddPanel ( spcore::IComponent& component )
{
	MyFrame* mf= new MyFrame;
	wxBoxSizer* sizer= new wxBoxSizer(wxVERTICAL);
	mf->SetSizer(sizer);
	wxWindow* pan= component.GetGUI(mf);
	if (!pan) {
		delete mf;
		return NULL;
	}
	sizer->Add (static_cast<wxWindow*>(pan), 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 0);
	mf->GetSizer()->SetSizeHints(mf);	// Fit to content

	return mf;
}

// ----------------------------------------------------------------------------
// the application class
// ----------------------------------------------------------------------------

TestWXApp::TestWXApp() {
}

/*
  Initialisation for TestWXApp

  Return true to signal correct initialization or false when error
 */
bool TestWXApp::OnInit()
{
#if wxUSE_XPM
	wxImage::AddHandler(new wxXPMHandler);
#endif
#if wxUSE_LIBPNG
	wxImage::AddHandler(new wxPNGHandler);
#endif
#if wxUSE_LIBJPEG
	wxImage::AddHandler(new wxJPEGHandler);
#endif
#if wxUSE_GIF
	wxImage::AddHandler(new wxGIFHandler);
#endif

#if defined(WIN32)
	// Uncomment this to enable a console in Windows for debug purposes
	//AllocConsole(); freopen("CONOUT$", "wb", stdout);
#endif

	ICoreRuntime* cr= getSpCoreRuntime();

	int retval= cr->LoadModule("spmod_camera");
	DumpCoreRuntime(cr);
	if (retval!= 0)
		ExitErr("error loading mod_camera");

	// root_component
	m_comp= cr->CreateComponent ("component_composer", "root_component", 0, NULL);
	if (!m_comp.get()) ExitErr("error cannot create component");

	// camera_grabber
	SmartPtr<spcore::IComponent> grabber= cr->CreateComponent ("camera_grabber", "grabber", 0, NULL);
	if (!grabber.get()) ExitErr("error cannot create grabber component");
	retval= m_comp->AddChild(grabber);
	if (retval!= 0) ExitErr("error adding grabber");

	// camera_viewer
	SmartPtr<spcore::IComponent> viewer= cr->CreateComponent ("camera_viewer", "viewer", 0, NULL);
	if (!viewer.get()) ExitErr("error cannot create viewer component");
	retval= m_comp->AddChild(viewer);
	if (retval!= 0) ExitErr("error adding viewer");

	// roi_storage
	SmartPtr<spcore::IComponent> roi_storage= cr->CreateComponent ("roi_storage", "root_roi", 0, NULL);
	if (!roi_storage.get()) ExitErr("error cannot create roi_storage component");
	if (m_comp->AddChild(roi_storage)!= 0) ExitErr("error adding viewer");

	// Connect pins
	if (spcore::Connect (grabber.get(), "image", viewer.get(), "image")!= 0) ExitErr("cannot connect pins");
	if (spcore::Connect (roi_storage.get(), "roi", viewer.get(), "roi")!= 0) ExitErr("cannot connect pins");
	if (spcore::Connect (viewer.get(), "roi", roi_storage.get(), "roi")!= 0) ExitErr("cannot connect pins");

	SmartPtr<CTypeROI> roi_root= CTypeROI::CreateInstance();
	if (!roi_root.get()) ExitErr("cannot create ROI instance");


	roi_root->SetSize (0.8f, 0.8f);
	roi_root->SetCenter (0.5f, 0.5f);
	//roi_root->SetIsEditable (true);
	roi_root->SetIsVisible (true);
	//roi_root->SetUseDirection(true);
	roi_root->SetColor (0xFF0000);

	SmartPtr<CTypeROI> roi2= CTypeROI::CreateInstance();
	if (!roi2.get()) ExitErr("cannot create ROI instance");
	roi2->SetSize(0.5f, 0.5f);
	roi2->SetIsVisible(true);
	roi2->SetIsEditable(true);
	roi2->SetUseDirection(true);
	roi2->SetColor(0x00FF00);
	if (roi_root->AddChild(roi2)!= 0) ExitErr("cannot add child");

	SmartPtr<CTypeROI> roi3= CTypeROI::CreateInstance();
	if (!roi3.get()) ExitErr("cannot create ROI instance");
	roi3->SetSize(0.3f, 0.3f);
	roi3->SetIsVisible(true);
	roi3->SetIsEditable(true);
	roi3->SetUseDirection(false);
	roi3->SetColor(0x0000FF);
	if (roi_root->AddChild(roi3)!= 0) ExitErr("cannot add child");


	spcore::IInputPin* ipin= IComponent::FindInputPin(*roi_storage, "roi");
	if (!ipin) ExitErr("cannot find input pin");
	ipin->Send (roi_root);


	// Create dialog
	MyFrame* mf= MyFrame::CreateAddPanel ( *viewer );
	if (!mf) ExitErr("error error creating pannel");

	mf->Show();

	retval= m_comp->Initialize();
	if (retval!= 0) ExitErr("initialization failed");
	retval= m_comp->Start();
	if (retval!= 0) ExitErr("start root component failed");

	return true;
}


/*!
  Cleanup for TestWXApp
 */

int TestWXApp::OnExit()
{
	m_comp->Stop();
	m_comp->Finish();

	return wxApp::OnExit();
}

// ----------------------------------------------------------------------------
// main
// ----------------------------------------------------------------------------

int main(int argc, char *argv[]) {

#if defined(__WXGTK__) || defined(__WXMOTIF__) || defined(__WXX11__)
	// Under X11 it's necessary enable threading support
	if ( XInitThreads() == 0 ) {
		ExitErr("Unable to initialize multithreaded X11 code (XInitThreads failed)");
		exit( EXIT_FAILURE );
	}
#endif

	// Initialize spcore
	ICoreRuntime* cr= getSpCoreRuntime();
	if (!cr) return false;

	// Run wxWidgets message pump
	wxEntry(argc, argv);

	// Free spcore
	freeSpCoreRuntime();

	// If execution reaches this => all tests ok
	return 0;
}
