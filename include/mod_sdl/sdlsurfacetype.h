/////////////////////////////////////////////////////////////////////////////
// File:        sdlsurfacetype.h
// Author:      Cesar Mauri Loba (cesar at crea-si dot com)
// Copyright:   (C) 2010 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////
#ifndef SDLSURFACETYPE_H
#define SDLSURFACETYPE_H

#include "spcore/pin.h"
#include "spcore/pinimpl.h"
#include "spcore/coreruntime.h"
#include "spcore/basictypes.h"
#include <SDL.h>
#if defined(WIN32) && defined(ENABLE_WXWIDGETS)
#include <wx/msw/winundef.h>
#endif
#include <boost/static_assert.hpp>

#if defined(main)
#undef main
#endif


namespace mod_sdl {

/**
	Class that carries a SDL_Surface
*/

class CTypeSDLSurfaceContents : public spcore::CTypeAny {
public:
	static inline const char* getTypeName() { return "sdl_surface"; }
	virtual const SDL_Surface* getSurface() const { return m_surface; }
	
	virtual SDL_Surface* getSurface() { return m_surface; }
	virtual void setSurface(SDL_Surface* s) {
		// In principle setting the same pointer twice may 
		// point some kind of problem using this class
		assert (!s || s!= m_surface);
		// Anyway, in release mode handle it accordingly
		if (m_surface && s!= m_surface) SDL_FreeSurface(m_surface);
		m_surface= s; 
	}

	virtual void setX (short x) { m_x=x; }
	virtual short getX() const { return m_x; }
	
	virtual void setY (short y) { m_y=y; }
	virtual short getY() const { return m_y; }

protected:
	CTypeSDLSurfaceContents(int id) 
	: CTypeAny(id) 
	, m_x(0)
	, m_y(0)
	, m_surface(NULL)
	{}

	virtual ~CTypeSDLSurfaceContents() {
		if (m_surface) SDL_FreeSurface(m_surface);
		m_surface= NULL;
	}
private:
	short m_x, m_y;
	SDL_Surface* m_surface;
	BOOST_STATIC_ASSERT(sizeof(Sint16)== sizeof(short));
};

typedef spcore::SimpleType< CTypeSDLSurfaceContents > CTypeSDLSurface;

}
#endif
