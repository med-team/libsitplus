type testchoice
name "Test choice"

create widget_choice cho1 -l "Label 1" -o "First option|Second option|Third\"|\" option" 
create print print
connect cho1 selection print in
connect cho1 selection_string print in

create widget_choice cho2 -o "1st option|2nd option|3rd option" 
create print print2
connect cho2 selection print2 in
connect cho2 selection_string print2 in

connect cho1 selection cho2 selection
connect cho2 selection cho1 selection

create widget_choice cho3 -l "Label 3" -o "1st option|2nd option|3rd option" -v 2
create print print3
connect cho3 selection print3 in
connect cho3 selection_string print3 in


create widget_choice cho4 -l "Void"

begin_gui_layout
	layout_begin vbox
		component cho1
		component cho2
		component cho3
		component cho4
	layout_end
end_gui_layout

