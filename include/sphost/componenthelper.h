/**
* @file		componenthelper.h
* @brief	sp script parser.
* @author	Cesar Mauri Loba (cesar at crea-si dot com)
* 
* -------------------------------------------------------------------------
*
* Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef COMPONENTHELPER_H
#define COMPONENTHELPER_H

#include "spcore/component.h"
#include "sphost/libimpexp_sphost.h"

#include <exception>
#include <stdexcept>
#include <istream>

#ifdef _MSC_VER
#pragma warning(push)
// non dll-interface class 'A' used as base for dll-interface class 'B'
#pragma warning(disable: 4275)
#endif // _MSC_VER

namespace sphost {

//
// Classes to signal errors during script parsing
//

// Full parser error
class SPIMPEXP_CLASS_HOST parser_error : public std::runtime_error 
{
public:
	parser_error (const std::string& what_arg) : std::runtime_error (what_arg) {}
};

// Partial error. Used when not enough information is known to fully complete the error message
class SPIMPEXP_CLASS_HOST parser_partial_error : public std::runtime_error 
{
public:
	parser_partial_error (const std::string& what_arg) : std::runtime_error (what_arg) {}
};

/**
	This class is intended only to ease the task of working with components and so
	it is only intended to be used by the host application not the modules. Therefore,
	we export directly in the dynamic library.
*/
class SPIMPEXP_CLASS_HOST ComponentHelper {
public:
	/**
		Flags to control parsing behaviour
	*/
	enum { 
		IGNORE_CONNECT_SAME_COMPONENT= 1
	};

	/**
		@brief Parse an instantiate a .sp script from a stream
	
		@param is input stream to parse
		@param file actual file path (for error messages and import directive)
		@param dataDir value for SP_DATA_DIR 
		@param flags currently either 0 or ComponentHelper::IGNORE_CONNECT_SAME_COMPONENT
			(0 default)
		@param argc numbers of elements in array argv
		@param argv argument values passed to the script

		@return the created component

		If any error is found throws an parser_error exception

		See README.txt file on scripts directory for syntax details.
	**/
	static 
	SmartPtr<spcore::IComponent> SimpleParser (
		std::istream& is, 
		const std::string& file, 
		const char* dataDir, 
		int flags= 0,
		int argc= 0,
		const char *argv[]= NULL);

	/**
		@brief Parse an instantiate a .sp script from a file
	
		@param file path to the file 
		@param dataDir value for SP_DATA_DIR 
		@param flags currently either 0 or ComponentHelper::IGNORE_CONNECT_SAME_COMPONENT
			(0 default)
		@param argc numbers of elements in array argv
		@param argv argument values passed to the script

		@return the created component

		If any error is found throws an parser_error exception

		See README.txt file on scripts directory for syntax details.
	**/
	static 
	SmartPtr<spcore::IComponent> SimpleParserFile (
		const char* file, 
		const char* dataDir, 
		int flags= 0,
		int argc= 0,
		const char *argv[]= NULL);
	
	/**
		@brief Recursively find a component by name. 
		
		@param component: parent component where to search
		@param name: parent component where to search
		@param levels: maximum nesting deep. 0 means unlimited.
		@return pointer to the component or NULL if not found
	*/
	static
	spcore::IComponent* FindComponentByName (spcore::IComponent& component, const char* name, int deep);
};

} // namespace spcore

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#endif
