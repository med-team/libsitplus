/**
* @file		configutils.h
* @brief	Configuration utility functions.
* @author	Cesar Mauri Loba
*
* -------------------------------------------------------------------------
*
* Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
* 
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SPHOST_CONFIGUTILS_H
#define SPHOST_CONFIGUTILS_H

#include "spcore/configuration.h"
#include "sphost/libimpexp_sphost.h"

namespace sphost {

/**
	@brief Try to load a configuration set
	@param fname File name to load. If the path
	is relative the user data directory is appended.
	@return A smart pointer to the configuration which can be NULL if 
	the configuration file does not exist or an error ocurred.
*/
SPIMPEXP_CLASS_HOST
SmartPtr<spcore::IConfiguration> LoadConfiguration(const char* fname);

/**
	@brief Try to save a configuration set.
	@param cfg Reference to the configuration set.
	@param fname File name to load. If the path	is relative the user data 
	directory is appended. Missing directories are created as needed.
	@return true on success, false when an error ocurred.
*/
SPIMPEXP_CLASS_HOST
bool SaveConfiguration(const spcore::IConfiguration& cfg, const char* fname);

};

#endif
