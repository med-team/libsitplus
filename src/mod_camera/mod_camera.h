/////////////////////////////////////////////////////////////////////////////
// File:        mod_camera.h
// Author:      Cesar Mauri Loba (cesar at crea-si dot com)
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////
#ifndef MOD_CAMERA_H
#define MOD_CAMERA_H

#include "mod_camera/iplimagetype.h"
#include "spcore/component.h"
#include "creavision/crvimage.h"
#include "creavision/crvcamera.h"
#include "creavision/crvcamera_enum.h"

#include <string>
#include <boost/thread/thread.hpp>
#include <boost/thread/xtime.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>

namespace mod_camera {

/*
	camera_capture thread
*/

// camera capture listener
class CameraCaptureListener
{
public:
	virtual void CameraCaptureCallback (SmartPtr<const CTypeIplImage>)= 0;
};

// camera capture thread
class CameraCaptureThread
{
private:
	std::vector<CameraCaptureListener*> m_listeners;
	bool volatile m_Life;
	CCamera* volatile m_pCamera;

	bool volatile m_pause;
	boost::mutex m_pause_mutex;
	boost::condition_variable m_pause_changed;

	bool volatile m_pause2;
	boost::mutex m_pause_mutex2;
	boost::condition_variable m_pause_changed2;

public:
	CameraCaptureThread ()
	: m_Life(true)
	, m_pCamera(NULL)
	, m_pause(true)
	, m_pause2(true)
	{
	}

	~CameraCaptureThread () {
		Finish();
	}

	void Finish() {
		if (m_Life) {
			assert (!m_pCamera);
			m_Life= false;
			Pause(false);
		}
	}

	void Pause(bool pause) {
		{
			boost::unique_lock<boost::mutex> lock(m_pause_mutex);
			if (m_pause== pause) return;	// No change
			m_pause = pause;
		}
		if (!pause) {
			assert (m_pause2);
			m_pause2= false;
			m_pause_changed.notify_one();	// Remove pause
		}
		else {
			// Wait until is actually paused
			boost::unique_lock<boost::mutex> lock(m_pause_mutex2);
			while (!m_pause2) {
				m_pause_changed2.wait(lock);
			}
		}
	}

	bool IsPaused() { return m_pause; }

private:
	void BlockWhilePaused () {
		boost::unique_lock<boost::mutex> lock(m_pause_mutex);
		while(m_pause) {
			{
				boost::unique_lock<boost::mutex> lock(m_pause_mutex2);
				m_pause2= true;
			}			
			m_pause_changed2.notify_one();
			m_pause_changed.wait(lock);			
		}
	}

public:
	bool HasListeners() { return (m_listeners.size()> 0); }

	// Changes the camera
	void SetCamera (CCamera* new_cam) {
		assert (m_pause && m_pause2);
		if (new_cam== m_pCamera) return;
		m_pCamera= new_cam;
	}

	void RegisterListener (CameraCaptureListener& ccl) {
		assert (m_pause && m_pause2);

		// If listener not already registered add to the vector
		std::vector<CameraCaptureListener*>::iterator it=
			find(m_listeners.begin(), m_listeners.end(), &ccl);

		if (it== m_listeners.end())	m_listeners.push_back (&ccl);
	}

	void UnregisterListener (CameraCaptureListener& ccl) {
		assert (m_pause && m_pause2);

		std::vector<CameraCaptureListener*>::iterator it=
			find(m_listeners.begin(), m_listeners.end(), &ccl);

		if (it!= m_listeners.end())	m_listeners.erase (it);
	}

	// Thread entry point
	void Entry() {
		CIplImage image;

		// Start thread main loop
		for (;;) {
			BlockWhilePaused ();
			if (!m_Life) break;
			
			CCamera* camera= m_pCamera;
			if (!camera) {
				// No camera. should never happen.				
				assert(false);
				sleep_miliseconds( 200 );
				continue;
			}

			// Note that memory is automatically allocated
			// TODO: provide a smarter allocator to avoid continuous new's/delete's
			if (!camera->QueryFrame(image)) {
				// YUPS! error grabbing
				spcore::getSpCoreRuntime()->LogMessage (spcore::ICoreRuntime::LOG_ERROR, "error grabbing from camera.", "mod_camera");
				// Wait some amount of time... just in case
				sleep_miliseconds( 30 );
				continue;
			}

			SmartPtr<CTypeIplImage> spImage(CTypeIplImage::CreateInstance());
			spImage->setImage (image.Detach());

			// Call listeners
			std::vector<CameraCaptureListener*>::iterator it= m_listeners.begin();
			for(; it!= m_listeners.end(); ++it) (*it)->CameraCaptureCallback(spImage);
		}
	}

private:

	static void sleep_miliseconds( unsigned int ms )
	{
		boost::xtime xt;
		boost::xtime_get(&xt, boost::TIME_UTC);
		xt.nsec+= ms * 1000000;
		boost::thread::sleep(xt);
	}
};


/*
	camera_config component

	This is a singleton class that manages the camera configuration
	providing a dialog
*/


class CameraConfig : public spcore::CComponentAdapter {
private:
	unsigned int m_width, m_height;
	unsigned int m_fps;
	int m_selectedCamera;
	CCamera* m_pCamera;
	bool m_mirrorImage;
	CameraCaptureThread m_worker;
	boost::thread m_thread;

public:
	static const char* getTypeName() { return "camera_config"; };
	virtual const char* GetTypeName() const { return CameraConfig::getTypeName(); };

	CameraConfig(const char * name, int argc, const char * argv[])
	: CComponentAdapter(name, argc, argv)
	, m_width (320)
	, m_height (240)
	, m_fps(30)
	, m_selectedCamera(-1)
	, m_pCamera(NULL)
	, m_mirrorImage(true)
	, m_worker()
	, m_thread(&CameraCaptureThread::Entry, &m_worker)
	{
		assert (m_thread.joinable());
		RegisterInputPin (*SmartPtr<InputPinCameras>(new InputPinCameras(*this), false));
		RegisterInputPin (*SmartPtr<InputPinSelectedCamera>(new InputPinSelectedCamera(*this), false));
		RegisterInputPin (*SmartPtr<InputPinCaptureParameters>(new InputPinCaptureParameters(*this), false));
		RegisterInputPin (*SmartPtr<InputPinMirrorImage>(new InputPinMirrorImage(*this), false));
		RegisterInputPin (*SmartPtr<InputPinSettingDialog>(new InputPinSettingDialog(*this), false));

		// Set cam 0 as default (if exists)
		SetDesiredCam(0);
	}

private:
	~CameraConfig() {
		m_worker.Pause(true);
		m_worker.SetCamera(NULL);
		delete m_pCamera;		
		
		m_worker.Finish();
		m_thread.join();
	}

	int StartStopCameraWhenNeeded() {

		assert (m_worker.IsPaused());

		if (m_selectedCamera< 0) {
			// No camera currently selected, give out
			spcore::getSpCoreRuntime()->LogMessage (spcore::ICoreRuntime::LOG_ERROR,
				"No active camera available", "mod_camera");
			return -1;
		}

		// Is camera object created?
		if (!m_pCamera) {
			m_pCamera= CCameraEnum::GetCamera(m_selectedCamera, m_width, m_height, static_cast<float>(m_fps));
			if (!m_pCamera) {
				m_selectedCamera= 1;

				spcore::getSpCoreRuntime()->LogMessage (
					spcore::ICoreRuntime::LOG_ERROR, 
					"Cannot create camera.", "mod_camera");

				return -1;
			}
			m_pCamera->SetHorizontalFlip(m_mirrorImage);
			m_worker.SetCamera(m_pCamera);
		}

		
		if (m_worker.HasListeners()) {
			// Need to be started
			if (!m_pCamera->Open()) {
				// Cannot open camera
				delete m_pCamera;
				m_pCamera= NULL;
				m_selectedCamera= -1;
				spcore::getSpCoreRuntime()->LogMessage (
					spcore::ICoreRuntime::LOG_ERROR, 
					"Cannot open camera", "mod_camera");
				return -1;
			}

			// Camera successfully opened
			m_worker.Pause(false);
		}
		else {
			// Need to be stopped (already paused thread)
			m_pCamera->Close();
		}

		return 0;
	}

	int SetDesiredCam(int camNum) {
		if (camNum== m_selectedCamera) return 0;
		if (camNum< 0 || camNum>= CCameraEnum::GetNumDevices()) {
			spcore::getSpCoreRuntime()->LogMessage (
				spcore::ICoreRuntime::LOG_WARNING, 
				"Invalid camera number", "mod_camera");
			return -1;
		}

		// Try to create new camera
		CCamera* cam= CCameraEnum::GetCamera(camNum, m_width, m_height, static_cast<float>(m_fps));
		if (!cam) {
			spcore::getSpCoreRuntime()->LogMessage (
				spcore::ICoreRuntime::LOG_ERROR, 
				"Cannot create camera", "mod_camera");
			return -1;
		}
		cam->SetHorizontalFlip(m_mirrorImage);

		// OK. New camera created. Set as available camera.
		m_worker.Pause(true);
		delete m_pCamera;
		m_pCamera= cam;
		m_selectedCamera= camNum;
		m_worker.SetCamera(cam);
		
		return StartStopCameraWhenNeeded();
	}

	int SetCameraParameters (unsigned int width, unsigned int height, unsigned int fps, bool mirrorImage)
	{
		if (m_selectedCamera< 0) {
			// No camera currently selected, give out
			spcore::getSpCoreRuntime()->LogMessage (spcore::ICoreRuntime::LOG_ERROR,
				"No active camera available", "mod_camera");
			return -1;
		}

		assert (m_pCamera);

		if (width== m_width && height== m_height && fps== m_fps) {
			// only requested mirrorImage change (or nothing). do it and that's all.
			m_pCamera->SetHorizontalFlip (mirrorImage);
			m_mirrorImage= mirrorImage;
			return 0;
		}

		// Sanity checks
		if (width< 160 || width> 1280 || height< 120 || height> 720 || fps< 1 || fps> 30) {
			spcore::getSpCoreRuntime()->LogMessage (spcore::ICoreRuntime::LOG_WARNING,
				"Setting capture parameters, request ignored, invalid values", "mod_camera");
			return -1;
		}

		// Destroy current camera
		m_worker.Pause(true);
		m_pCamera->Close();
		delete m_pCamera;
		m_pCamera= NULL;

		// Keep old settings
		int oldSelectedCamera= m_selectedCamera;
		unsigned int oldWidth= m_width, oldHeight= m_height, oldFps= m_fps;
		bool oldMirrorImage= m_mirrorImage;

		// Set new settings			
		m_width= width;
		m_height= height;
		m_fps= fps;
		m_mirrorImage= mirrorImage;

		// Try to create & start the camera with the new configuration
		if (!StartStopCameraWhenNeeded()) return 0;

		// Didn't work. Restore previous settings and cross fingers...
		delete m_pCamera;
		m_pCamera= NULL;

		m_selectedCamera= oldSelectedCamera;
		m_width= oldWidth;
		m_height= oldHeight;
		m_fps= oldFps;
		m_mirrorImage= oldMirrorImage;

		return StartStopCameraWhenNeeded();
	}

public:
	void RegisterListener (CameraCaptureListener& ccl) {
		// TODO: insead of assert's way to signal globally that graph construction must be made within the main thread
		assert (spcore::getSpCoreRuntime()->IsMainThread());

		m_worker.Pause(true);
		m_worker.RegisterListener(ccl);
		StartStopCameraWhenNeeded();
	}

	void UnregisterListener (CameraCaptureListener& ccl) {
		assert (spcore::getSpCoreRuntime()->IsMainThread());

		m_worker.Pause(true);
		m_worker.UnregisterListener(ccl);
		StartStopCameraWhenNeeded();
	}

	virtual wxWindow* GetGUI(wxWindow * parent);

private:

	#define SELECTED_CAMERA_SETTING_NAME "selected_camera"
	#define WIDTH_SETTING_NAME "width"
	#define HEIGHT_SETTING_NAME "height"
	#define FPS_SETTING_NAME "fps"
	#define MIRROR_SETTING_NAME "mirror"

	virtual void SaveSettings(spcore::IConfiguration& cfg) {
		cfg.WriteInt(SELECTED_CAMERA_SETTING_NAME, m_selectedCamera);
		cfg.WriteInt(WIDTH_SETTING_NAME, m_width);
		cfg.WriteInt(HEIGHT_SETTING_NAME, m_height);
		cfg.WriteInt(FPS_SETTING_NAME, (int) m_fps);
		cfg.WriteBool(MIRROR_SETTING_NAME, m_mirrorImage);
	}

	virtual void LoadSettings(spcore::IConfiguration& cfg) {
		int selectedCamera;
		if (cfg.ReadInt(SELECTED_CAMERA_SETTING_NAME, &selectedCamera))
			SetDesiredCam(selectedCamera);

		int width, height, fps;
		bool mirror;

		if (!cfg.ReadInt(WIDTH_SETTING_NAME, &width)) return;
		if (!cfg.ReadInt(HEIGHT_SETTING_NAME, &height)) return;
		if (!cfg.ReadInt(FPS_SETTING_NAME, &fps)) return;
		if (!cfg.ReadBool(MIRROR_SETTING_NAME, &mirror)) return;
		SetCameraParameters ((unsigned int) width, (unsigned int) height, (unsigned int) fps, mirror);
	}

	void OpenCameraSettings ()
	{
		if (m_selectedCamera< 0) {
			// No camera currently selected, give out
			spcore::getSpCoreRuntime()->LogMessage (spcore::ICoreRuntime::LOG_ERROR,
				"no active camera available", "mod_camera");
			return;
		}

		assert (m_pCamera);

		if (!m_pCamera->HasSettingsDialog())
			spcore::getSpCoreRuntime()->LogMessage (spcore::ICoreRuntime::LOG_ERROR,
				"no settings dialog available", "mod_camera");
		else
			m_pCamera->ShowSettingsDialog();
	}

	

	// Read-only input pin to request available cameras
	class InputPinCameras : public spcore::CInputPinReadOnly<spcore::CTypeComposite, CameraConfig> {
	public:
		InputPinCameras (CameraConfig & component) : spcore::CInputPinReadOnly<spcore::CTypeComposite, CameraConfig>("cameras", component) {}
		virtual SmartPtr<spcore::CTypeComposite> DoRead() const {
			SmartPtr<spcore::CTypeComposite> retval= spcore::CTypeComposite::CreateInstance();

			int ndev= CCameraEnum::GetNumDevices();
			if (ndev< 1) {
				spcore::getSpCoreRuntime()->LogMessage (spcore::ICoreRuntime::LOG_WARNING, "not detected any camera", "mod_camera");
			}
			else {
				for (int i= 0; i< ndev; ++i) {
					SmartPtr<spcore::CTypeString> camname(spcore::CTypeString::CreateInstance());
					camname->setValue(CCameraEnum::GetDeviceName(i));
					retval->AddChild (camname);
				}
			}
			return retval;
		}
	};


	class InputPinSelectedCamera : public spcore::CInputPinReadWrite<spcore::CTypeInt, CameraConfig> {
	public:
		InputPinSelectedCamera (CameraConfig & component) : spcore::CInputPinReadWrite<spcore::CTypeInt, CameraConfig>("selected_camera", component) {}
		virtual int DoSend(const spcore::CTypeInt & message) {
			return m_component->SetDesiredCam(message.getValue());
		}
		virtual SmartPtr<spcore::CTypeInt> DoRead() const {
			SmartPtr<spcore::CTypeInt> retval= spcore::CTypeInt::CreateInstance();
			retval->setValue(m_component->m_selectedCamera);
			return retval;
		}
	};

	// The message is a composite with three values: width, height and fps
	class InputPinCaptureParameters : public spcore::CInputPinReadWrite<spcore::CTypeComposite, CameraConfig> {
	public:
		InputPinCaptureParameters (CameraConfig & component) : spcore::CInputPinReadWrite<spcore::CTypeComposite, CameraConfig>("capture_parameters", component) {}
		virtual int DoSend(const spcore::CTypeComposite & message) {
			const SmartPtr<spcore::IIterator<spcore::CTypeAny*> > it= message.QueryChildren();

			int width= -1, height= -1, fps= -1;
			for (int i= 0; !it->IsDone() && i< 3; it->Next(), ++i) {
				SmartPtr<spcore::CTypeInt> val= spcore::sptype_dynamic_cast<spcore::CTypeInt>(SmartPtr<spcore::CTypeAny>(it->CurrentItem()));
				if (!val.get()) {
					spcore::getSpCoreRuntime()->LogMessage (spcore::ICoreRuntime::LOG_WARNING, "setting capture parameters, request ignored, invalid message", "mod_camera");
					return -1;
				}

				switch(i) {
				case 0: width= val->getValue(); break;
				case 1: height= val->getValue(); break;
				case 2: fps= val->getValue(); break;
				default: assert (false);
				}
			}

			return m_component->SetCameraParameters (width, height, fps, m_component->m_mirrorImage);
		}
		virtual SmartPtr<spcore::CTypeComposite> DoRead() const {
			SmartPtr<spcore::CTypeComposite> retval= spcore::CTypeComposite::CreateInstance();
			SmartPtr<spcore::CTypeInt> width= spcore::CTypeInt::CreateInstance();
			SmartPtr<spcore::CTypeInt> height= spcore::CTypeInt::CreateInstance();
			SmartPtr<spcore::CTypeInt> fps= spcore::CTypeInt::CreateInstance();
			width->setValue(m_component->m_width);
			height->setValue(m_component->m_height);
			fps->setValue(m_component->m_fps);
			retval->AddChild (width);
			retval->AddChild (height);
			retval->AddChild (fps);
			return retval;
		}
	};

	class InputPinMirrorImage : public spcore::CInputPinReadWrite<spcore::CTypeBool, CameraConfig> {
	public:
		InputPinMirrorImage (CameraConfig & component) : spcore::CInputPinReadWrite<spcore::CTypeBool, CameraConfig>("mirror_image", component) {}
		virtual int DoSend(const spcore::CTypeBool & message) {
			return m_component->SetCameraParameters (m_component->m_width, m_component->m_height, m_component->m_fps, message.getValue());
		}
		virtual SmartPtr<spcore::CTypeBool> DoRead() const {
			SmartPtr<spcore::CTypeBool> retval= spcore::CTypeBool::CreateInstance();
			retval->setValue(m_component->m_mirrorImage);
			return retval;
		}
	};

	// Write-only to open camera settings dialog (if available)
	class InputPinSettingDialog : public spcore::CInputPinWriteOnly<spcore::CTypeAny, CameraConfig> {
	public:
		InputPinSettingDialog (CameraConfig & component) : spcore::CInputPinWriteOnly<spcore::CTypeAny, CameraConfig>("settings_dialog", component) {}
		virtual int DoSend(const spcore::CTypeAny &) {
			m_component->OpenCameraSettings ();
			return 0;
		}
	};
};

};

#endif
