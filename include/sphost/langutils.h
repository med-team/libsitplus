/**
* @file		langutils.h
* @brief	i18n utility functions.
* @author	Cesar Mauri Loba
*
* -------------------------------------------------------------------------
*
* Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
* 
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SPHOST_LANGUTILS_H
#define SPHOST_LANGUTILS_H

#include "config.h"

#ifdef ENABLE_NLS

#include <string>

#include "sphost/libimpexp_sphost.h"

namespace sphost {

/**
	@brief Adds a language to the list of supported languages for this application
	@param lang Language identifier
	@return false if an error ocurred (invalid language id), true otherwise.
*/
SPIMPEXP_CLASS_HOST
bool AddAvailableLanguage(int lang);

/**
	@brief Open a language selection dialog.
	@param previous Language identifier to be selected in the dialog or -1 if none.
	@return the selected language id or -1 if user cancelled.
	@remarks wx must has been previously initialized.
*/
SPIMPEXP_CLASS_HOST
int LanguageSelectionDialog(int previuos);

/**
	@brief Try to load the stored language setting if possible.
	@param fname Language configuration path
	@return the laguage id or
		-1 error reading language from config file (not found).
		-2 invalid language code.
*/
SPIMPEXP_CLASS_HOST
int LoadLanguageFromConfiguration(const char* fname);

/*!
	@brief Save the language setting
	@param lang Language identifier
	@param fname Language configuration path
	@return 0 if OK
		-1 error writting language to config file.
		-2 invalid language id
*/
SPIMPEXP_CLASS_HOST
int SaveLanguageToConfiguration(int lang, const char* fname);

};

#endif	// ENABLE_NLS

#endif	// SPHOST_LANGUTILS_H