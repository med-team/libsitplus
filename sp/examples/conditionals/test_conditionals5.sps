type test_conditionals5s
name "test_conditionals5s"
args -a -b -c -d

create print print $-a$ $-b$ $-c$ $-d$

if $-a$ == $-b$
	create print print2 "OK 1"
else
	create print print2 "KO 1"
endif

if $-c$ == $-d$
	create print print3 "KO 2"
else
	create print print3 "OK 2"
endif
