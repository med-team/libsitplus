/////////////////////////////////////////////////////////////////////////////
// Name:        mdichildcontainer.h
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     04/04/2011 21:30:07
// RCS-ID:      
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.     
/////////////////////////////////////////////////////////////////////////////

#ifndef _MDICHILDCONTAINER_H_
#define _MDICHILDCONTAINER_H_


/*!
 * Includes
 */

////@begin includes
#include "wx/mdi.h"
////@end includes
#include "widgets_base/libimpexp_widgetsbase.h"

/*!
 * Forward declarations
 */

////@begin forward declarations
class wxBoxSizer;
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_MDICHILDCONTAINER 10012
#define SYMBOL_MDICHILDCONTAINER_STYLE wxCAPTION
#define SYMBOL_MDICHILDCONTAINER_TITLE wxEmptyString
#define SYMBOL_MDICHILDCONTAINER_IDNAME ID_MDICHILDCONTAINER
#define SYMBOL_MDICHILDCONTAINER_SIZE wxDefaultSize
#define SYMBOL_MDICHILDCONTAINER_POSITION wxDefaultPosition
////@end control identifiers

namespace widgets_base {

/*!
 * MDIChildContainer class declaration
 */

class SPIMPEXP_CLASS_WIDGETSBASE MDIChildContainer: public wxMDIChildFrame
{    
    DECLARE_CLASS( MDIChildContainer )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    MDIChildContainer();
    MDIChildContainer( wxMDIParentFrame* parent, wxWindowID id = SYMBOL_MDICHILDCONTAINER_IDNAME, const wxString& caption = SYMBOL_MDICHILDCONTAINER_TITLE, const wxPoint& pos = SYMBOL_MDICHILDCONTAINER_POSITION, const wxSize& size = SYMBOL_MDICHILDCONTAINER_SIZE, long style = SYMBOL_MDICHILDCONTAINER_STYLE );

    bool Create( wxMDIParentFrame* parent, wxWindowID id = SYMBOL_MDICHILDCONTAINER_IDNAME, const wxString& caption = SYMBOL_MDICHILDCONTAINER_TITLE, const wxPoint& pos = SYMBOL_MDICHILDCONTAINER_POSITION, const wxSize& size = SYMBOL_MDICHILDCONTAINER_SIZE, long style = SYMBOL_MDICHILDCONTAINER_STYLE );

    /// Destructor
    ~MDIChildContainer();

	// Adds a panels from a component
	void AddSitplusPanel (wxWindow* panel);

private:
    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

////@begin MDIChildContainer event handler declarations

    /// wxEVT_SIZE event handler for ID_MDICHILDCONTAINER
    void OnSize( wxSizeEvent& event );

////@end MDIChildContainer event handler declarations

////@begin MDIChildContainer member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end MDIChildContainer member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin MDIChildContainer member variables
    wxBoxSizer* m_sizer;
////@end MDIChildContainer member variables
};

};

#endif
    // _MDICHILDCONTAINER_H_
