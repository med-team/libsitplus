type test_filebrowser
name "test_filebrowser"

create file_browser dirs -p $SP_DATA_DIR$ -t d
create widget_choice slddirs_full -l "Full path"
create widget_choice slddirs_names -l "File names"
connect dirs paths slddirs_full options
connect dirs files slddirs_names options

create file_browser files -p $SP_DATA_DIR$ -t a
create widget_choice sldfiles_full -l "Full path"
create widget_choice sldfiles_names -l "File names"
connect files paths sldfiles_full options
connect files files sldfiles_names options

create widget_filepicker pick -t d -l "Pick a directory" -v $SP_DATA_DIR$
connect pick value dirs path
connect pick value files path

create widget_checkbox chk -l "Press to refresh"
connect chk value dirs refresh
connect chk value files refresh

begin_gui_layout
	layout_begin vbox
		component pick
		component chk
		layout_begin hbox
			layout_begin vbox "Directories"
				component slddirs_full
				component slddirs_names
			layout_end
			layout_begin vbox "Files"
				component sldfiles_full
				component sldfiles_names
			layout_end
		layout_end
	layout_end
end_gui_layout

