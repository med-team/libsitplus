# Update the source file dependencies of the pot file.
#
# This globs all files h and cpp in the src and include directories 
#
# Remove the old input file.
# Dummy target with a non existing (and not created file) is always executed.
add_custom_command(
	OUTPUT ${PROJECT_SOURCE_DIR}/po/${DOMAIN}/POTFILES.in.dummy
	# remove the old file.
	COMMAND ${CMAKE_COMMAND} 
			-E remove ${PROJECT_SOURCE_DIR}/po/${DOMAIN}/POTFILES.in
	COMMENT "pot-update [${DOMAIN}]: Removed existing POTFILES.in."
)

add_custom_command(
	OUTPUT ${PROJECT_SOURCE_DIR}/po/${DOMAIN}/POTFILES.in

	# Create an empty new one, to be sure it will exist.
	COMMAND ${CMAKE_COMMAND} 
			-E touch ${PROJECT_SOURCE_DIR}/po/${DOMAIN}/POTFILES.in

	# Find all h and cpp files
	COMMAND find src -name '*h' -print -o -name '*cpp' -print |
			sort |
			while read file\; do
				echo $$file >> 
					${PROJECT_SOURCE_DIR}/po/${DOMAIN}/POTFILES.in \;
			done
	COMMAND find include -name '*h' -print |
			sort |
			while read file\; do
				echo $$file >> 
					${PROJECT_SOURCE_DIR}/po/${DOMAIN}/POTFILES.in \;
			done

	DEPENDS ${PROJECT_SOURCE_DIR}/po/${DOMAIN}/POTFILES.in.dummy
	WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
	COMMENT 
		"pot-update [${DOMAIN}]: Created POTFILES.in for ${DOMAIN} domain."
)
