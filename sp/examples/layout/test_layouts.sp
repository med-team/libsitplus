type test_layout
name "test_layout"

create widget_checkbox chk1 -l "Boolean option" -v true
create widget_checkbox chk2 -l "Another checkbox" -v true

create widget_filepicker pick1 -l "Pick file" 

create widget_slider sld1 -l "Adjustment 1"
create widget_slider sld2 -l "Adjustment 2"
create widget_slider sld3 -l "Adjustment 3"
create widget_slider sld4 -l "Adjustment 4"

create camera_viewer cam1


begin_gui_layout
	layout_begin hbox "All options"
		# Left panel
		layout_begin vbox "Left panel"
			component cam1
			component sld1
			layout_begin collapsible "Superadvanced"
				component chk1
				component sld2
			layout_end			
		layout_end
		# Right panel
		layout_begin vbox "Right panel"
			layout_begin book
				layout_begin book_page "Page 1"
					component sld3
					component pick1
				layout_end
				layout_begin book_page "Page 2"
					component sld4
					component chk2
				layout_end
			layout_end
		layout_end
	layout_end
end_gui_layout