#include "sphost/testcommon.h"
#include "spcore/coreruntime.h"
#include "mod_sdl/sdlsurfacetype.h"

using namespace spcore;

static void DoTests()
{
	// Initialization
	spcore::ICoreRuntime* cr= spcore::getSpCoreRuntime();
	if (!cr) ExitErr("spcore::getSpCoreRuntime(); failed");

	int retval= cr->LoadModule ("mod_sdl");
	if (retval< 0) {
		std::cerr << "Cannot load shared library. Error code: " << retval << std::endl;
		exit(-1);
	}

	DumpCoreRuntime(cr);

	// Check symbols

	SmartPtr<IComponent> compo= cr->CreateComponent("sdl_drawer", "my_drawer", 0, NULL);
	if (!compo.get()) ExitErr("sdl_drawer component not found");

	SmartPtr<CTypeAny> sur= cr->CreateTypeInstance("sdl_surface");
	if (!sur.get()) ExitErr("sdl_surface component not found");


}

int main (int argc, char* argv[])
{
	DoTests();

	// Cleanup
	spcore::freeSpCoreRuntime();

	return 0;
}
