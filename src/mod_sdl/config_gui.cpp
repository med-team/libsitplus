/////////////////////////////////////////////////////////////////////////////
// Name:        sdlconfiguration.cpp
// Purpose:
// Author:      C�sar Mauri Loba
// Modified by:
// Created:     09/01/2011 03:25:06
// RCS-ID:
// Copyright:   (c) 2011 C�sar Mauri Loba
// Licence:
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes

#include "config_gui.h"
#include "spcore/coreruntime.h"
#include "spcore/component.h"
#include "spcore/module.h"
#include "spcore/pin.h"
#include "spcore/basictypes.h"
using namespace spcore;

////@begin XPM images
////@end XPM images


/*
 * SDLConfigurationGUI type definition
 */

IMPLEMENT_DYNAMIC_CLASS( SDLConfigurationGUI, wxPanel )


/*
 * SDLConfigurationGUI event table definition
 */

BEGIN_EVENT_TABLE( SDLConfigurationGUI, wxPanel )

////@begin SDLConfigurationGUI event table entries
    EVT_INIT_DIALOG( SDLConfigurationGUI::OnInitDialog )

    EVT_BUTTON( wxID_CANCEL, SDLConfigurationGUI::OnCancelClick )

////@end SDLConfigurationGUI event table entries

END_EVENT_TABLE()


/*
 * SDLConfigurationGUI constructors
 */

SDLConfigurationGUI::SDLConfigurationGUI()
{
    Init();
}

SDLConfigurationGUI::SDLConfigurationGUI( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
    Init();
    Create(parent, id, caption, pos, size, style);
}


/*
 * SDLConfiguration creator
 */

bool SDLConfigurationGUI::Create( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
////@begin SDLConfigurationGUI creation
    SetExtraStyle(wxWS_EX_BLOCK_EVENTS);
    wxPanel::Create( parent, id, pos, size, style );

    CreateControls();
    if (GetSizer())
    {
        GetSizer()->SetSizeHints(this);
    }
    Centre();
////@end SDLConfigurationGUI creation
    return true;
}


/*
 * SDLConfigurationGUI destructor
 */

SDLConfigurationGUI::~SDLConfigurationGUI()
{
////@begin SDLConfigurationGUI destruction
////@end SDLConfigurationGUI destruction
}


/*
 * Member initialisation
 */

void SDLConfigurationGUI::Init()
{
////@begin SDLConfigurationGUI member initialisation
    m_txtWidth = NULL;
    m_txtHeight = NULL;
    m_chkFullscreen = NULL;
////@end SDLConfigurationGUI member initialisation
}


/*
 * Control creation for SDLConfiguration
 */

void SDLConfigurationGUI::CreateControls()
{
////@begin SDLConfigurationGUI content construction
    SDLConfigurationGUI* itemPanel1 = this;

    wxBoxSizer* itemBoxSizer2 = new wxBoxSizer(wxVERTICAL);
    itemPanel1->SetSizer(itemBoxSizer2);

    wxFlexGridSizer* itemFlexGridSizer3 = new wxFlexGridSizer(0, 2, 0, 0);
    itemBoxSizer2->Add(itemFlexGridSizer3, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5);

    wxStaticText* itemStaticText4 = new wxStaticText( itemPanel1, wxID_STATIC, _("Width:"), wxDefaultPosition, wxDefaultSize, 0 );
    itemFlexGridSizer3->Add(itemStaticText4, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    m_txtWidth = new wxTextCtrl( itemPanel1, ID_TEXTCTRL, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
    m_txtWidth->SetMaxLength(4);
    itemFlexGridSizer3->Add(m_txtWidth, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxStaticText* itemStaticText6 = new wxStaticText( itemPanel1, wxID_STATIC, _("Height:"), wxDefaultPosition, wxDefaultSize, 0 );
    itemFlexGridSizer3->Add(itemStaticText6, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    m_txtHeight = new wxTextCtrl( itemPanel1, ID_TEXTCTRL1, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
    m_txtHeight->SetMaxLength(4);
    itemFlexGridSizer3->Add(m_txtHeight, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxStaticText* itemStaticText8 = new wxStaticText( itemPanel1, wxID_STATIC, _("Full screen:"), wxDefaultPosition, wxDefaultSize, 0 );
    itemFlexGridSizer3->Add(itemStaticText8, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    m_chkFullscreen = new wxCheckBox( itemPanel1, ID_CHECKBOX, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
    m_chkFullscreen->SetValue(false);
    itemFlexGridSizer3->Add(m_chkFullscreen, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxStdDialogButtonSizer* itemStdDialogButtonSizer10 = new wxStdDialogButtonSizer;

    itemBoxSizer2->Add(itemStdDialogButtonSizer10, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5);
    wxButton* itemButton11 = new wxButton( itemPanel1, wxID_OK, _("&OK"), wxDefaultPosition, wxDefaultSize, 0 );
    itemStdDialogButtonSizer10->AddButton(itemButton11);

    wxButton* itemButton12 = new wxButton( itemPanel1, wxID_CANCEL, _("&Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
    itemStdDialogButtonSizer10->AddButton(itemButton12);

    itemStdDialogButtonSizer10->Realize();

////@end SDLConfigurationGUI content construction
}


/*
 * Should we show tooltips?
 */

bool SDLConfigurationGUI::ShowToolTips()
{
    return true;
}

/*
 * Get bitmap resources
 */

wxBitmap SDLConfigurationGUI::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin SDLConfigurationGUI bitmap retrieval
    wxUnusedVar(name);
    return wxNullBitmap;
////@end SDLConfigurationGUI bitmap retrieval
}

/*
 * Get icon resources
 */

wxIcon SDLConfigurationGUI::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin SDLConfigurationGUI icon retrieval
    wxUnusedVar(name);
    return wxNullIcon;
////@end SDLConfigurationGUI icon retrieval
}


/*
 * wxEVT_INIT_DIALOG event handler for ID_SDLCONFIGURATION
 */


void SDLConfigurationGUI::OnInitDialog( wxInitDialogEvent& event )
{
    spcore::ICoreRuntime* cr= spcore::getSpCoreRuntime();
	SmartPtr<IComponent> cfgCompo= cr->CreateComponent("sdl_config", "sdl_config", 0, NULL);
	if (!cfgCompo.get()) {
		wxMessageDialog dlg(this, _("SDL Configuration error: cannot create sdl_config component"));
		dlg.ShowModal();
	}
	else {
		// Update gui controls
		IInputPin* ipin= IComponent::FindInputPin(*cfgCompo, "width");
		assert (ipin);
		SmartPtr<const CTypeInt> width= sptype_dynamic_cast<const CTypeInt>(ipin->Read());
		assert (width.get());
		m_txtWidth->SetValue (wxString::Format(_T("%d"), width->getValue()));

		ipin= IComponent::FindInputPin(*cfgCompo, "height");
		assert (ipin);
		SmartPtr<const CTypeInt> height= sptype_dynamic_cast<const CTypeInt>(ipin->Read());
		assert (height.get());
		m_txtHeight->SetValue (wxString::Format(_T("%d"), height->getValue()));

		ipin= IComponent::FindInputPin(*cfgCompo, "fullscreen");
		assert (ipin);
		SmartPtr<const CTypeBool> fullscreen= sptype_dynamic_cast<const CTypeBool>(ipin->Read());
		assert (fullscreen.get());
		m_chkFullscreen->SetValue(fullscreen->getValue());
	}

    event.Skip(false);
}

/*
	SDLConfigGUI

	GUI support for SDLConfig

	This module only provides a pannel to set up SDL parameters
*/
class CSDLConfigGUIComp : public CComponentAdapter {
public:
	CSDLConfigGUIComp(const char * name, int argc, const char * argv[]) 
	: CComponentAdapter(name, argc, argv) {}

	static const char* getTypeName() { return "sdl_config_gui"; };
	virtual const char* GetTypeName() const { return CSDLConfigGUIComp::getTypeName(); };

	virtual wxWindow* GetGUI(wxWindow* parent) {
		return new SDLConfigurationGUI(parent);
	}

private:
	~CSDLConfigGUIComp() {
	}
};


/**
	Module
*/
class CSDLConfigGUIModule : public spcore::CModuleAdapter {
public:
	CSDLConfigGUIModule() {
		RegisterComponentFactory(SmartPtr<spcore::IComponentFactory>(new ComponentFactory<CSDLConfigGUIComp>(), false));
	}
	virtual const char * GetName() const { return "sdl_config_gui"; }
};

static spcore::IModule* g_module= NULL;
SPEXPORT_FUNCTION spcore::IModule* module_create_instance()
{
	if (g_module== NULL) g_module= new CSDLConfigGUIModule();
	return g_module;
}


/*
 * wxEVT_COMMAND_BUTTON_CLICKED event handler for wxID_CANCEL
 */

void SDLConfigurationGUI::OnCancelClick( wxCommandEvent& event )
{
    Close();
    event.Skip(false);
}

