cmake_minimum_required(VERSION 2.6)
project(wiiuse)

set(wiiuse_SRCS
	balance_board.c
	balance_board.h
	classic.c
	classic.h
	definitions.h
	dynamics.c
	dynamics.h
	events.c
	events.h
	guitar_hero_3.c
	guitar_hero_3.h
	io.c
	io.h
	ir.c
	ir.h	
	motion_plus.c
	motion_plus.h
	nunchuk.c
	nunchuk.h
	os.h
	wiiuse.c
	../include/wiiuse.h
	wiiuse_internal.h
)

IF(CMAKE_SYSTEM_NAME MATCHES "Linux")

	#
	# Linux specific files
	#

	set(wiiuse_SRCS ${wiiuse_SRCS}
		io_nix.c
	)

	#
	# Find bluetooth library
	#

	FIND_PATH (
		bluetooth_INCLUDE_DIR
	  NAMES
		bluetooth/bluetooth.h
	  PATHS
		/usr/include
		/usr/local/include
		/opt/local/include
	)

	FIND_LIBRARY (
		libbluetooth_LIBRARY 
	  NAMES
		bluetooth
	  PATHS
		/opt/local/lib
		/sw/lib
		/usr/lib
		/usr/local/lib
	)

	IF ( NOT (bluetooth_INCLUDE_DIR AND libbluetooth_LIBRARY) )
		message(FATAL_ERROR " libbluetooth not found")
	ENDIF()

	INCLUDE_DIRECTORIES (${bluetooth_INCLUDE_DIR})

ELSE()
	set(wiiuse_SRCS ${wiiuse_SRCS}
		io_win.c
	)

	FIND_PATH (
		winddk_ROOT
	  NAMES
		inc/wxp/hidsdi.h
	  PATHS	
		"$ENV{DDKROOT}"
		"$ENV{WINDDK}"	
		"C:/Program Files/WINDDK/3790.1830"
	)

	IF ( NOT (winddk_ROOT) OR (${winddk_ROOT} MATCHES ".*NOTFOUND") )
		message(FATAL_ERROR " WINDDK not found")
	ENDIF()

	message(STATUS "winddk_ROOT: ${winddk_ROOT}")

	SET (winddk_INCLUDE_DIR "${winddk_ROOT}/inc/wxp")
	SET (winddk_LIBRARY_DIR "${winddk_ROOT}/lib/wxp/i386")

	INCLUDE_DIRECTORIES(${winddk_INCLUDE_DIR})
	LINK_DIRECTORIES(${winddk_LIBRARY_DIR})

ENDIF(CMAKE_SYSTEM_NAME MATCHES "Linux")

#
# Target
#

add_library (wiiuse SHARED ${wiiuse_SRCS})

IF(CMAKE_SYSTEM_NAME MATCHES "Linux")
	TARGET_LINK_LIBRARIES(wiiuse bluetooth m)
ELSEIF(WIN32)
	TARGET_LINK_LIBRARIES(wiiuse odbc32.lib odbccp32.lib Ws2_32.lib hid.lib setupapi.lib)
ENDIF(CMAKE_SYSTEM_NAME MATCHES "Linux")

#IF(BUILD_SHARED_LIBS)
	# Remove transitive dependencies
	set_target_properties(wiiuse
      PROPERTIES
      LINK_INTERFACE_LIBRARIES ""
	)
  
	set_target_properties(wiiuse PROPERTIES 
		VERSION "${WIIUSE_MAJOR_VERSION}.${WIIUSE_RELEASE_VERSION}.${WIIUSE_EPOCH_VERSION}"
		SOVERSION ${WIIUSE_MAJOR_VERSION}
	)
	
	INSTALL (TARGETS wiiuse RUNTIME DESTINATION ${LIBRUNTIMEDIR} LIBRARY DESTINATION ${LIBDIR})
#ENDIF(BUILD_SHARED_LIBS)
