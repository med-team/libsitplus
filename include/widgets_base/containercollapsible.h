/////////////////////////////////////////////////////////////////////////////
// Name:        containercollapsible.h
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     01/06/2011 16:28:31
// RCS-ID:      
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.   
/////////////////////////////////////////////////////////////////////////////

#ifndef _CONTAINERCOLLAPSIBLE_H_
#define _CONTAINERCOLLAPSIBLE_H_


/*!
 * Includes
 */

////@begin includes
#include "wx/collpane.h"
////@end includes
#include "widgets_base/libimpexp_widgetsbase.h"

/*!
 * Forward declarations
 */

////@begin forward declarations
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_COLLAPSIBLEPANE -1
#define SYMBOL_CONTAINERCOLLAPSIBLE_STYLE wxCP_DEFAULT_STYLE|wxCP_NO_TLW_RESIZE
#define SYMBOL_CONTAINERCOLLAPSIBLE_IDNAME ID_COLLAPSIBLEPANE
#define SYMBOL_CONTAINERCOLLAPSIBLE_SIZE wxDefaultSize
#define SYMBOL_CONTAINERCOLLAPSIBLE_POSITION wxDefaultPosition
////@end control identifiers

namespace widgets_base {


/*!
 * ContainerCollapsible class declaration
 */

class SPIMPEXP_CLASS_WIDGETSBASE ContainerCollapsible: public wxGenericCollapsiblePane
{    
    DECLARE_DYNAMIC_CLASS( ContainerCollapsible )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    ContainerCollapsible();
    ContainerCollapsible(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& label = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxCP_DEFAULT_STYLE, const wxValidator& validator = wxDefaultValidator);

    /// Creation
    bool Create(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& label = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxCP_DEFAULT_STYLE, const wxValidator& validator = wxDefaultValidator);

    /// Destructor
    ~ContainerCollapsible();

    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

////@begin ContainerCollapsible event handler declarations
    /// wxEVT_COMMAND_COLLPANE_CHANGED event handler for ID_COLLAPSIBLEPANE
    void OnCollapsiblepanePaneChanged( wxCollapsiblePaneEvent& event );

////@end ContainerCollapsible event handler declarations

////@begin ContainerCollapsible member function declarations
    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end ContainerCollapsible member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin ContainerCollapsible member variables
////@end ContainerCollapsible member variables
};

}; // namespace

#endif
    // _CONTAINERCOLLAPSIBLE_H_
