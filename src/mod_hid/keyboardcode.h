/////////////////////////////////////////////////////////////////////////////
// Name:        keyboardcode.h
// Author:      Cesar Mauri Loba (cesar at crea-si dot com)
// Created:     28/09/2010
// Copyright:   (C) 2010-12 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////
#ifndef KEYBOARDCODE_H
#define KEYBOARDCODE_H


// DEBUG
//#define USING_WXWIDGETS
// DEBUG

#ifdef USING_WXWIDGETS
	#include <wx/string.h>
#endif

#if !defined(WIN32)
	#include <X11/Xlib.h>
#endif

class KeyboardCode
{
public:
	// EUnicode is used to encode a character using Unicode plus additional
	// keystrokes. These are defined using standard special purpose ASCII codes
	// (e.g. BACKSPACE) or Private Use Area of Unicode.
	enum EUnicode {
		KC_BACK=	0x8,
		KC_TAB=		0x9,
		KC_RETURN=	0xD,
		KC_ESCAPE=	0x1B,
		KC_SPACE=	0x20,
		KC_DELETE=	0x7F,

		// Unicode PUA
		KC_FIRST= 0xE000,
		KC_CLEAR= KC_FIRST,
		KC_SHIFT,
		KC_LSHIFT,
		KC_RSHIFT,
		KC_CONTROL,
		KC_LCONTROL,
		KC_RCONTROL,
		KC_MENU,
		KC_PAUSE,
		KC_CAPSLOCK,
		KC_PAGEUP,
		KC_PAGEDOWN,
		KC_END,
		KC_HOME,
		KC_LEFT,
		KC_UP,
		KC_RIGHT,
		KC_DOWN,
		KC_SELECT,
		KC_PRINT,
		KC_EXECUTE,
		KC_SNAPSHOT,
		KC_INSERT,
		KC_HELP,
		KC_CANCEL,
		KC_ALT,
		KC_LALT,
		KC_RALT,
		KC_NUMPAD0,
		KC_NUMPAD1,
		KC_NUMPAD2,
		KC_NUMPAD3,
		KC_NUMPAD4,
		KC_NUMPAD5,
		KC_NUMPAD6,
		KC_NUMPAD7,
		KC_NUMPAD8,
		KC_NUMPAD9,
		KC_MULTIPLY,
		KC_ADD,
		KC_SEPARATOR,
		KC_SUBTRACT,
		KC_DECIMAL,
		KC_DIVIDE,
		KC_F1,
		KC_F2,
		KC_F3,
		KC_F4,
		KC_F5,
		KC_F6,
		KC_F7,
		KC_F8,
		KC_F9,
		KC_F10,
		KC_F11,
		KC_F12,
		KC_F13,
		KC_F14,
		KC_F15,
		KC_F16,
		KC_F17,
		KC_F18,
		KC_F19,
		KC_F20,
		KC_F21,
		KC_F22,
		KC_F23,
		KC_F24,
		KC_NUMLOCK,
		KC_SCROLL,
		KC_NUMPAD_SPACE,
		KC_NUMPAD_TAB,
		KC_NUMPAD_ENTER,
		KC_NUMPAD_HOME,
		KC_NUMPAD_LEFT,
		KC_NUMPAD_UP,
		KC_NUMPAD_RIGHT,
		KC_NUMPAD_DOWN,
		KC_NUMPAD_PAGEUP,
		KC_NUMPAD_PAGEDOWN,
		KC_NUMPAD_END,
		KC_NUMPAD_BEGIN,
		KC_NUMPAD_INSERT,
		KC_NUMPAD_DELETE,
		KC_NUMPAD_EQUAL,
		KC_NUMPAD_MULTIPLY,
		KC_NUMPAD_ADD,
		KC_NUMPAD_SEPARATOR,
		KC_NUMPAD_SUBTRACT,
		KC_NUMPAD_DECIMAL,
		KC_NUMPAD_DIVIDE,
		KC_WINDOWS_LEFT,
		KC_WINDOWS_RIGHT,
		KC_WINDOWS_MENU,
		KC_LAST= KC_WINDOWS_MENU
	};

	// Default constructor. Initializes internal keycode to a invalid value
    KeyboardCode() : m_keyCode(0) {}
	
	// Default destructor, copy constructor and copy operator are fine
	bool operator==(const KeyboardCode &other) const {
		return m_keyCode== other.m_keyCode;
	}

	bool operator!=(const KeyboardCode &other) const {
		return !(*this== other);
	} 
	
	// Get its readable name
	const char* GetName() const;

	// Send a keystoke to the system
	void SendKeystroke();

	// TODO: for future use
	// void SendCharacter();

	bool IsValid() const { return m_keyCode!= 0; }
	
	// Reads a keycode from keyboard and returns a KeyboardCode object
	static 
	KeyboardCode ReadKeyCode();

	// Given an scan code returns the corresponding KeyboardCode object
	static 
	KeyboardCode FromScanCode (unsigned int scanCode);

	// Given a platform dependent virtual-key or KeySym returns the 
	// corresponding KeyboardCode object
	static 
	KeyboardCode FromVirtualKeyCode (unsigned long kCode);

	// Given an EUnicode value which could include any 8 bit latin1 character,
	// a full unicode character or a special EUnicode value as defined above
	static
	KeyboardCode FromEUnicode (unsigned int kc);

	// Given the name of the key (which is formed removing th prepending KC_ of the
	// entries in the EUnicode enumeration or its printed character (only ascii standard 
	// characters are supported currently) returns its KeyboardCode. 
	static
	KeyboardCode FromKeyName (const char *kname);

	// Set the internal raw value. NOTE: intended only
	// for storage purposes not to work with
	static 
	KeyboardCode FromRawValue (unsigned long kc);

	// Get the internal raw value. NOTE: intended only
	// for storage purposes not to work with
	unsigned long GetRawValue() const;

#ifdef USING_WXWIDGETS
	// Given an wxKeyCode code returns the corresponding KeyboardCode object
	static KeyboardCode FromWXKeyCode (wxKeyCode kc);
#endif

#if !defined(NDEBUG)
	void Dump() const;
#endif	
private:
	// Stores the virtual key code. This corresponds to a unique key
	// after taking into account the keyboard layout (the keyboard
	// returns a scan code which, using the layout, is	ed to 
	// this value). In Unix systems this corresponds to KeySym
	// while for Windows systems it is called virtual-key code.
#if defined(WIN32)
	// Construct object from an EUnicode entry
	KeyboardCode(unsigned int code) : m_keyCode(code) {}
	
	// On Windows store a EUnicode as a full 32bit integer,
	// although WIN32 works internally with UTF-16.
	unsigned int m_keyCode;
	enum { KEY_CODE_NAME_LENGHT= 100 };
	// TODO. NOT THREAD SAFE. Use thread-local storage. 
	// Note that VS keyword __declspec(thread) does not work with DLLs loaded using LoadLibrary()
	// See: http://msdn.microsoft.com/en-us/library/2s9wt68x.aspx
	static char g_keyCodeName[KEY_CODE_NAME_LENGHT];	
#else
	// Construct object from a KeySym
	KeyboardCode(KeySym code) : m_keyCode(code) { }
	
	KeySym m_keyCode;
	static Display* getDisplay();
#endif
	
};

#endif
