#include "sphost/testcommon.h"
#include "spcore/coreruntime.h"
#include "spcore/basictypes.h"

#include <assert.h>
#include <iostream>
#include <stdio.h>

using namespace spcore;

static
void DoTest()
{
	spcore::ICoreRuntime* core= spcore::getSpCoreRuntime();

	DumpCoreRuntime(core);

	{
		/*
		// Create using core runtime directly

		CTypeInt* intVar= dynamic_cast<CTypeInt*>(core->CreateTypeInstance("int"));
		if (!intVar) exit (-1);
		intVar->Release();
		CTypeFloat* floatVar= dynamic_cast<CTypeFloat*>(core->CreateTypeInstance("float"));
		if (!floatVar) exit (-1);
		floatVar->Release();
		CTypeBool* boolVar= dynamic_cast<CTypeBool*>(core->CreateTypeInstance("bool"));
		if (!boolVar) exit (-1);
		boolVar->Release();
		*/
	}



	// Create using static members of each type
	SmartPtr<CTypeInt> intVar= CTypeInt::CreateInstance();
	if (!intVar.get()) exit (-1);

	SmartPtr<CTypeFloat> floatVar= CTypeFloat::CreateInstance();
	if (!floatVar.get()) exit (-1);

	SmartPtr<CTypeBool> boolVar= CTypeBool::CreateInstance();
	if (!boolVar.get()) exit (-1);

	SmartPtr<CTypeString> stringVar= CTypeString::CreateInstance();
	if (!stringVar.get()) exit (-1);

	SmartPtr<CTypeComposite> compositeVar= CTypeComposite::CreateInstance();
	if (!compositeVar.get()) exit (-1);


	// Test values
	const int intValList[] = { 12, -5, 0, 40, 12345 };
	for (unsigned int i= 0; i< sizeof(intValList)/sizeof(int); ++i) {
		intVar->setValue (intValList[i]);
		if (intVar->getValue()!= intValList[i]) exit (-1);
	}

	const float floatValList[] = { 12.23455f, -5.0f, 0, 4000000000.34545f, 12345.6789f };
	for (unsigned int i= 0; i< sizeof(floatValList)/sizeof(int); ++i) {
		floatVar->setValue (floatValList[i]);
		if (floatVar->getValue()!= floatValList[i]) exit (-1);
	}

	const bool boolValList[] = { true, false };
	for (unsigned int i= 0; i< sizeof(boolValList)/sizeof(int); ++i) {
		boolVar->setValue (boolValList[i]);
		if (boolVar->getValue()!= boolValList[i]) exit (-1);
	}

	const char* stringValList[] = { "str1", "str2", "longer string\n" };
	for (unsigned int i= 0; i< sizeof(stringValList)/sizeof(char*); ++i) {
		stringVar->setValue(stringValList[i]);
		if (strcmp (stringVar->getValue(), stringValList[i])) exit (-1);
	}
	
	if (compositeVar->AddChild(intVar)) exit (-1);
	if (compositeVar->AddChild(floatVar)) exit (-1);
	if (compositeVar->AddChild(boolVar)) exit (-1);
	if (compositeVar->AddChild(stringVar)) exit (-1);

	DumpTypeInstance (*compositeVar);
}

int main (int, char**)
{
	DoTest();
	
	// Cleanup
	spcore::freeSpCoreRuntime();

	return 0;
}
