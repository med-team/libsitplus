/////////////////////////////////////////////////////////////////////////////
// Name:		test_keycodes.cpp
// Author:      Cesar Mauri Loba
// Copyright:   (C) 2012 Cesar Mauri Loba - CREA Software Systems
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////
#include "../keyboardcode.h"

#include <ostream>
#include <sstream>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>


static
void print (KeyboardCode c) {
	printf ("Raw code: %lu, 0x%lx. Description: %s\n", c.GetRawValue(), c.GetRawValue(), c.GetName());
}

static
void test_from_unicode () {

	// Test latin1 codes
	for (unsigned int i= 0; i< 256; ++i) {
		KeyboardCode c= KeyboardCode::FromEUnicode (i);
		print (c);
	}

	// Test EUnicode special codes
	for (unsigned int i= KeyboardCode::KC_FIRST; i<= KeyboardCode::KC_LAST; ++i) {
		KeyboardCode c= KeyboardCode::FromEUnicode (i);
		print (c);
	}
}

static
void test_from_virtual_key_code () {
#if defined(WIN32)
	// Only on Win32, test full VK range
	printf ("DUMPING FromVirtualKeyCodes\n");
	for (unsigned int i= 0; i< 256; ++i) {
		KeyboardCode c= KeyboardCode::FromVirtualKeyCode (i);
		printf ("i: %x ", i);
		print (c);
	}
#endif

	// No linux test. It stores KeySym values directly.
}

static
void test_from_scan_code () {
	printf ("FromScanCode\n");
	for (unsigned int i= 0; i< 256; ++i) {
		KeyboardCode c= KeyboardCode::FromScanCode (i);
		printf ("SC: %x ", i);
		print (c);
	}
}

static
int test_write () {
	KeyboardCode intro= KeyboardCode::FromEUnicode(KeyboardCode::KC_RETURN);
	for (char c= '0'; c<= '9'; c++) {
		KeyboardCode cc= KeyboardCode::FromEUnicode(c);
		cc.SendKeystroke();
		intro.SendKeystroke();
		int rc= getchar();
		if (rc!= c) {
			printf ("Wrong read: %c, %d\n", (char) rc, rc);
			return -1;
		}
		getchar();	// Remove enter
	}

	for (char c= 'a'; c<= 'z'; c++) {
		KeyboardCode cc= KeyboardCode::FromEUnicode(c);
		cc.SendKeystroke();
		intro.SendKeystroke();
		int rc= getchar();
		if (rc!= c && rc!= c-32) {
			printf ("Wrong read: %c, %d\n", (char) rc, rc);
			return -1;
		}
		getchar();	// Remove enter
	}

	return 0;
}

static const char* knames[]= {
	"ADD",
	"ALT",
	"BACK",
	"CANCEL",
	"CAPSLOCK",
	"CLEAR",
	"CONTROL",
	"DECIMAL",
	"DELETE",
	"DIVIDE",
	"DOWN",
	"END",
	"ESCAPE",
	"EXECUTE",
	"F10",
	"F11",
	"F12",
	"F13",
	"F14",
	"F15",
	"F16",
	"F17",
	"F18",
	"F19",
	"F1",
	"F20",
	"F21",
	"F22",
	"F23",
	"F24",
	"F2",
	"F3",
	"F4",
	"F5",
	"F6",
	"F7",
	"F8",
	"F9",
	"FIRST",
	"HELP",
	"HOME",
	"INSERT",
	"LALT",
	"LCONTROL",
	"LEFT",
	"LSHIFT",
	"MENU",
	"MULTIPLY",
	"NUMLOCK",
	"NUMPAD0",
	"NUMPAD1",
	"NUMPAD2",
	"NUMPAD3",
	"NUMPAD4",
	"NUMPAD5",
	"NUMPAD6",
	"NUMPAD7",
	"NUMPAD8",
	"NUMPAD9",
	"NUMPAD_ADD",
	"NUMPAD_BEGIN",
	"NUMPAD_DECIMAL",
	"NUMPAD_DELETE",
	"NUMPAD_DIVIDE",
	"NUMPAD_DOWN",
	"NUMPAD_END",
	"NUMPAD_ENTER",
	"NUMPAD_EQUAL",
	"NUMPAD_HOME",
	"NUMPAD_INSERT",
	"NUMPAD_LEFT",
	"NUMPAD_MULTIPLY",
	"NUMPAD_PAGEDOWN",
	"NUMPAD_PAGEUP",
	"NUMPAD_RIGHT",
	"NUMPAD_SEPARATOR",
	"NUMPAD_SPACE",
	"NUMPAD_SUBTRACT",
	"NUMPAD_TAB",
	"NUMPAD_UP",
	"PAGEDOWN",
	"PAGEUP",
	"PAUSE",
	"PRINT",
	"RALT",
	"RCONTROL",
	"RETURN",
	"RIGHT",
	"RSHIFT",
	"SCROLL",
	"SELECT",
	"SEPARATOR",
	"SHIFT",
	"SNAPSHOT",
	"SPACE",
	"SUBTRACT",
	"TAB",
	"UP",
	"WINDOWS_LEFT",
	"WINDOWS_MENU",
	"WINDOWS_RIGHT"
};

#define KNAMES_SIZE (sizeof(knames)/sizeof(char*))

static
int test_from_key_name () {
	KeyboardCode intro= KeyboardCode::FromEUnicode(KeyboardCode::KC_RETURN);
	char buff[2];
	buff[1]= 0;

	for (buff[0]= '0'; buff[0]<= '9'; buff[0]++) {
		KeyboardCode cc= KeyboardCode::FromKeyName(buff);
		cc.SendKeystroke();
		intro.SendKeystroke();
		int rc= getchar();
		if (rc!= buff[0]) {
			printf ("Wrong read: %c, %d\n", (char) rc, rc);
			return -1;
		}
		getchar();	// Remove enter
	}

	for (buff[0]= 'a'; buff[0]<= 'z'; buff[0]++) {
		KeyboardCode cc= KeyboardCode::FromKeyName(buff);
		cc.SendKeystroke();
		intro.SendKeystroke();
		int rc= getchar();
		if (rc!= buff[0] && rc!= buff[0]-32) {
			printf ("Wrong read: %c, %d\n", (char) rc, rc);
			return -1;
		}
		getchar();	// Remove enter
	}

	for (buff[0]= 32; buff[0]<= 64; buff[0]++) {
		KeyboardCode cc= KeyboardCode::FromKeyName(buff);
		print (cc);
	}

	for (buff[0]= 91; buff[0]<= 96; buff[0]++) {
		KeyboardCode cc= KeyboardCode::FromKeyName(buff);
		print (cc);
	}

	for (buff[0]= 123; buff[0]<= 126; buff[0]++) {
		KeyboardCode cc= KeyboardCode::FromKeyName(buff);
		print (cc);
	}

	for (unsigned int i= 0; i< KNAMES_SIZE; i++) {
		printf("Trans: %s. ", knames[i]);
		KeyboardCode c= KeyboardCode::FromKeyName(knames[i]);
		print (c);
	}

	return 0;
}

int main(int, char *[]) {
	test_from_unicode ();

	test_from_virtual_key_code ();

	test_from_scan_code ();

#if !defined(WIN32)
	// On Linux and without this sleep the test gets stuck waiting
	// for a enter keypress.
	sleep (1);
#endif

	if (test_write ()) return -1;

	if (test_from_key_name ()) return -1;

	// Manual tests
	KeyboardCode c;
	c= KeyboardCode::FromEUnicode(KeyboardCode::KC_NUMLOCK);
	c.SendKeystroke();
	c= KeyboardCode::FromEUnicode(KeyboardCode::KC_CAPSLOCK);
	c.SendKeystroke();
	c= KeyboardCode::FromEUnicode(KeyboardCode::KC_SCROLL);
	c.SendKeystroke();

	// If execution reaches this => all tests ok
	return 0;
}

