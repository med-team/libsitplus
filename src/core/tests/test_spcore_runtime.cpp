#include "sphost/testcommon.h"
#include "spcore/coreruntime.h"

#include <assert.h>
#include <iostream>
#include <stdio.h>


static
void DoTest()
{
	spcore::ICoreRuntime* core= spcore::getSpCoreRuntime();

	DumpCoreRuntime(core);
	
	
}

int main (int, char**)
{
	DoTest();
	
	// Cleanup
	spcore::freeSpCoreRuntime();

	return 0;
}
