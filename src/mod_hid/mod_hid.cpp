/////////////////////////////////////////////////////////////////////////////
// File:        mod_hid.cpp
// Author:      Cesar Mauri Loba (cesar at crea-si dot com)
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////
#include "spcore/component.h"
#include "spcore/module.h"
#include "spcore/libimpexp.h"
#include "spcore/basictypes.h"

#include "mousecontrol.h"
#include "keyboardcode.h"

#include <string.h>

using namespace spcore;

namespace mod_hid {

/**
	mouse_output component

		Send mouse events.
		
	Input pins:
		left_click (any)	- Perform left click when message received.
		right_click (any)	- Perform right click when message received.
		middle_click (any)	- Perform middle click when message received.
	
	Ouput pins:		

	Command line:
*/
class MouseOutput : public CComponentAdapter {
public:
	static const char* getTypeName() { return "mouse_output"; }
	virtual const char* GetTypeName() const { return MouseOutput::getTypeName(); }
	MouseOutput(const char * name, int argc, const char * argv[]) 
	: CComponentAdapter(name, argc, argv)
	{

		if (RegisterInputPin(*SmartPtr<IInputPin>(new InputPinClick("left_click", *this, MouseOutput::LEFT_CLICK), false))!= 0)
			throw std::runtime_error("error creating input pin left_click");

		if (RegisterInputPin(*SmartPtr<IInputPin>(new InputPinClick("right_click", *this, MouseOutput::RIGHT_CLICK), false))!= 0)
			throw std::runtime_error("error creating input pin right_click");

		if (RegisterInputPin(*SmartPtr<IInputPin>(new InputPinClick("middle_click", *this, MouseOutput::MIDDLE_CLICK), false))!= 0)
			throw std::runtime_error("error creating input pin middle_click");
	}

	enum EClickType { LEFT_CLICK= 0, RIGHT_CLICK= 1, MIDDLE_CLICK= 2 } ;

private:
	virtual ~MouseOutput() {}

	class InputPinClick : public CInputPinWriteOnly<CTypeAny, MouseOutput> {
	public:
		InputPinClick (const char * name, MouseOutput & component, MouseOutput::EClickType ct)
		: CInputPinWriteOnly<CTypeAny, MouseOutput>(name, component) 
		, m_clickType(ct) 
		{			
		}

		virtual int DoSend(const CTypeAny & ) {
			switch (m_clickType) {
				case MouseOutput::LEFT_CLICK:
					this->m_component->m_mouseControl.LeftClick();
					break;
				case MouseOutput::RIGHT_CLICK:
					this->m_component->m_mouseControl.RightClick();
					break;
				case MouseOutput::MIDDLE_CLICK:
					this->m_component->m_mouseControl.MiddleClick();
					break;
				default:
					assert(false);
			}
			return 0;
		}

	private:
		MouseOutput::EClickType m_clickType;
	};

	//
	// Data members
	//
	CMouseControl m_mouseControl;
};

typedef ComponentFactory<MouseOutput> MouseOutputFactory;

/**
	keystroke_mouse_output component

		Send keystrokes or mouse events. Note that that for keystrokes it only 
		emulates the corresponding keypress regarless the CONTROL, SHIFT and ALT
		states needed to form a certain character.

		
	Input pins:
		event_name (string)	- Name of the event to send
			Available mouse events: 
				MOUSE_LEFT,	MOUSE_RIGHT, MOUSE_MIDDLE
			Available keyboard events:
				BACK, TAB, RETURN, ESCAPE, SPACE, DELETE, CLEAR, SHIFT, LSHIFT, RSHIFT, 
				CONTROL, LCONTROL, RCONTROL, MENU, PAUSE, CAPSLOCK, PAGEUP, PAGEDOWN, 
				END, HOME, LEFT, UP, RIGHT, DOWN, SELECT, PRINT, EXECUTE, SNAPSHOT, 
				INSERT, HELP, CANCEL, ALT, LALT, RALT, NUMPAD0, NUMPAD1, NUMPAD2, NUMPAD3, 
				NUMPAD4, NUMPAD5, NUMPAD6, NUMPAD7, NUMPAD8, NUMPAD9, MULTIPLY, ADD, 
				SEPARATOR, SUBTRACT, DECIMAL, DIVIDE, F1, F2, F3, F4, F5, F6, F7, F8, F9, 
				F10, F11, F12, F13, F14, F15, F16, F17, F18, F19, F20, F21, F22, F23, F24, 
				NUMLOCK, SCROLL, NUMPAD_SPACE, NUMPAD_TAB, NUMPAD_ENTER, NUMPAD_HOME, 
				NUMPAD_LEFT, NUMPAD_UP, NUMPAD_RIGHT, NUMPAD_DOWN, NUMPAD_PAGEUP, 
				NUMPAD_PAGEDOWN, NUMPAD_END, NUMPAD_BEGIN, NUMPAD_INSERT, NUMPAD_DELETE, 
				NUMPAD_EQUAL, NUMPAD_MULTIPLY, NUMPAD_ADD, NUMPAD_SEPARATOR, NUMPAD_SUBTRACT, 
				NUMPAD_DECIMAL, NUMPAD_DIVIDE, WINDOWS_LEFT, WINDOWS_RIGHT,WINDOWS_MENU
				and standard ASCII characters
	Ouput pins:		

	Command line:
*/
class KeystrokeMouseOutput : public CComponentAdapter {
public:
	static const char* getTypeName() { return "keystroke_mouse_output"; }
	virtual const char* GetTypeName() const { return KeystrokeMouseOutput::getTypeName(); }
	KeystrokeMouseOutput(const char * name, int argc, const char * argv[]) 
	: CComponentAdapter(name, argc, argv)
	{
		if (RegisterInputPin(*SmartPtr<IInputPin>(new InputPinEventName("event_name", *this), false))!= 0)
			throw std::runtime_error("error creating input pin event_name");
	}

private:
	virtual ~KeystrokeMouseOutput() {}

	class InputPinEventName : public CInputPinWriteOnly<CTypeString, KeystrokeMouseOutput> {
	public:
		InputPinEventName (const char * name, KeystrokeMouseOutput & component)
		: CInputPinWriteOnly<CTypeString, KeystrokeMouseOutput>(name, component) 
		{			
		}

		#if defined(WIN32)
			#define strcasecmp stricmp
		#endif

		virtual int DoSend(const CTypeString & name) {
			if (strcasecmp(name.getValue(), "MOUSE_LEFT")== 0)
				this->m_component->m_mouseControl.LeftClick();
			else if (strcasecmp(name.getValue(), "MOUSE_RIGHT")== 0)
				this->m_component->m_mouseControl.RightClick();
			else if (strcasecmp(name.getValue(), "MOUSE_MIDDLE")== 0)
				this->m_component->m_mouseControl.MiddleClick();
			else {
				// Keyboard event
				KeyboardCode c= KeyboardCode::FromKeyName(name.getValue());
				if (c.IsValid()) c.SendKeystroke();
			}
			return 0;
		}
	};

	//
	// Data members
	//
	CMouseControl m_mouseControl;

};

typedef ComponentFactory<KeystrokeMouseOutput> KeystrokeMouseOutputFactory;


/* ******************************************************************************
	hid  module
****************************************************************************** */
class HidModule : public CModuleAdapter {
public:
	HidModule() {
		//
		// components
		//
		RegisterComponentFactory(SmartPtr<spcore::IComponentFactory>(new MouseOutputFactory(), false));
		RegisterComponentFactory(SmartPtr<spcore::IComponentFactory>(new KeystrokeMouseOutputFactory(), false));
	}

	~HidModule() {
	}

	virtual const char * GetName() const { return "mod_hid"; }
};

static spcore::IModule* g_module= NULL;
SPEXPORT_FUNCTION spcore::IModule* module_create_instance()
{
	if (g_module== NULL) g_module= new HidModule();
	return g_module;
}

};
