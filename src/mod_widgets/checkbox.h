/////////////////////////////////////////////////////////////////////////////
// Name:        checkbox.h
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     15/05/2011 19:42:16
// RCS-ID:      
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.    
/////////////////////////////////////////////////////////////////////////////

#ifndef _CHECKBOX_H_
#define _CHECKBOX_H_


/*!
 * Includes
 */

////@begin includes
////@end includes
#include "spcore/component.h"
#include "spcore/pinimpl.h"
#include "spcore/basictypes.h"

#include <wx/panel.h>
#include <wx/checkbox.h>
#include <wx/icon.h>

/*!
 * Forward declarations
 */

////@begin forward declarations
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_CHECKBOXPANEL 10004
#define ID_CHECKBOX 10005
#define SYMBOL_CHECKBOXPANEL_STYLE wxTAB_TRAVERSAL
#define SYMBOL_CHECKBOXPANEL_TITLE wxEmptyString
#define SYMBOL_CHECKBOXPANEL_IDNAME ID_CHECKBOXPANEL
#define SYMBOL_CHECKBOXPANEL_SIZE wxDefaultSize
#define SYMBOL_CHECKBOXPANEL_POSITION wxDefaultPosition
////@end control identifiers

namespace mod_widgets {

class CheckboxComponent;

/*!
 * CheckboxPanel class declaration
 */

class CheckboxPanel: public wxPanel
{    
    DECLARE_DYNAMIC_CLASS( CheckboxPanel )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    CheckboxPanel();
    CheckboxPanel( wxWindow* parent, wxWindowID id = SYMBOL_CHECKBOXPANEL_IDNAME, const wxPoint& pos = SYMBOL_CHECKBOXPANEL_POSITION, const wxSize& size = SYMBOL_CHECKBOXPANEL_SIZE, long style = SYMBOL_CHECKBOXPANEL_STYLE, const wxString& name = SYMBOL_CHECKBOXPANEL_TITLE );

    /// Creation
    bool Create( wxWindow* parent, wxWindowID id = SYMBOL_CHECKBOXPANEL_IDNAME, const wxPoint& pos = SYMBOL_CHECKBOXPANEL_POSITION, const wxSize& size = SYMBOL_CHECKBOXPANEL_SIZE, long style = SYMBOL_CHECKBOXPANEL_STYLE, const wxString& name = SYMBOL_CHECKBOXPANEL_TITLE );

    /// Destructor
    ~CheckboxPanel();

	// Set destruction callback. NULL removes.
	void SetComponent(CheckboxComponent* component) { m_component= component; }

	// Notify value change
	void ValueChanged();

private:
    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

////@begin CheckboxPanel event handler declarations

    /// wxEVT_COMMAND_CHECKBOX_CLICKED event handler for ID_CHECKBOX
    void OnCheckboxClick( wxCommandEvent& event );

////@end CheckboxPanel event handler declarations

	// Value change event
	void OnValueChanged( wxCommandEvent& );

////@begin CheckboxPanel member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end CheckboxPanel member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin CheckboxPanel member variables
    wxCheckBox* m_theCheckbox;
////@end CheckboxPanel member variables

	CheckboxComponent* m_component;
};

/**
	widget_checkbox component

	Input pins:
		value (bool)

		Sets the value of the widget.		

	Output pins:
		value (bool)

		Sends the value when the slider is manipulated

	Command line:
		[-l <label> ]	Label. If empty, no label is shown.
		[-v [0|1|true|false]]	Value (default min)
*/
class CheckboxComponent : public spcore::CComponentAdapter {
public:
	static const char* getTypeName() { return "widget_checkbox"; };
	virtual const char* GetTypeName() const { return CheckboxComponent::getTypeName(); };

	//
	// Component methods
	//
	CheckboxComponent(const char * name, int argc, const char * argv[]);
	
	virtual int DoInitialize();

	virtual wxWindow* GetGUI(wxWindow * parent);

	// 
	// Called from GUI
	//	
	void SetCheckboxValue (bool v);
	bool GetCheckboxValue() const;
	
	const std::string& GetLabel() const { return m_label; }
	
	void OnPanelDestroyed ();

private:
	CheckboxPanel* m_panel;
	SmartPtr<spcore::IInputPin> m_iPin;
	SmartPtr<spcore::IOutputPin> m_oPin;
	SmartPtr<spcore::CTypeBool> m_value;
	std::string m_label;	

	virtual ~CheckboxComponent();

	void OnPinValue (const spcore::CTypeBool & msg);
	
	class InputPinValue 
		: public spcore::CInputPinWriteOnly<spcore::CTypeBool, CheckboxComponent> {
	public:
		InputPinValue (CheckboxComponent & component)
		: spcore::CInputPinWriteOnly<spcore::CTypeBool, CheckboxComponent>("value", component) { }

		virtual int DoSend(const spcore::CTypeBool & msg) {
			m_component->OnPinValue(msg);
			return 0;
		}
	};
};

typedef spcore::ComponentFactory<CheckboxComponent> CheckboxComponentFactory;

};	// namespace

#endif
    // _CHECKBOX_H_
