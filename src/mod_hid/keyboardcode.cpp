/////////////////////////////////////////////////////////////////////////////
// Name:        keyboardcode.cpp
// Author:      Cesar Mauri Loba (cesar at crea-si dot com)
// Created:     28/09/2010
// Copyright:   (C) 2010-12 Cesar Mauri Loba - CREA Software Systems
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////
#include "keyboardcode.h"

#ifdef USING_WXWIDGETS
	#include <wx/utils.h>
	#include <wx/stdpaths.h>
	#include <wx/defs.h>
#endif

#if defined(WIN32)
	#define _WIN32_WINNT 0x0500
	#include <windows.h>
#else
	#include <X11/extensions/XTest.h>
	#define XK_MISCELLANY 1
	#define XK_LATIN1 1
	#include <X11/keysymdef.h>
	#include "ucs2keysym.h"
#endif
#include <assert.h>
#include <stdexcept>
#include <string.h>

#if !defined(WIN32)
	// Make sure that an unsinged long is wide enough to store a KeySym
	//wxCOMPILE_TIME_ASSERT( sizeof(KeySym)== sizeof(unsigned long), KeySymDifferentThanLongError );
	//wxCOMPILE_TIME_ASSERT( sizeof(KeyCode)<= sizeof(unsigned int), KeyCodeGreaterThanIntError );
	
	// Class to store a connection to a display which is automatically closed when destructor is called
	class DisplayHolder {
	private:
		Display* m_display;
	public:
		DisplayHolder() : m_display(NULL) {} 
		~DisplayHolder() {
			if (m_display) XCloseDisplay(m_display);
			m_display= NULL;
		}
		Display* get() { return m_display; }
		void set(Display* d) { m_display= d; }
	};

	static
	DisplayHolder g_displayHolder;

	Display* KeyboardCode::getDisplay() {
		if (!g_displayHolder.get()) {
			g_displayHolder.set(XOpenDisplay(NULL));
			if (!g_displayHolder.get()) throw std::runtime_error("KeyboardCode: cannot open display");
		}
		return g_displayHolder.get();
	}
#else
	char KeyboardCode::g_keyCodeName[KEY_CODE_NAME_LENGHT];

	// Convert EUnicode (inc. Unicode) to Virtual-Key Code. Return 0 if no translation available.
	static
	WORD EUnicode2VirtualKeyCode (unsigned int c) {
		// First try special codes
		switch (c) {
			case KeyboardCode::KC_BACK: return VK_BACK;
			case KeyboardCode::KC_TAB: return VK_TAB;			
			case KeyboardCode::KC_RETURN: return VK_RETURN;
			case KeyboardCode::KC_ESCAPE: return VK_ESCAPE;
			case KeyboardCode::KC_SPACE: return VK_SPACE;
			case KeyboardCode::KC_DELETE: return VK_DELETE;
			
			case KeyboardCode::KC_CLEAR: return VK_CLEAR;
			case KeyboardCode::KC_SHIFT: return VK_SHIFT;
			case KeyboardCode::KC_LSHIFT: return VK_LSHIFT;
			case KeyboardCode::KC_RSHIFT: return VK_RSHIFT;
			case KeyboardCode::KC_CONTROL: return VK_CONTROL;
			case KeyboardCode::KC_LCONTROL: return VK_LCONTROL;
			case KeyboardCode::KC_RCONTROL: return VK_RCONTROL;
			case KeyboardCode::KC_MENU: return VK_MENU;
			case KeyboardCode::KC_PAUSE: return VK_PAUSE;
			case KeyboardCode::KC_CAPSLOCK: return VK_CAPITAL;
			case KeyboardCode::KC_PAGEUP: return VK_PRIOR;
			case KeyboardCode::KC_PAGEDOWN: return VK_NEXT;
			case KeyboardCode::KC_END: return VK_END;
			case KeyboardCode::KC_HOME: return VK_HOME;
			case KeyboardCode::KC_LEFT: return VK_LEFT;
			case KeyboardCode::KC_UP: return VK_UP;
			case KeyboardCode::KC_RIGHT: return VK_RIGHT;
			case KeyboardCode::KC_DOWN: return VK_DOWN;
			case KeyboardCode::KC_SELECT: return VK_SELECT;
			case KeyboardCode::KC_PRINT: return VK_PRINT;
			case KeyboardCode::KC_EXECUTE: return VK_EXECUTE;
			case KeyboardCode::KC_SNAPSHOT: return VK_SNAPSHOT;
			case KeyboardCode::KC_INSERT: return VK_INSERT;
			case KeyboardCode::KC_HELP: return VK_HELP;
			case KeyboardCode::KC_CANCEL: return VK_CANCEL;
			case KeyboardCode::KC_ALT: return VK_MENU;
			case KeyboardCode::KC_LALT: return VK_MENU;
			// AltGr is a combination of ctrl + alt on WIN32. No translation provided.
			case KeyboardCode::KC_RALT: return 0;
			case KeyboardCode::KC_NUMPAD0: return VK_NUMPAD0;
			case KeyboardCode::KC_NUMPAD1: return VK_NUMPAD1;
			case KeyboardCode::KC_NUMPAD2: return VK_NUMPAD2;
			case KeyboardCode::KC_NUMPAD3: return VK_NUMPAD3;
			case KeyboardCode::KC_NUMPAD4: return VK_NUMPAD4;
			case KeyboardCode::KC_NUMPAD5: return VK_NUMPAD5;
			case KeyboardCode::KC_NUMPAD6: return VK_NUMPAD6;
			case KeyboardCode::KC_NUMPAD7: return VK_NUMPAD7;
			case KeyboardCode::KC_NUMPAD8: return VK_NUMPAD8;
			case KeyboardCode::KC_NUMPAD9: return VK_NUMPAD9;
			case KeyboardCode::KC_MULTIPLY: c= 0x2A; break; // let VkKeyScanW do the job
			case KeyboardCode::KC_ADD: c= 0x2B; break; // let VkKeyScanW do the job
			case KeyboardCode::KC_SEPARATOR: return 0;	// Ni est�, ni se le espera.
			case KeyboardCode::KC_SUBTRACT: c= 0x2D; break; // let VkKeyScanW do the job
			case KeyboardCode::KC_DECIMAL: return VK_DECIMAL;
			case KeyboardCode::KC_DIVIDE: c= 0x2F; break; // let VkKeyScanW do the job
			case KeyboardCode::KC_F1: return VK_F1;
			case KeyboardCode::KC_F2: return VK_F2;
			case KeyboardCode::KC_F3: return VK_F3;
			case KeyboardCode::KC_F4: return VK_F4;
			case KeyboardCode::KC_F5: return VK_F5;
			case KeyboardCode::KC_F6: return VK_F6;
			case KeyboardCode::KC_F7: return VK_F7;
			case KeyboardCode::KC_F8: return VK_F8;
			case KeyboardCode::KC_F9: return VK_F9;
			case KeyboardCode::KC_F10: return VK_F10;
			case KeyboardCode::KC_F11: return VK_F11;
			case KeyboardCode::KC_F12: return VK_F12;
			case KeyboardCode::KC_F13: return VK_F13;
			case KeyboardCode::KC_F14: return VK_F14;
			case KeyboardCode::KC_F15: return VK_F15;
			case KeyboardCode::KC_F16: return VK_F16;
			case KeyboardCode::KC_F17: return VK_F17;
			case KeyboardCode::KC_F18: return VK_F18;
			case KeyboardCode::KC_F19: return VK_F19;
			case KeyboardCode::KC_F20: return VK_F20;
			case KeyboardCode::KC_F21: return VK_F21;
			case KeyboardCode::KC_F22: return VK_F22;
			case KeyboardCode::KC_F23: return VK_F23;
			case KeyboardCode::KC_F24: return VK_F24;
			case KeyboardCode::KC_NUMLOCK: return VK_NUMLOCK;
			case KeyboardCode::KC_SCROLL: return VK_SCROLL;
			case KeyboardCode::KC_NUMPAD_SPACE: return VK_SPACE;
			case KeyboardCode::KC_NUMPAD_TAB: return VK_TAB;
			case KeyboardCode::KC_NUMPAD_ENTER: return VK_RETURN;
			case KeyboardCode::KC_NUMPAD_HOME: return VK_HOME;
			case KeyboardCode::KC_NUMPAD_LEFT: return VK_LEFT;
			case KeyboardCode::KC_NUMPAD_UP: return VK_UP;
			case KeyboardCode::KC_NUMPAD_RIGHT: return VK_RIGHT;
			case KeyboardCode::KC_NUMPAD_DOWN: return VK_DOWN;
			case KeyboardCode::KC_NUMPAD_PAGEUP: return VK_PRIOR;
			case KeyboardCode::KC_NUMPAD_PAGEDOWN: return VK_NEXT;
			case KeyboardCode::KC_NUMPAD_END: return VK_END;
			case KeyboardCode::KC_NUMPAD_BEGIN: return VK_NUMPAD5;
			case KeyboardCode::KC_NUMPAD_INSERT: return VK_INSERT;
			case KeyboardCode::KC_NUMPAD_DELETE: return VK_DELETE;
			case KeyboardCode::KC_NUMPAD_EQUAL: return 0;
			case KeyboardCode::KC_NUMPAD_MULTIPLY: return VK_MULTIPLY;
			case KeyboardCode::KC_NUMPAD_ADD: return VK_ADD;
			case KeyboardCode::KC_NUMPAD_SEPARATOR: return VK_SEPARATOR;
			case KeyboardCode::KC_NUMPAD_SUBTRACT: return VK_SUBTRACT;
			case KeyboardCode::KC_NUMPAD_DECIMAL: return VK_DECIMAL;
			case KeyboardCode::KC_NUMPAD_DIVIDE: return VK_DIVIDE;
			case KeyboardCode::KC_WINDOWS_LEFT: return VK_LWIN;
			case KeyboardCode::KC_WINDOWS_RIGHT: return VK_RWIN;
			case KeyboardCode::KC_WINDOWS_MENU: return VK_APPS;
		}
		// At this point we assume an unicode character was provided.
		// On WIN32, VkKeyScanW function only takes one 16bit unicode 
		// character (encoded using UTF-16), so in theory characters
		// in the Supplementary Multilingual Plane cannot be translated
		// using this function.
		assert (c<= 0xFFFF);
		WORD evkc= VkKeyScanW((WCHAR) c);
		if (evkc== 0xFFFF) return 0;

		unsigned char hi= evkc >> 8;
		// If control XOR alt pressed then is a special control code without translation
		if (hi & 2 && !(hi & 4)) return 0;
		if (hi & 4 && !(hi & 2)) return 0;

		// Sometimes VkKeyScanW returns an undefined VKC
		unsigned char lo= evkc & 0xFF;
		if (lo== 64) return 0;

		return (WORD) lo;
	}

	// Convert a native WIN32 Virtual-Key Code to EUnicode (inc. Unicode). 
	// Return 0 if no translation available.
	// NOTE that on WIN32 given a virtual-key code can be converted into 
	// many characters
	static
	unsigned int VirtualKeyCode2EUnicode (WORD vkc) {
		// First try to transate the virtual key-code into a character
		UINT c= MapVirtualKeyW(vkc, MAPVK_VK_TO_CHAR);
		// Ignore whether is a dead key
		c= c & 0xFFFF;		
		if (c) return c;	// Finish if translation successful

		// OK. Untranslatable VKC. Switch/case mapping.
		switch (vkc) {
			case VK_LBUTTON:
			case VK_RBUTTON: return 0;
			case VK_CANCEL: return KeyboardCode::KC_CANCEL;
			case VK_MBUTTON:
			case VK_XBUTTON1:
			case VK_XBUTTON2: return 0;
			case VK_BACK: return KeyboardCode::KC_BACK;
			case VK_TAB: return KeyboardCode::KC_TAB;
			case VK_CLEAR: return KeyboardCode::KC_CLEAR;			
			case VK_RETURN: return KeyboardCode::KC_RETURN; // WIN32 cannot tell between main & keypad RETURN
			case VK_SHIFT: return KeyboardCode::KC_SHIFT;
			case VK_CONTROL: return KeyboardCode::KC_CONTROL;
			case VK_MENU: return KeyboardCode::KC_ALT; // VK_MENU is ALT key, no matter left or right
			case VK_PAUSE: return KeyboardCode::KC_PAUSE;
			case VK_CAPITAL: return KeyboardCode::KC_CAPSLOCK;
			case VK_KANA: /* VK_HANGUL */
			case VK_JUNJA:
			case VK_FINAL:
			case VK_HANJA: /* VK_KANJI */ return 0;			
			case VK_ESCAPE: return KeyboardCode::KC_ESCAPE;
			case VK_CONVERT: 
			case VK_NONCONVERT: 
			case VK_ACCEPT:
			case VK_MODECHANGE: return 0;
			case VK_SPACE: return KeyboardCode::KC_SPACE;
			case VK_PRIOR: return KeyboardCode::KC_PAGEUP;
			case VK_NEXT: return KeyboardCode::KC_PAGEDOWN;
			case VK_END: return KeyboardCode::KC_END;
			case VK_HOME: return KeyboardCode::KC_HOME;
			case VK_LEFT: return KeyboardCode::KC_LEFT;
			case VK_UP: return KeyboardCode::KC_UP;
			case VK_RIGHT: return KeyboardCode::KC_RIGHT;
			case VK_DOWN: return KeyboardCode::KC_DOWN;
			case VK_SELECT: return KeyboardCode::KC_SELECT;
			case VK_PRINT: return KeyboardCode::KC_PRINT;
			case VK_EXECUTE: return KeyboardCode::KC_EXECUTE;
			case VK_SNAPSHOT: return KeyboardCode::KC_SNAPSHOT;
			case VK_INSERT: return KeyboardCode::KC_INSERT;
			case VK_DELETE: return KeyboardCode::KC_DELETE;		
			case VK_HELP: return KeyboardCode::KC_HELP;
			case VK_LWIN: return KeyboardCode::KC_WINDOWS_LEFT;
			case VK_RWIN: return KeyboardCode::KC_WINDOWS_RIGHT;
			case VK_APPS: return KeyboardCode::KC_WINDOWS_MENU;
			case VK_SLEEP: return 0;
			case VK_NUMPAD0: return KeyboardCode::KC_NUMPAD0;
			case VK_NUMPAD1: return KeyboardCode::KC_NUMPAD1;
			case VK_NUMPAD2: return KeyboardCode::KC_NUMPAD2;
			case VK_NUMPAD3: return KeyboardCode::KC_NUMPAD3;
			case VK_NUMPAD4: return KeyboardCode::KC_NUMPAD4;
			case VK_NUMPAD5: return KeyboardCode::KC_NUMPAD5;
			case VK_NUMPAD6: return KeyboardCode::KC_NUMPAD6;
			case VK_NUMPAD7: return KeyboardCode::KC_NUMPAD7;
			case VK_NUMPAD8: return KeyboardCode::KC_NUMPAD8;
			case VK_NUMPAD9: return KeyboardCode::KC_NUMPAD9;
			case VK_MULTIPLY: return KeyboardCode::KC_NUMPAD_MULTIPLY;
			case VK_ADD: return KeyboardCode::KC_NUMPAD_ADD;
			case VK_SEPARATOR: return KeyboardCode::KC_NUMPAD_SEPARATOR;
			case VK_SUBTRACT: return KeyboardCode::KC_NUMPAD_SUBTRACT;
			case VK_DECIMAL: return KeyboardCode::KC_NUMPAD_DECIMAL;
			case VK_DIVIDE: return KeyboardCode::KC_NUMPAD_DIVIDE;
			case VK_F1: return KeyboardCode::KC_F1;
			case VK_F2: return KeyboardCode::KC_F2;
			case VK_F3: return KeyboardCode::KC_F3;
			case VK_F4: return KeyboardCode::KC_F4;
			case VK_F5: return KeyboardCode::KC_F5;
			case VK_F6: return KeyboardCode::KC_F6;
			case VK_F7: return KeyboardCode::KC_F7;
			case VK_F8: return KeyboardCode::KC_F8;
			case VK_F9: return KeyboardCode::KC_F9;
			case VK_F10: return KeyboardCode::KC_F10;
			case VK_F11: return KeyboardCode::KC_F11;
			case VK_F12: return KeyboardCode::KC_F12;
			case VK_F13: return KeyboardCode::KC_F13;
			case VK_F14: return KeyboardCode::KC_F14;
			case VK_F15: return KeyboardCode::KC_F15;
			case VK_F16: return KeyboardCode::KC_F16;
			case VK_F17: return KeyboardCode::KC_F17;
			case VK_F18: return KeyboardCode::KC_F18;
			case VK_F19: return KeyboardCode::KC_F19;
			case VK_F20: return KeyboardCode::KC_F20;
			case VK_F21: return KeyboardCode::KC_F21;
			case VK_F22: return KeyboardCode::KC_F22;
			case VK_F23: return KeyboardCode::KC_F23;
			case VK_F24: return KeyboardCode::KC_F24;
			case VK_NUMLOCK: return KeyboardCode::KC_NUMLOCK;
			case VK_SCROLL: return KeyboardCode::KC_SCROLL;
			case VK_LSHIFT: return KeyboardCode::KC_LSHIFT;
			case VK_RSHIFT: return KeyboardCode::KC_RSHIFT;
			case VK_LCONTROL: return KeyboardCode::KC_LCONTROL;
			case VK_RCONTROL: return KeyboardCode::KC_RCONTROL;
			case VK_LMENU: return KeyboardCode::KC_LALT;
			case VK_RMENU: return KeyboardCode::KC_RALT;
			/* gap of browser, multimedia and internet buttons */

			// these with assert should have been translated before
			case VK_OEM_1: assert (0); return 0;
			case VK_OEM_PLUS: assert (0); return '+';
			case VK_OEM_COMMA: assert (0); return ',';
			case VK_OEM_MINUS: assert (0); return '-';
			case VK_OEM_PERIOD: assert (0); return '.';
		}
		return 0;
	}

#endif 

const char* KeyboardCode::GetName() const
{	 
#if !defined(WIN32)	
	return XKeysymToString(m_keyCode);
#else
	// This function only returns the name of a key identified by its scan code
	WORD vkc= EUnicode2VirtualKeyCode (m_keyCode);
	if (!vkc) return "";
	// Note that when translating from a virtual-key code multiple scan codes
	// are possible (e.g. arrow keys, delete, etc.) and this function does not
	// allow to choose. The only effective workaround would be to store the
	// original scan code is the object is constructed using this.
	UINT scanCode= ::MapVirtualKeyW(vkc, MAPVK_VK_TO_VSC);
	scanCode= scanCode << 16;
	*g_keyCodeName= '\0';
	::GetKeyNameTextA ((LONG) scanCode, g_keyCodeName, KEY_CODE_NAME_LENGHT);
	return g_keyCodeName;
#endif	
}

void KeyboardCode::SendKeystroke()
{
	if (!m_keyCode) return;
#if !defined(WIN32)
	Display* d= getDisplay();

	KeyCode k= XKeysymToKeycode(d, m_keyCode);
	XTestFakeKeyEvent(d, k, True, 0);
	XTestFakeKeyEvent(d, k, False, 1);
	XFlush(d);
#else
	// It seems that, to work properly, SendInput() need both the virtual-key code and the scan code. 
	// Note that KEYEVENTF_EXTENDEDKEY flag is not used.
	WORD vkc= EUnicode2VirtualKeyCode (m_keyCode);
	if (!vkc) return;
	unsigned int scanCode = MapVirtualKey(vkc, MAPVK_VK_TO_VSC);

	INPUT ip;
	memset(&ip, 0, sizeof(ip));
	ip.type= INPUT_KEYBOARD;
	ip.ki.wVk= vkc;
	ip.ki.wScan= (WORD) scanCode;

	// Key press
	SendInput(1, &ip, sizeof(ip));

	// Key release
	ip.ki.dwFlags|= KEYEVENTF_KEYUP;
	SendInput(1, &ip, sizeof(ip));
#endif
}

// Reads a keycode from keyboard and returns a KeyboardCode object
KeyboardCode KeyboardCode::ReadKeyCode()
{
#if !defined(WIN32)
	char keys_return[32];
	unsigned char keys;
	unsigned int kc = 0;
	
	XQueryKeymap(getDisplay(), keys_return);

	// TODO: implement this correctly
	for(int i=0; i<32; i++) {
		keys = (unsigned char) keys_return[i];
		if (keys > 0) {
			switch (keys) {
				case 1:   kc = 0; break;
				case 2:   kc = 1; break;
				case 4:   kc = 2; break;
				case 8:   kc = 3; break;
				case 16:  kc = 4; break;
				case 32:  kc = 5; break;
				case 64:  kc = 6; break;
				case 128: kc = 7; break;
			}
			kc += 8 * i;
		}
	}	
	return FromScanCode (kc);
#else
	// Still not available for Windows
	assert(0);
	return KeyboardCode();
#endif
}

// Given an scan code returns the corresponding KeyboardCode object
KeyboardCode KeyboardCode::FromScanCode (unsigned int scanCode)
{	
#if !defined(WIN32)
	return KeyboardCode (static_cast<unsigned long> (XKeycodeToKeysym(getDisplay(), (KeyCode) scanCode, 0)));
#else
	UINT vKCode= ::MapVirtualKey(scanCode, MAPVK_VSC_TO_VK_EX);
	assert (vKCode< 0xFFFF);
	return KeyboardCode(VirtualKeyCode2EUnicode (static_cast<WORD>(vKCode)));
#endif
}

// Given a platform dependent virtual-key or KeySym returns the 
// corresponding KeyboardCode object
KeyboardCode KeyboardCode::FromVirtualKeyCode (unsigned long vkCode)
{
#if !defined(WIN32)
	return KeyboardCode (vkCode);
#else
	assert (vkCode<= 0xFFFF);
	return KeyboardCode(VirtualKeyCode2EUnicode (static_cast<WORD>(vkCode)));
#endif
}

// Given an EUnicode value which could include any 8 bit latin1 character,
// a full unicode character or a special EUnicode value as defined above
KeyboardCode KeyboardCode::FromEUnicode (unsigned int kc)
{
#if !defined(WIN32)
	KeySym ks= ucs2keysym(kc);
	if (ks) return KeyboardCode(ks);

	switch (kc) {
		case KeyboardCode::KC_BACK: return KeyboardCode(XK_BackSpace);
		case KeyboardCode::KC_TAB: return KeyboardCode(XK_Tab);
		case KeyboardCode::KC_RETURN: return KeyboardCode(XK_Return);
		case KeyboardCode::KC_ESCAPE: return KeyboardCode(XK_Escape);
		case KeyboardCode::KC_SPACE: return KeyboardCode(XK_KP_Space);
		case KeyboardCode::KC_DELETE: return KeyboardCode(XK_Delete);

		case KeyboardCode::KC_CLEAR: return KeyboardCode(XK_Clear);
		case KeyboardCode::KC_SHIFT: return KeyboardCode(XK_Shift_L);
		case KeyboardCode::KC_LSHIFT: return KeyboardCode(XK_Shift_L);
		case KeyboardCode::KC_RSHIFT: return KeyboardCode(XK_Shift_R);
		case KeyboardCode::KC_CONTROL: return KeyboardCode(XK_Control_L);
		case KeyboardCode::KC_LCONTROL: return KeyboardCode(XK_Control_L);
		case KeyboardCode::KC_RCONTROL: return KeyboardCode(XK_Control_R);
		case KeyboardCode::KC_MENU: return KeyboardCode(XK_Menu);
		case KeyboardCode::KC_PAUSE: return KeyboardCode(XK_Pause);
		case KeyboardCode::KC_CAPSLOCK: return KeyboardCode(XK_Caps_Lock);
		case KeyboardCode::KC_PAGEUP: return KeyboardCode(XK_Page_Up);
		case KeyboardCode::KC_PAGEDOWN: return KeyboardCode(XK_Page_Down); 
		case KeyboardCode::KC_END: return KeyboardCode(XK_End);
		case KeyboardCode::KC_HOME: return KeyboardCode(XK_Home);
		case KeyboardCode::KC_LEFT: return KeyboardCode(XK_Left);
		case KeyboardCode::KC_UP: return KeyboardCode(XK_Up);
		case KeyboardCode::KC_RIGHT: return KeyboardCode(XK_Right);
		case KeyboardCode::KC_DOWN: return KeyboardCode(XK_Down);
		case KeyboardCode::KC_SELECT: return KeyboardCode(XK_Select);
		case KeyboardCode::KC_PRINT: return KeyboardCode(XK_Print);
		case KeyboardCode::KC_EXECUTE: return KeyboardCode(XK_Execute);
		case KeyboardCode::KC_SNAPSHOT: return KeyboardCode(XK_Print);
		case KeyboardCode::KC_INSERT: return KeyboardCode(XK_Insert);
		case KeyboardCode::KC_HELP: return KeyboardCode(XK_Help);
		case KeyboardCode::KC_CANCEL: return KeyboardCode(XK_Break);
		case KeyboardCode::KC_ALT: return KeyboardCode(XK_Alt_L);
		case KeyboardCode::KC_LALT: return KeyboardCode(XK_Alt_L);
		case KeyboardCode::KC_RALT: return KeyboardCode(XK_Alt_R);
		case KeyboardCode::KC_NUMPAD0: return KeyboardCode(XK_KP_0);
		case KeyboardCode::KC_NUMPAD1: return KeyboardCode(XK_KP_1);
		case KeyboardCode::KC_NUMPAD2: return KeyboardCode(XK_KP_2);
		case KeyboardCode::KC_NUMPAD3: return KeyboardCode(XK_KP_3);
		case KeyboardCode::KC_NUMPAD4: return KeyboardCode(XK_KP_4);
		case KeyboardCode::KC_NUMPAD5: return KeyboardCode(XK_KP_5);
		case KeyboardCode::KC_NUMPAD6: return KeyboardCode(XK_KP_6);
		case KeyboardCode::KC_NUMPAD7: return KeyboardCode(XK_KP_7);
		case KeyboardCode::KC_NUMPAD8: return KeyboardCode(XK_KP_8);
		case KeyboardCode::KC_NUMPAD9: return KeyboardCode(XK_KP_9);
		case KeyboardCode::KC_MULTIPLY: return KeyboardCode(XK_multiply);
		case KeyboardCode::KC_ADD: return KeyboardCode(XK_plus);
		case KeyboardCode::KC_SEPARATOR: return KeyboardCode(0);
		case KeyboardCode::KC_SUBTRACT: return KeyboardCode(XK_minus);
		case KeyboardCode::KC_DECIMAL: return KeyboardCode(XK_period);
		case KeyboardCode::KC_DIVIDE: return KeyboardCode(XK_division);
		case KeyboardCode::KC_F1: return KeyboardCode(XK_F1);
		case KeyboardCode::KC_F2: return KeyboardCode(XK_F2);
		case KeyboardCode::KC_F3: return KeyboardCode(XK_F3);
		case KeyboardCode::KC_F4: return KeyboardCode(XK_F4);
		case KeyboardCode::KC_F5: return KeyboardCode(XK_F5);
		case KeyboardCode::KC_F6: return KeyboardCode(XK_F6);
		case KeyboardCode::KC_F7: return KeyboardCode(XK_F7);
		case KeyboardCode::KC_F8: return KeyboardCode(XK_F8);
		case KeyboardCode::KC_F9: return KeyboardCode(XK_F9);
		case KeyboardCode::KC_F10: return KeyboardCode(XK_F10);
		case KeyboardCode::KC_F11: return KeyboardCode(XK_F11);
		case KeyboardCode::KC_F12: return KeyboardCode(XK_F12);
		case KeyboardCode::KC_F13: return KeyboardCode(XK_F13);
		case KeyboardCode::KC_F14: return KeyboardCode(XK_F14);
		case KeyboardCode::KC_F15: return KeyboardCode(XK_F15);
		case KeyboardCode::KC_F16: return KeyboardCode(XK_F16);
		case KeyboardCode::KC_F17: return KeyboardCode(XK_F17);
		case KeyboardCode::KC_F18: return KeyboardCode(XK_F18);
		case KeyboardCode::KC_F19: return KeyboardCode(XK_F19);
		case KeyboardCode::KC_F20: return KeyboardCode(XK_F20);
		case KeyboardCode::KC_F21: return KeyboardCode(XK_F21);
		case KeyboardCode::KC_F22: return KeyboardCode(XK_F22);
		case KeyboardCode::KC_F23: return KeyboardCode(XK_F23);
		case KeyboardCode::KC_F24: return KeyboardCode(XK_F24);
		case KeyboardCode::KC_NUMLOCK: return KeyboardCode(XK_Num_Lock);
		case KeyboardCode::KC_SCROLL: return KeyboardCode(XK_Scroll_Lock);		
		case KeyboardCode::KC_NUMPAD_SPACE: return KeyboardCode(XK_KP_Space);
		case KeyboardCode::KC_NUMPAD_TAB: return KeyboardCode(XK_KP_Tab);
		case KeyboardCode::KC_NUMPAD_ENTER: return KeyboardCode(XK_KP_Enter);
		case KeyboardCode::KC_NUMPAD_HOME: return KeyboardCode(XK_KP_Home);
		case KeyboardCode::KC_NUMPAD_LEFT: return KeyboardCode(XK_KP_Left);
		case KeyboardCode::KC_NUMPAD_UP: return KeyboardCode(XK_KP_Up);
		case KeyboardCode::KC_NUMPAD_RIGHT: return KeyboardCode(XK_KP_Right);
		case KeyboardCode::KC_NUMPAD_DOWN: return KeyboardCode(XK_KP_Down);
		case KeyboardCode::KC_NUMPAD_PAGEUP: return KeyboardCode(XK_KP_Page_Up);
		case KeyboardCode::KC_NUMPAD_PAGEDOWN: return KeyboardCode(XK_KP_Page_Down);
		case KeyboardCode::KC_NUMPAD_END: return KeyboardCode(XK_KP_End);
		case KeyboardCode::KC_NUMPAD_BEGIN: return KeyboardCode(XK_KP_Begin);
		case KeyboardCode::KC_NUMPAD_INSERT: return KeyboardCode(XK_KP_Insert);
		case KeyboardCode::KC_NUMPAD_DELETE: return KeyboardCode(XK_KP_Delete);
		case KeyboardCode::KC_NUMPAD_EQUAL: return KeyboardCode(XK_KP_Equal);
		case KeyboardCode::KC_NUMPAD_MULTIPLY: return KeyboardCode(XK_KP_Multiply);
		case KeyboardCode::KC_NUMPAD_ADD: return KeyboardCode(XK_KP_Add);
		case KeyboardCode::KC_NUMPAD_SEPARATOR: return KeyboardCode(XK_KP_Separator);
		case KeyboardCode::KC_NUMPAD_SUBTRACT: return KeyboardCode(XK_KP_Subtract);
		case KeyboardCode::KC_NUMPAD_DECIMAL: return KeyboardCode(XK_KP_Decimal);
		case KeyboardCode::KC_NUMPAD_DIVIDE: return KeyboardCode(XK_KP_Divide);		
		case KeyboardCode::KC_WINDOWS_LEFT: return KeyboardCode(XK_Super_L);
		case KeyboardCode::KC_WINDOWS_RIGHT: return KeyboardCode(XK_Super_R);
		case KeyboardCode::KC_WINDOWS_MENU: return KeyboardCode(XK_Menu);
		default:
			//assert (false);
			return KeyboardCode(0);
	}
#else
	// Make sure input value is translatable
	if (EUnicode2VirtualKeyCode(kc)) return KeyboardCode(kc);
	else return KeyboardCode(0);
#endif
}

// Given the name of the key (which is formed removing th prepending KC_ of the
// entries in the EUnicode enumeration or its printed character (only ascii standard 
// characters are supported currently) returns its KeyboardCode. 

static struct name2eunicode {
	const char *name;
	unsigned int code;
} name2eunicode_tab[] = {
	{ "ADD", KeyboardCode::KC_ADD },
	{ "ALT", KeyboardCode::KC_ALT },
	{ "BACK", KeyboardCode::KC_BACK },
	{ "CANCEL", KeyboardCode::KC_CANCEL },
	{ "CAPSLOCK", KeyboardCode::KC_CAPSLOCK },
	{ "CLEAR", KeyboardCode::KC_CLEAR },
	{ "CONTROL", KeyboardCode::KC_CONTROL },
	{ "DECIMAL", KeyboardCode::KC_DECIMAL },
	{ "DELETE", KeyboardCode::KC_DELETE },
	{ "DIVIDE", KeyboardCode::KC_DIVIDE },
	{ "DOWN", KeyboardCode::KC_DOWN },
	{ "END", KeyboardCode::KC_END },
	{ "ESCAPE", KeyboardCode::KC_ESCAPE },
	{ "EXECUTE", KeyboardCode::KC_EXECUTE },
	{ "F1", KeyboardCode::KC_F1 },
	{ "F10", KeyboardCode::KC_F10 },
	{ "F11", KeyboardCode::KC_F11 },
	{ "F12", KeyboardCode::KC_F12 },
	{ "F13", KeyboardCode::KC_F13 },
	{ "F14", KeyboardCode::KC_F14 },
	{ "F15", KeyboardCode::KC_F15 },
	{ "F16", KeyboardCode::KC_F16 },
	{ "F17", KeyboardCode::KC_F17 },
	{ "F18", KeyboardCode::KC_F18 },
	{ "F19", KeyboardCode::KC_F19 },
	{ "F2", KeyboardCode::KC_F2 },
	{ "F20", KeyboardCode::KC_F20 },
	{ "F21", KeyboardCode::KC_F21 },
	{ "F22", KeyboardCode::KC_F22 },
	{ "F23", KeyboardCode::KC_F23 },
	{ "F24", KeyboardCode::KC_F24 },
	{ "F3", KeyboardCode::KC_F3 },
	{ "F4", KeyboardCode::KC_F4 },
	{ "F5", KeyboardCode::KC_F5 },
	{ "F6", KeyboardCode::KC_F6 },
	{ "F7", KeyboardCode::KC_F7 },
	{ "F8", KeyboardCode::KC_F8 },
	{ "F9", KeyboardCode::KC_F9 },
	{ "FIRST", KeyboardCode::KC_FIRST },
	{ "HELP", KeyboardCode::KC_HELP },
	{ "HOME", KeyboardCode::KC_HOME },
	{ "INSERT", KeyboardCode::KC_INSERT },
	{ "LALT", KeyboardCode::KC_LALT },
	{ "LCONTROL", KeyboardCode::KC_LCONTROL },
	{ "LEFT", KeyboardCode::KC_LEFT },
	{ "LSHIFT", KeyboardCode::KC_LSHIFT },
	{ "MENU", KeyboardCode::KC_MENU },
	{ "MULTIPLY", KeyboardCode::KC_MULTIPLY },
	{ "NUMLOCK", KeyboardCode::KC_NUMLOCK },
	{ "NUMPAD0", KeyboardCode::KC_NUMPAD0 },
	{ "NUMPAD1", KeyboardCode::KC_NUMPAD1 },
	{ "NUMPAD2", KeyboardCode::KC_NUMPAD2 },
	{ "NUMPAD3", KeyboardCode::KC_NUMPAD3 },
	{ "NUMPAD4", KeyboardCode::KC_NUMPAD4 },
	{ "NUMPAD5", KeyboardCode::KC_NUMPAD5 },
	{ "NUMPAD6", KeyboardCode::KC_NUMPAD6 },
	{ "NUMPAD7", KeyboardCode::KC_NUMPAD7 },
	{ "NUMPAD8", KeyboardCode::KC_NUMPAD8 },
	{ "NUMPAD9", KeyboardCode::KC_NUMPAD9 },
	{ "NUMPAD_ADD", KeyboardCode::KC_NUMPAD_ADD },
	{ "NUMPAD_BEGIN", KeyboardCode::KC_NUMPAD_BEGIN },
	{ "NUMPAD_DECIMAL", KeyboardCode::KC_NUMPAD_DECIMAL },
	{ "NUMPAD_DELETE", KeyboardCode::KC_NUMPAD_DELETE },
	{ "NUMPAD_DIVIDE", KeyboardCode::KC_NUMPAD_DIVIDE },
	{ "NUMPAD_DOWN", KeyboardCode::KC_NUMPAD_DOWN },
	{ "NUMPAD_END", KeyboardCode::KC_NUMPAD_END },
	{ "NUMPAD_ENTER", KeyboardCode::KC_NUMPAD_ENTER },
	{ "NUMPAD_EQUAL", KeyboardCode::KC_NUMPAD_EQUAL },
	{ "NUMPAD_HOME", KeyboardCode::KC_NUMPAD_HOME },
	{ "NUMPAD_INSERT", KeyboardCode::KC_NUMPAD_INSERT },
	{ "NUMPAD_LEFT", KeyboardCode::KC_NUMPAD_LEFT },
	{ "NUMPAD_MULTIPLY", KeyboardCode::KC_NUMPAD_MULTIPLY },
	{ "NUMPAD_PAGEDOWN", KeyboardCode::KC_NUMPAD_PAGEDOWN },
	{ "NUMPAD_PAGEUP", KeyboardCode::KC_NUMPAD_PAGEUP },
	{ "NUMPAD_RIGHT", KeyboardCode::KC_NUMPAD_RIGHT },
	{ "NUMPAD_SEPARATOR", KeyboardCode::KC_NUMPAD_SEPARATOR },
	{ "NUMPAD_SPACE", KeyboardCode::KC_NUMPAD_SPACE },
	{ "NUMPAD_SUBTRACT", KeyboardCode::KC_NUMPAD_SUBTRACT },
	{ "NUMPAD_TAB", KeyboardCode::KC_NUMPAD_TAB },
	{ "NUMPAD_UP", KeyboardCode::KC_NUMPAD_UP },
	{ "PAGEDOWN", KeyboardCode::KC_PAGEDOWN },
	{ "PAGEUP", KeyboardCode::KC_PAGEUP },
	{ "PAUSE", KeyboardCode::KC_PAUSE },
	{ "PRINT", KeyboardCode::KC_PRINT },
	{ "RALT", KeyboardCode::KC_RALT },
	{ "RCONTROL", KeyboardCode::KC_RCONTROL },
	{ "RETURN", KeyboardCode::KC_RETURN },
	{ "RIGHT", KeyboardCode::KC_RIGHT },
	{ "RSHIFT", KeyboardCode::KC_RSHIFT },
	{ "SCROLL", KeyboardCode::KC_SCROLL },
	{ "SELECT", KeyboardCode::KC_SELECT },
	{ "SEPARATOR", KeyboardCode::KC_SEPARATOR },
	{ "SHIFT", KeyboardCode::KC_SHIFT },
	{ "SNAPSHOT", KeyboardCode::KC_SNAPSHOT },
	{ "SPACE", KeyboardCode::KC_SPACE },
	{ "SUBTRACT", KeyboardCode::KC_SUBTRACT },
	{ "TAB", KeyboardCode::KC_TAB },
	{ "UP", KeyboardCode::KC_UP },
	{ "WINDOWS_LEFT", KeyboardCode::KC_WINDOWS_LEFT },
	{ "WINDOWS_MENU", KeyboardCode::KC_WINDOWS_MENU },
	{ "WINDOWS_RIGHT", KeyboardCode::KC_WINDOWS_RIGHT }
};

#define NAME2ENICODE_TAB_SIZE (sizeof(name2eunicode_tab)/sizeof(struct name2eunicode))

KeyboardCode KeyboardCode::FromKeyName (const char *kname)
{
	// Lookup in name2eunicode_tab
	char kname_up[256];
	int i= 0;
	for (; kname[i] && i< 255; i++)
		kname_up[i]= (char) toupper(kname[i]);
	kname_up[i]= 0;
	
	int min = 0;
	int max = NAME2ENICODE_TAB_SIZE - 1;
	int mid;

	// binary search in table
	while (max >= min) {
		mid = (min + max) / 2;
		int cmp= strcmp(name2eunicode_tab[mid].name, kname_up);
		if (cmp< 0) min = mid + 1;
		else if (cmp> 0) max = mid - 1;
		else {
			// found it
			return KeyboardCode::FromEUnicode(name2eunicode_tab[mid].code);
		}
	}

#if !defined(WIN32)
	if (*kname && kname[1]== 0) {
		KeySym ks= ucs2keysym(*kname);
		if (ks) return KeyboardCode(ks);
	}

	KeySym ks= XStringToKeysym(kname);
	if (ks!= NoSymbol) return KeyboardCode::FromVirtualKeyCode(ks);
#else
	// On Windows and when only 1 character passed
	if (*kname && kname[1]== 0) {
		const char c= *kname;
		if (EUnicode2VirtualKeyCode(c)) return KeyboardCode(c);
	}
#endif

	// Not found. 
	return KeyboardCode(0);
}

KeyboardCode KeyboardCode::FromRawValue (unsigned long kc)
{
	return FromVirtualKeyCode (kc);
}

// Get the internal raw value. NOTE: intended only
// for storage purposes not to work with
unsigned long KeyboardCode::GetRawValue() const
{
	return m_keyCode;
}

#if !defined(NDEBUG)
#include <stdio.h>
void KeyboardCode::Dump() const
{
#if !defined(WIN32)
	printf ("KeySym: %lu	KeyCode: %u Name: %s\n", m_keyCode, XKeysymToKeycode(getDisplay(), m_keyCode), GetName());
#else
	printf ("V. Key Code: %u Name: %s\n", m_keyCode, GetName());
#endif
}
#endif


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef USING_WXWIDGETS

#error Code not tested

// Given an wxKeyCode code returns the corresponding KeyboardCode object
KeyboardCode KeyboardCode::FromWXKeyCode (wxKeyCode kc)
{
#if !defined(WIN32)
	switch (kc) {
		case WXK_BACK: return KeyboardCode(XK_BackSpace);
		case WXK_TAB: return KeyboardCode(XK_Tab);
		case WXK_RETURN: return KeyboardCode(XK_Return);
		case WXK_ESCAPE: return KeyboardCode(XK_Escape);		
		case WXK_DELETE: return KeyboardCode(XK_Delete);
		case WXK_CLEAR: return KeyboardCode(XK_Clear);
		case WXK_SHIFT: return KeyboardCode(XK_Shift_L);
		case WXK_ALT: return KeyboardCode(XK_Alt_L);
		case WXK_CONTROL: return KeyboardCode(XK_Control_L);
		case WXK_MENU: return KeyboardCode(XK_Menu);
		case WXK_PAUSE: return KeyboardCode(XK_Pause);
		case WXK_END: return KeyboardCode(XK_End);
		case WXK_HOME: return KeyboardCode(XK_Home);
		case WXK_LEFT: return KeyboardCode(XK_Left);
		case WXK_UP: return KeyboardCode(XK_Up);
		case WXK_RIGHT: return KeyboardCode(XK_Right);
		case WXK_DOWN: return KeyboardCode(XK_Down);
		case WXK_SELECT: return KeyboardCode(XK_Select);
		case WXK_PRINT: return KeyboardCode(XK_Print);
		case WXK_EXECUTE: return KeyboardCode(XK_Execute);
		case WXK_SNAPSHOT: return KeyboardCode(XK_Print);
		case WXK_INSERT: return KeyboardCode(XK_Insert);
		case WXK_HELP: return KeyboardCode(XK_Help);
		case WXK_NUMPAD0: return KeyboardCode(XK_KP_0);
		case WXK_NUMPAD1: return KeyboardCode(XK_KP_1);
		case WXK_NUMPAD2: return KeyboardCode(XK_KP_2);
		case WXK_NUMPAD3: return KeyboardCode(XK_KP_3);
		case WXK_NUMPAD4: return KeyboardCode(XK_KP_4);
		case WXK_NUMPAD5: return KeyboardCode(XK_KP_5);
		case WXK_NUMPAD6: return KeyboardCode(XK_KP_6);
		case WXK_NUMPAD7: return KeyboardCode(XK_KP_7);
		case WXK_NUMPAD8: return KeyboardCode(XK_KP_8);
		case WXK_NUMPAD9: return KeyboardCode(XK_KP_9);
		case WXK_MULTIPLY: return KeyboardCode(XK_KP_Multiply);
		case WXK_ADD: return KeyboardCode(XK_KP_Add);
		case WXK_SEPARATOR: return KeyboardCode(XK_KP_Separator);
		case WXK_SUBTRACT: return KeyboardCode(XK_KP_Subtract);
		case WXK_DECIMAL: return KeyboardCode(XK_KP_Decimal);
		case WXK_DIVIDE: return KeyboardCode(XK_KP_Divide);
		case WXK_F1: return KeyboardCode(XK_F1);
		case WXK_F2: return KeyboardCode(XK_F2);
		case WXK_F3: return KeyboardCode(XK_F3);
		case WXK_F4: return KeyboardCode(XK_F4);
		case WXK_F5: return KeyboardCode(XK_F5);
		case WXK_F6: return KeyboardCode(XK_F6);
		case WXK_F7: return KeyboardCode(XK_F7);
		case WXK_F8: return KeyboardCode(XK_F8);
		case WXK_F9: return KeyboardCode(XK_F9);
		case WXK_F10: return KeyboardCode(XK_F10);
		case WXK_F11: return KeyboardCode(XK_F11);
		case WXK_F12: return KeyboardCode(XK_F12);
		case WXK_F13: return KeyboardCode(XK_F13);
		case WXK_F14: return KeyboardCode(XK_F14);
		case WXK_F15: return KeyboardCode(XK_F15);
		case WXK_F16: return KeyboardCode(XK_F16);
		case WXK_F17: return KeyboardCode(XK_F17);
		case WXK_F18: return KeyboardCode(XK_F18);
		case WXK_F19: return KeyboardCode(XK_F19);
		case WXK_F20: return KeyboardCode(XK_F20);
		case WXK_F21: return KeyboardCode(XK_F21);
		case WXK_F22: return KeyboardCode(XK_F22);
		case WXK_F23: return KeyboardCode(XK_F23);
		case WXK_F24: return KeyboardCode(XK_F24);
		case WXK_NUMLOCK: return KeyboardCode(XK_Num_Lock);
		case WXK_SCROLL: return KeyboardCode(XK_Scroll_Lock);
		case WXK_PAGEUP: return KeyboardCode(XK_Page_Up);
		case WXK_PAGEDOWN: return KeyboardCode(XK_Page_Down); 
		case WXK_NUMPAD_SPACE: return KeyboardCode(XK_KP_Space);
		case WXK_NUMPAD_TAB: return KeyboardCode(XK_KP_Tab);
		case WXK_NUMPAD_ENTER: return KeyboardCode(XK_KP_Enter);
		case WXK_NUMPAD_F1: return KeyboardCode(XK_KP_F1);
		case WXK_NUMPAD_F2: return KeyboardCode(XK_KP_F2);
		case WXK_NUMPAD_F3: return KeyboardCode(XK_KP_F3);
		case WXK_NUMPAD_F4: return KeyboardCode(XK_KP_F4);
		case WXK_NUMPAD_HOME: return KeyboardCode(XK_KP_Home);
		case WXK_NUMPAD_LEFT: return KeyboardCode(XK_KP_Left);
		case WXK_NUMPAD_UP: return KeyboardCode(XK_KP_Up);
		case WXK_NUMPAD_RIGHT: return KeyboardCode(XK_KP_Right);
		case WXK_NUMPAD_DOWN: return KeyboardCode(XK_KP_Down);
		case WXK_NUMPAD_PAGEUP: return KeyboardCode(XK_KP_Page_Up);
		case WXK_NUMPAD_PAGEDOWN: return KeyboardCode(XK_KP_Page_Down);
		case WXK_NUMPAD_END: return KeyboardCode(XK_KP_End);
		case WXK_NUMPAD_BEGIN: return KeyboardCode(XK_KP_Begin);
		case WXK_NUMPAD_INSERT: return KeyboardCode(XK_KP_Insert);
		case WXK_NUMPAD_DELETE: return KeyboardCode(XK_KP_Delete);
		case WXK_NUMPAD_EQUAL: return KeyboardCode(XK_KP_Equal);
		case WXK_NUMPAD_MULTIPLY: return KeyboardCode(XK_KP_Multiply);
		case WXK_NUMPAD_ADD: return KeyboardCode(XK_KP_Add);
		case WXK_NUMPAD_SEPARATOR: return KeyboardCode(XK_KP_Separator);
		case WXK_NUMPAD_SUBTRACT: return KeyboardCode(XK_KP_Subtract);
		case WXK_NUMPAD_DECIMAL: return KeyboardCode(XK_KP_Decimal);
		case WXK_NUMPAD_DIVIDE: return KeyboardCode(XK_KP_Divide);		
		// the following key codes are only generated under Windows currently
		case WXK_WINDOWS_LEFT: return KeyboardCode(XK_Super_L);
		case WXK_WINDOWS_RIGHT: return KeyboardCode(XK_Super_R);
		case WXK_WINDOWS_MENU: return KeyboardCode(XK_Menu);
		case WXK_SPACE: return KeyboardCode(XK_KP_Space);
		case WXK_CANCEL: return KeyboardCode(XK_Break);
		case WXK_CAPITAL: return KeyboardCode(XK_Caps_Lock);

		//
		// Untranslated codes
		//		
		case WXK_START:
		case WXK_LBUTTON:
		case WXK_RBUTTON:		
		case WXK_MBUTTON:
		case WXK_COMMAND:

		// Hardware-specific buttons
		case WXK_SPECIAL1:
		case WXK_SPECIAL2:
		case WXK_SPECIAL3:
		case WXK_SPECIAL4:
		case WXK_SPECIAL5:
		case WXK_SPECIAL6:
		case WXK_SPECIAL7:
		case WXK_SPECIAL8:
		case WXK_SPECIAL9:
		case WXK_SPECIAL10:
		case WXK_SPECIAL11:
		case WXK_SPECIAL12:
		case WXK_SPECIAL13:
		case WXK_SPECIAL14:
		case WXK_SPECIAL15:
		case WXK_SPECIAL16:
		case WXK_SPECIAL17:
		case WXK_SPECIAL18:
		case WXK_SPECIAL19:
		case WXK_SPECIAL20:
			assert (false);
			return KeyboardCode(0);
			break;
	}

	// If the code reaches this point it means that wxKeyCode kc carries 
	// an unicode character (see wxKeyEvent::GetUnicodeKey)
	// TODO: translate these unicode char to a keysym
	return KeyboardCode(0);
#else
	
	switch (kc) {
		case WXK_BACK: return KeyboardCode(VK_BACK);
		case WXK_TAB: return KeyboardCode(VK_TAB);			
		case WXK_RETURN: return KeyboardCode(VK_RETURN);
		case WXK_ESCAPE: return KeyboardCode(VK_ESCAPE);
		case WXK_DELETE: return KeyboardCode(VK_DELETE);
		case WXK_CLEAR: return KeyboardCode(VK_CLEAR);
		case WXK_SHIFT: return KeyboardCode(VK_SHIFT);
		case WXK_ALT: return KeyboardCode(VK_MENU);
		case WXK_CONTROL: return KeyboardCode(VK_CONTROL);
		case WXK_MENU: return KeyboardCode(VK_MENU);
		case WXK_PAUSE: return KeyboardCode(VK_PAUSE);
		case WXK_END: return KeyboardCode(VK_END);
		case WXK_HOME: return KeyboardCode(VK_HOME);
		case WXK_LEFT: return KeyboardCode(VK_LEFT);
		case WXK_UP: return KeyboardCode(VK_UP);
		case WXK_RIGHT: return KeyboardCode(VK_RIGHT);
		case WXK_DOWN: return KeyboardCode(VK_DOWN);
		case WXK_SELECT: return KeyboardCode(VK_SELECT);
		case WXK_PRINT: return KeyboardCode(VK_PRINT);
		case WXK_EXECUTE: return KeyboardCode(VK_EXECUTE);
		case WXK_SNAPSHOT: return KeyboardCode(VK_SNAPSHOT);
		case WXK_INSERT: return KeyboardCode(VK_INSERT);
		case WXK_HELP: return KeyboardCode(VK_HELP);
		case WXK_NUMPAD0: return KeyboardCode(VK_NUMPAD0);
		case WXK_NUMPAD1: return KeyboardCode(VK_NUMPAD1);
		case WXK_NUMPAD2: return KeyboardCode(VK_NUMPAD2);
		case WXK_NUMPAD3: return KeyboardCode(VK_NUMPAD3);
		case WXK_NUMPAD4: return KeyboardCode(VK_NUMPAD4);
		case WXK_NUMPAD5: return KeyboardCode(VK_NUMPAD5);
		case WXK_NUMPAD6: return KeyboardCode(VK_NUMPAD6);
		case WXK_NUMPAD7: return KeyboardCode(VK_NUMPAD7);
		case WXK_NUMPAD8: return KeyboardCode(VK_NUMPAD8);
		case WXK_NUMPAD9: return KeyboardCode(VK_NUMPAD9);
		case WXK_MULTIPLY: return KeyboardCode(VK_MULTIPLY);
		case WXK_ADD: return KeyboardCode(VK_ADD);
		case WXK_SEPARATOR: return KeyboardCode(VK_SEPARATOR);
		case WXK_SUBTRACT: return KeyboardCode(VK_SUBTRACT);
		case WXK_DECIMAL: return KeyboardCode(VK_DECIMAL);
		case WXK_DIVIDE: return KeyboardCode(VK_DIVIDE);
		case WXK_F1: return KeyboardCode(VK_F1);
		case WXK_F2: return KeyboardCode(VK_F2);
		case WXK_F3: return KeyboardCode(VK_F3);
		case WXK_F4: return KeyboardCode(VK_F4);
		case WXK_F5: return KeyboardCode(VK_F5);
		case WXK_F6: return KeyboardCode(VK_F6);
		case WXK_F7: return KeyboardCode(VK_F7);
		case WXK_F8: return KeyboardCode(VK_F8);
		case WXK_F9: return KeyboardCode(VK_F9);
		case WXK_F10: return KeyboardCode(VK_F10);
		case WXK_F11: return KeyboardCode(VK_F11);
		case WXK_F12: return KeyboardCode(VK_F12);
		case WXK_F13: return KeyboardCode(VK_F13);
		case WXK_F14: return KeyboardCode(VK_F14);
		case WXK_F15: return KeyboardCode(VK_F15);
		case WXK_F16: return KeyboardCode(VK_F16);
		case WXK_F17: return KeyboardCode(VK_F17);
		case WXK_F18: return KeyboardCode(VK_F18);
		case WXK_F19: return KeyboardCode(VK_F19);
		case WXK_F20: return KeyboardCode(VK_F20);
		case WXK_F21: return KeyboardCode(VK_F21);
		case WXK_F22: return KeyboardCode(VK_F22);
		case WXK_F23: return KeyboardCode(VK_F23);
		case WXK_F24: return KeyboardCode(VK_F24);
		case WXK_NUMLOCK: return KeyboardCode(VK_NUMLOCK);
		case WXK_SCROLL: return KeyboardCode(VK_SCROLL);
		case WXK_PAGEUP: return KeyboardCode(VK_PRIOR);
		case WXK_PAGEDOWN: return KeyboardCode(VK_NEXT);
		case WXK_NUMPAD_SPACE: return KeyboardCode(VK_SPACE);
		case WXK_NUMPAD_TAB: return KeyboardCode(VK_TAB);
		case WXK_NUMPAD_ENTER: return KeyboardCode(VK_RETURN);
		case WXK_NUMPAD_F1: return KeyboardCode(VK_F1);
		case WXK_NUMPAD_F2: return KeyboardCode(VK_F2);
		case WXK_NUMPAD_F3: return KeyboardCode(VK_F3);
		case WXK_NUMPAD_F4: return KeyboardCode(VK_F4);
		case WXK_NUMPAD_HOME: return KeyboardCode(VK_HOME);
		case WXK_NUMPAD_LEFT: return KeyboardCode(VK_LEFT);
		case WXK_NUMPAD_UP: return KeyboardCode(VK_UP);
		case WXK_NUMPAD_RIGHT: return KeyboardCode(VK_RIGHT);
		case WXK_NUMPAD_DOWN: return KeyboardCode(VK_DOWN);
		case WXK_NUMPAD_PAGEUP: return KeyboardCode(VK_PRIOR);
		case WXK_NUMPAD_PAGEDOWN: return KeyboardCode(VK_NEXT);
		case WXK_NUMPAD_END: return KeyboardCode(VK_END);
		case WXK_NUMPAD_BEGIN: return KeyboardCode(VK_NUMPAD5);
		case WXK_NUMPAD_INSERT: return KeyboardCode(VK_INSERT);
		case WXK_NUMPAD_DELETE: return KeyboardCode(VK_DELETE);		
		case WXK_NUMPAD_MULTIPLY: return KeyboardCode(VK_MULTIPLY);
		case WXK_NUMPAD_ADD: return KeyboardCode(VK_ADD);
		case WXK_NUMPAD_SEPARATOR: return KeyboardCode(VK_SEPARATOR);
		case WXK_NUMPAD_SUBTRACT: return KeyboardCode(VK_SUBTRACT);
		case WXK_NUMPAD_DECIMAL: return KeyboardCode(VK_DECIMAL);
		case WXK_NUMPAD_DIVIDE: return KeyboardCode(VK_DIVIDE);
		
		// the following key codes are only generated under Windows currently
		case WXK_WINDOWS_LEFT: return KeyboardCode(VK_LWIN);
		case WXK_WINDOWS_RIGHT: return KeyboardCode(VK_RWIN);
		case WXK_WINDOWS_MENU: return KeyboardCode(VK_APPS);
		case WXK_SPACE: return KeyboardCode(VK_SPACE);
		case WXK_CANCEL: return KeyboardCode(VK_CANCEL);
		case WXK_CAPITAL: return KeyboardCode(VK_CAPITAL);

		//
		// Untranslated codes
		//		
		case WXK_NUMPAD_EQUAL:
		case WXK_START:
		case WXK_LBUTTON:
		case WXK_RBUTTON:		
		case WXK_MBUTTON:
		case WXK_COMMAND:

		// Hardware-specific buttons
		case WXK_SPECIAL1:
		case WXK_SPECIAL2:
		case WXK_SPECIAL3:
		case WXK_SPECIAL4:
		case WXK_SPECIAL5:
		case WXK_SPECIAL6:
		case WXK_SPECIAL7:
		case WXK_SPECIAL8:
		case WXK_SPECIAL9:
		case WXK_SPECIAL10:
		case WXK_SPECIAL11:
		case WXK_SPECIAL12:
		case WXK_SPECIAL13:
		case WXK_SPECIAL14:
		case WXK_SPECIAL15:
		case WXK_SPECIAL16:
		case WXK_SPECIAL17:
		case WXK_SPECIAL18:
		case WXK_SPECIAL19:
		case WXK_SPECIAL20:
			assert (false);
			return KeyboardCode(0);
			break;
	}

	return KeyboardCode(0);
#endif
}
#endif
