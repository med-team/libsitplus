/////////////////////////////////////////////////////////////////////////////
// File:        language.cpp
// Author:      Cesar Mauri Loba (cesar at crea-si dot com)
// Copyright:   (C) 2010-10 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////
#include "spcore/language.h"
#include "spcore/coreruntime.h"

#include <clocale>
#include <vector>
#include <string>
#include <iostream>
#include <boost/static_assert.hpp>
#include <libintl.h>

/*	
	Some versions of libintl.h for Win32 provide macros for printf 
	funtion family. Those macros can cause crashes on Windows so
	we do not want them.
*/
#ifdef printf
	#error "libintl.h defines printf"
#endif

#define ARRAY_SIZE(foo) (sizeof(foo)/sizeof(foo[0]))

/*
	Language id's and names
*/
typedef struct {
	const char* locale;
	const char* native_name;
	const char* winName;
}  LangInfo;

// NOTE: this file is encoded using UTF8
static LangInfo g_supported_lang_ids[]= {
	{ "", N__("System default"), ""},	// spLANGUAGE_DEFAULT,
	{ "C", "", ""},						// spLANGUAGE_UNKNOWN,		
	{ "eu_ES", "Euskara", "Basque" },	// spLANGUAGE_BASQUE,
	{ "ca_ES", "Català", "Catalan" },	// spLANGUAGE_CATALAN,
	{ "zh_TW", "汉语", "Chinese" },		// spLANGUAGE_CHINESE,
	{ "en_GB", "English", "English" },	// spLANGUAGE_ENGLISH,
	{ "fr_FR", "Français", "French" },	// spLANGUAGE_FRENCH,
	{ "gl_ES", "Galego", "Galician" },	// spLANGUAGE_GALICIAN,
	{ "de_DE", "Deutsch", "German" },	// spLANGUAGE_GERMAN,
	{ "el_GR", "Ελληνικά", "Greek" },	// spLANGUAGE_GREEK,
	{ "he_IL", "עברית", "Hebrew" },		// spLANGUAGE_HEBREW,
	{ "it_IT", "Italino", "Italian" },	// spLANGUAGE_ITALIAN,
	{ "ja_JP", "日本語", "Japanese" },	// spLANGUAGE_JAPANESE,
	{ "pt_PT", "Português", "Portuguese" },	// spLANGUAGE_PORTUGUESE,
	{ "ru_RU", "Русский", "Russian" },	// spLANGUAGE_RUSSIAN,
	{ "es_ES", "Español", "Spanish" },		// spLANGUAGE_SPANISH,
	{ "sv_SE", "Svenska", "Swedish" },		// spLANGUAGE_SWEDISH,
	{ "tr_TR", "Türkçe", "Turkish" }		// spLANGUAGE_TURKISH
};

BOOST_STATIC_ASSERT(ARRAY_SIZE(g_supported_lang_ids)== spLANGUAGE_LAST);

#ifdef ENABLE_WXWIDGETS
static const int g_supported_lang_wxids[]= { 
	wxLANGUAGE_DEFAULT,
	wxLANGUAGE_UNKNOWN,		
	wxLANGUAGE_BASQUE,
	wxLANGUAGE_CATALAN,
	wxLANGUAGE_CHINESE,
	wxLANGUAGE_ENGLISH,
	wxLANGUAGE_FRENCH,
	wxLANGUAGE_GALICIAN,
	wxLANGUAGE_GERMAN,
	wxLANGUAGE_GREEK,
	wxLANGUAGE_HEBREW,
	wxLANGUAGE_ITALIAN,
	wxLANGUAGE_JAPANESE,
	wxLANGUAGE_PORTUGUESE,
	wxLANGUAGE_RUSSIAN,
	wxLANGUAGE_SPANISH,
	wxLANGUAGE_SWEDISH,
	wxLANGUAGE_TURKISH
};
BOOST_STATIC_ASSERT(ARRAY_SIZE(g_supported_lang_ids)== ARRAY_SIZE(g_supported_lang_wxids));
#endif

static int g_current_lang_id= -1;

// List of domains
static std::vector<std::string> g_text_domains;

#ifdef WIN32

// Functions in win32env.cpp
extern int win32_putenv(const char *envval);
extern void win32_unsetenv(const char *name);

#ifndef _WIN32_WINNT
	#error "_WIN32_WINNT not defined"
#endif
#include <windows.h>

#endif	// WIN32

// Code based on language.cpp from http://svn.gna.org/viewcvs/wesnoth/trunk/
// TODO: consider using tinygettext on Win32 due to the problems which gettext has
static 
bool sp_setlocale(int category, int id, std::vector<std::string> const *alternates)
{
	std::string locale = g_supported_lang_ids[id].locale;

#ifdef _WIN32	
	{
		// Pick language part (2 or 3 chars)
		char gettextloc[4];
		gettextloc[0]= 0;
		unsigned int i= 0;
		for (; i< 3 && i< locale.size() && locale[i]!='_' && locale[i]!='@' && locale[i]!= '.'; ++i)
			gettextloc[i]= locale[i];
		gettextloc[i]= 0;

		if(category == LC_MESSAGES) {
			// For windows we need to define the environtment variable LANGUAGE
			std::string env = std::string("LANGUAGE=") + gettextloc;
			win32_putenv(env.c_str());
			//SetEnvironmentVariableA("LANG", locale.c_str());
			return true;
		}

		// Get Win32 language name
		locale= g_supported_lang_ids[id].winName;
	}
#endif

	char *res = NULL;
	std::vector<std::string>::const_iterator i;
	if (alternates) i = alternates->begin();

	for (;;) {
		std::string lang = locale, extra;
		std::string::size_type pos = locale.find('@');
		if (pos != std::string::npos) {
			lang.erase(pos);
			extra = locale.substr(pos);
		}

		/*
		 * The "" is the last item to work-around a problem in glibc picking
		 * the non utf8 locale instead an utf8 version if available.
		 */
		char const *encoding[] = { ".utf-8", ".UTF-8", "" };
		for (int j = 0; j != 3; ++j) {
			locale = lang + encoding[j] + extra;
			res = std::setlocale(category, locale.c_str());
			if (res) return true;	// OK
		}

		if (!alternates || i == alternates->end()) break;
		locale = *i;
		++i;
	}

	std::cerr << "setlocale() failed for '" << g_supported_lang_ids[id].locale << "'.\n";
	/*
#ifndef _WIN32
#ifndef __AMIGAOS4__
	if(category == LC_MESSAGES) {
		std::cerr << "Setting LANGUAGE to '" << slocale << "'.\n";
		setenv("LANGUAGE", slocale.c_str(), 1);
		std::setlocale(LC_MESSAGES, "");
	}
#endif
#endif*/

	//done:
	//std::cerr << "Numeric locale: " << std::setlocale(LC_NUMERIC, NULL) << '\n';
	//std::cerr << "Full locale: " << std::setlocale(LC_ALL, NULL) << '\n';

	return false;
}

#ifdef ENABLE_WXWIDGETS
static
wxLocale& GetWxLocale()
{
	static wxLocale locale;
	return locale;
}
#endif

SPEXPORT_FUNCTION
const char * spGettext (const char * msgid)
{
	std::vector<std::string>::const_iterator it= g_text_domains.begin();
	while (it!= g_text_domains.end()) {
		const char * retval= dgettext(it->c_str(), msgid);
		if (retval!= msgid) return retval;
		it++;
	}
	return msgid;
}

SPEXPORT_FUNCTION
const char * spDGettext (const char * domainname, const char * msgid)
{
	return dgettext(domainname, msgid);
}

SPEXPORT_FUNCTION
int spSetLanguage (int id)
{
	if (id< spLANGUAGE_FIRST || id> spLANGUAGE_LAST || g_current_lang_id!= -1) return -1;

	g_current_lang_id= id;

#ifdef WIN32
	// Make sure associated runtime library is loaded
	gettext(NULL);
#endif

	// configuration for gettext
	// TODO: check errors
	sp_setlocale(LC_COLLATE, id, NULL);
	sp_setlocale(LC_TIME, id, NULL);
	sp_setlocale(LC_MESSAGES, id, NULL);

#ifdef ENABLE_WXWIDGETS
	// configuration for wx
	// TODO: consider using wxLOCALE_LOAD_DEFAULT flags (loads wxstd catalog)
	if (!GetWxLocale().Init(g_supported_lang_wxids[id], 0)) return -1;
#endif

	return 0;
}

SPEXPORT_FUNCTION
int spGetLanguage () { return g_current_lang_id; }

SPEXPORT_FUNCTION
bool isLanguageValid (int id) {
	if (id< spLANGUAGE_FIRST || id> spLANGUAGE_LAST) return false;

	return true;
}

SPEXPORT_FUNCTION
int spBindTextDomain(const char* domain, const char* dirname)
{
#ifdef ENABLE_WXWIDGETS
	GetWxLocale().AddCatalogLookupPathPrefix(wxString(dirname, wxConvUTF8));
	if (!GetWxLocale().AddCatalog(wxString(domain, wxConvUTF8))) return -1;
#endif
	bindtextdomain( domain, dirname );
	bind_textdomain_codeset (domain, "UTF-8");
	g_text_domains.push_back(domain);
	return 0;
}

SPEXPORT_FUNCTION
int spGetAvailableLanguages()
{
	return (spLANGUAGE_LAST - spLANGUAGE_FIRST);
}

SPEXPORT_FUNCTION
const char* spGetLocaleId (int id)
{
	if (!isLanguageValid(id)) return NULL;
	
	return g_supported_lang_ids[id].locale;
}

SPEXPORT_FUNCTION
int spResolveLocaleId (const char* locale)
{
	if (!locale) return -1;

	for (int i= 0; i< spGetAvailableLanguages(); ++i)
		if (strcmp(locale, g_supported_lang_ids[i].locale)== 0) return (int) i;
	
	return -1;
}

SPEXPORT_FUNCTION
const char* spGetLanguageNativeName (int id, const char* domain)
{
	if (!isLanguageValid(id)) return NULL;

	if (id== spLANGUAGE_DEFAULT) {
		if (domain) return dgettext(domain, g_supported_lang_ids[id].native_name);
		else return gettext(g_supported_lang_ids[id].native_name);
	}

	return g_supported_lang_ids[id].native_name;
}

