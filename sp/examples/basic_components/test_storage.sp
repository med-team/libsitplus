type test_storage
name "test_storage"

# int storage
create int int_storage -v 10
create widget_slider int_sld --min -100 --max 100 -v 0 -l "Set int value" -i
connect int_sld value int_storage store
create widget_button int_btn -l "Send int"
connect int_btn pressed int_storage retrieve
create print int_print "int print:"
connect int_storage out int_print in

# float
create float float_storage -v 10.25
create widget_slider float_sld --min 0 --max 100 -v 0 --log -l "Set float value"
connect float_sld value float_storage store
create widget_button float_btn -l "Send float"
connect float_btn pressed float_storage retrieve
create print float_print "float print:"
connect float_storage out float_print in

# bool
create bool bool_storage
create bool bool_storage_true -v true
create bool bool_storage_false -v false

create widget_button bool_set_true_btn -l "Set true"
create widget_button bool_set_false_btn -l "Set false"

connect bool_storage_true out bool_storage store
connect bool_storage_false out bool_storage store
connect bool_set_true_btn pressed bool_storage_true retrieve
connect bool_set_false_btn pressed bool_storage_false retrieve

create widget_button bool_retrieve_btn -l "Retrieve bool"
connect bool_retrieve_btn pressed bool_storage retrieve
create print bool_print "bool print:"
connect bool_storage out bool_print in

# string
create string string_storage
create string string_storage_one -v "One"
create string string_storage_two -v "Two"

create widget_button string_set_one_btn -l "Set One"
create widget_button string_set_two_btn -l "Set Two"

connect string_storage_one out string_storage store
connect string_storage_two out string_storage store
connect string_set_one_btn pressed string_storage_one retrieve
connect string_set_two_btn pressed string_storage_two retrieve

create widget_button string_retrieve_btn -l "Retrieve string"
connect string_retrieve_btn pressed string_storage retrieve
create print string_print "string print:"
connect string_storage out string_print in


begin_gui_layout
	layout_begin vbox
		component int_sld
		component int_btn
		component float_sld
		component float_btn
		
		component bool_set_true_btn
		component bool_set_false_btn
		component bool_retrieve_btn
		
		component string_set_one_btn
		component string_set_two_btn
		component string_retrieve_btn
	layout_end
end_gui_layout

