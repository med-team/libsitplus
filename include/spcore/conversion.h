/**
* @file		conversion.h
* @brief	Locale independent string to number conversion functions.
* @author	Cesar Mauri Loba (cesar at crea-si dot com)
*
* -------------------------------------------------------------------------
*
* Copyright:   (C) 2010 Cesar Mauri Loba - CREA Software Systems
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "spcore/libimpexp.h"

namespace spcore {

/**
	@brief Converts a string into a float.
	@return true is conversion successful, or false otherwise.
*/
SPEXPORT_FUNCTION 
bool StrToFloat (const char* str, float* val);

/**
	@brief Converts a string into a double.
	@return true is conversion successful, or false otherwise.
*/
SPEXPORT_FUNCTION 
bool StrToDouble (const char* str, double* val);

/**
	@brief Converts a string into a long double.
	@return true is conversion successful, or false otherwise.
*/
SPEXPORT_FUNCTION 
bool StrToLongDouble (const char* str, long double* val);

/**
	@brief Converts a string into a int.
	@return true is conversion successful, or false otherwise.
*/
SPEXPORT_FUNCTION 
bool StrToInt (const char* str, int* val);

/**
	@brief Converts a string into an unsigned int.
	@return true is conversion successful, or false otherwise.
*/
SPEXPORT_FUNCTION 
bool StrToUint (const char* str, unsigned int* val);

/**
	@brief Converts a string into a long.
	@return true is conversion successful, or false otherwise.
*/
SPEXPORT_FUNCTION 
bool StrToLongInt (const char* str, long* val);

} // namespace spcore
