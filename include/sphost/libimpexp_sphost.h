/**
* @file		libimpexp_sphost.h
* @brief	Preprocessor macros when bulding dynamic library.
* @author	Cesar Mauri Loba (cesar at crea-si dot com)
*
* -------------------------------------------------------------------------
*
* Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include "spcore/libimpexp.h"

#ifdef SPMAKING_DLL_SPHOST
	#define SPIMPEXP_CLASS_HOST SPEXPORT_CLASS
	#define SPIMPEXT_FUNCTION_HOST SPEXPORT_FUNCTION
#elif defined (SPUSING_DLL)
	#define SPIMPEXP_CLASS_HOST SPIMPORT_CLASS
	#define SPIMPEXT_FUNCTION_HOST SPIMPORT_FUNCTION
#else
	#define SPIMPEXP_CLASS_HOST
	#define SPIMPEXT_FUNCTION_HOST
#endif	// SPMAKING_DLL_SPHOST
