/////////////////////////////////////////////////////////////////////////////
// Name:        wiimotesproperties.cpp
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     18/03/2011 13:00:20
// RCS-ID:      
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.    
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes

#include "wwiimotesproperties.h"

////@begin XPM images
#include "icons/none.xpm"
////@end XPM images
#include "icons/balance-board.xpm"
#include "icons/wiimote-small.xpm"

namespace mod_wiimotes {

/*!
 * Wiimotesproperties type definition
 */

IMPLEMENT_DYNAMIC_CLASS( Wiimotesproperties, wxPanel )


/*!
 * Wiimotesproperties event table definition
 */

BEGIN_EVENT_TABLE( Wiimotesproperties, wxPanel )

////@begin Wiimotesproperties event table entries
////@end Wiimotesproperties event table entries

END_EVENT_TABLE()


/*!
 * Wiimotesproperties constructors
 */

Wiimotesproperties::Wiimotesproperties()
{
    Init();
}

Wiimotesproperties::Wiimotesproperties( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style )
{
    Init();
    Create(parent, id, pos, size, style);
}


/*!
 * Wiimotesproperties creator
 */

bool Wiimotesproperties::Create( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style )
{
////@begin Wiimotesproperties creation
    wxPanel::Create( parent, id, pos, size, style );

    CreateControls();
    if (GetSizer())
    {
        GetSizer()->SetSizeHints(this);
    }
    Centre();
////@end Wiimotesproperties creation
    return true;
}


/*!
 * Wiimotesproperties destructor
 */

Wiimotesproperties::~Wiimotesproperties()
{
////@begin Wiimotesproperties destruction
////@end Wiimotesproperties destruction
}


/*!
 * Member initialisation
 */

void Wiimotesproperties::Init()
{
////@begin Wiimotesproperties member initialisation
    m_icon = NULL;
    m_chkConnected = NULL;
    m_chkAcc = NULL;
    m_chkNunchuck = NULL;
    m_chkMotionPlus = NULL;
////@end Wiimotesproperties member initialisation
}


/*!
 * Control creation for Wiimotesproperties
 */

void Wiimotesproperties::CreateControls()
{    
////@begin Wiimotesproperties content construction
    Wiimotesproperties* itemPanel1 = this;

    wxBoxSizer* itemBoxSizer2 = new wxBoxSizer(wxHORIZONTAL);
    itemPanel1->SetSizer(itemBoxSizer2);

    m_icon = new wxStaticBitmap;
    m_icon->Create( itemPanel1, wxID_ICON, itemPanel1->GetBitmapResource(wxT("icons/none.xpm")), wxDefaultPosition, wxSize(64, 42), 0 );
    itemBoxSizer2->Add(m_icon, 0, wxALIGN_TOP|wxALL, 5);

    wxGridSizer* itemGridSizer4 = new wxGridSizer(2, 2, 0, 0);
    itemBoxSizer2->Add(itemGridSizer4, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    m_chkConnected = new wxCheckBox;
    m_chkConnected->Create( itemPanel1, ID_CHK_CONNECTED, _("Connected"), wxDefaultPosition, wxDefaultSize, 0 );
    m_chkConnected->SetValue(false);
    m_chkConnected->Enable(false);
    itemGridSizer4->Add(m_chkConnected, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    m_chkAcc = new wxCheckBox;
    m_chkAcc->Create( itemPanel1, ID_CHK_ACC, _("Accelerometers enabled"), wxDefaultPosition, wxDefaultSize, 0 );
    m_chkAcc->SetValue(false);
    m_chkAcc->Enable(false);
    itemGridSizer4->Add(m_chkAcc, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    m_chkNunchuck = new wxCheckBox;
    m_chkNunchuck->Create( itemPanel1, ID_CHK_NUNCHUCK, _("Nunchuck enabled"), wxDefaultPosition, wxDefaultSize, 0 );
    m_chkNunchuck->SetValue(false);
    m_chkNunchuck->Enable(false);
    itemGridSizer4->Add(m_chkNunchuck, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    m_chkMotionPlus = new wxCheckBox;
    m_chkMotionPlus->Create( itemPanel1, ID_CHK_MOTION_PLUS, _("Motion plus enabled"), wxDefaultPosition, wxDefaultSize, 0 );
    m_chkMotionPlus->SetValue(false);
    m_chkMotionPlus->Enable(false);
    itemGridSizer4->Add(m_chkMotionPlus, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

////@end Wiimotesproperties content construction
}


/*!
 * Should we show tooltips?
 */

bool Wiimotesproperties::ShowToolTips()
{
    return true;
}

/*!
 * Get bitmap resources
 */

wxBitmap Wiimotesproperties::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin Wiimotesproperties bitmap retrieval
    wxUnusedVar(name);
    if (name == _T("icons/none.xpm"))
    {
        wxBitmap bitmap(none);
        return bitmap;
    }
    return wxNullBitmap;
////@end Wiimotesproperties bitmap retrieval
}

/*!
 * Get icon resources
 */

wxIcon Wiimotesproperties::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin Wiimotesproperties icon retrieval
    wxUnusedVar(name);
    return wxNullIcon;
////@end Wiimotesproperties icon retrieval
}

void Wiimotesproperties::Update (const CTypeWiimotesStatus & status, unsigned int n)
{
	if (status.IsConnected(n)) {
		m_chkConnected->SetValue(true);
		if (status.HasBalanceBoard(n)) {
			m_icon->SetBitmap(wxBitmap(balance_board));
		}
		else {
			m_icon->SetBitmap(wxBitmap(wiimote_small));
			m_chkAcc->SetValue(status.IsAccelerometersEnabled(n));
			m_chkMotionPlus->SetValue(status.IsMotionPlusEnabled(n));
			m_chkNunchuck->SetValue(status.IsNunchuckEnabled(n));				
		}
	}
	else {
		m_chkConnected->SetValue(false);
		m_icon->SetBitmap(wxBitmap(none));
		m_chkAcc->SetValue(false);
		m_chkMotionPlus->SetValue(false);
		m_chkNunchuck->SetValue(false);
	}
}

};
