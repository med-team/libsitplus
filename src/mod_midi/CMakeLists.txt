cmake_minimum_required(VERSION 2.8)
project(mod_midi)

#
# Required libraries
# 
find_package ( PortMidi REQUIRED )
if (MSVC)	
	find_file (PortMidi_RUNTIME_REL portmidi.dll HINTS $ENV{PORTMIDIDIR}/lib $ENV{PORTMIDIDIR}/bin)
	find_file (PortMidi_RUNTIME_DBG portmidid.dll HINTS $ENV{PORTMIDIDIR}/lib $ENV{PORTMIDIDIR}/bin)
	
	configure_file("${PortMidi_RUNTIME_DBG}" ${RUNTIME_OUTPUT_DIRECTORY}/Debug COPYONLY)
	configure_file("${PortMidi_RUNTIME_REL}" ${RUNTIME_OUTPUT_DIRECTORY}/Release COPYONLY)
endif(MSVC)

if(WIN32)
	install(FILES ${PortMidi_RUNTIME_DBG} DESTINATION "${LIBRUNTIMEDIR}" CONFIGURATIONS Debug)
	install(FILES ${PortMidi_RUNTIME_REL} DESTINATION "${LIBRUNTIMEDIR}" CONFIGURATIONS Release)
endif(WIN32)

set(mod_midi_SRCS
	${CUSTOM_INCLUDE_PATH}/mod_midi/midi_types.h
	mod_midi.cpp
	midiconfiggui.h
	midiconfiggui.cpp
)

include_directories (${PortMidi_INCLUDE_DIRS})
add_library (mod_midi MODULE ${mod_midi_SRCS})
target_link_libraries(mod_midi spcore)
target_link_libraries(mod_midi ${PortMidi_LIBRARIES})
target_link_libraries(mod_midi ${wxWidgets_LIBRARIES})

install (FILES ${CUSTOM_INCLUDE_PATH}/mod_midi/midi_types.h DESTINATION ${INCLUDEDIR}/${PROJECT_NAME})
INSTALL (TARGETS mod_midi RUNTIME DESTINATION ${LIBRUNTIMEDIR} LIBRARY DESTINATION ${PLUGINDIR})

IF(BUILD_TESTS)
	ADD_SUBDIRECTORY(tests)
ENDIF(BUILD_TESTS)
