/**
* @file		language.h
* @brief	i18n related functions.
* @author	Cesar Mauri Loba (cesar at crea-si dot com)
*
* -------------------------------------------------------------------------
*
* Copyright:   (C) 2010-12 Cesar Mauri Loba - CREA Software Systems
* 
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "config.h"
#include "spcore/libimpexp.h"

#ifdef ENABLE_NLS

	enum spLanguage	{
		// Language identifiers. Based on <wx/intl.h>. 
		// New supported languages to be added as necessary.
		spLANGUAGE_FIRST= 0,
		spLANGUAGE_DEFAULT= spLANGUAGE_FIRST,
		spLANGUAGE_UNKNOWN,		
		spLANGUAGE_BASQUE,
		spLANGUAGE_CATALAN,
		spLANGUAGE_CHINESE,
		spLANGUAGE_ENGLISH,
		spLANGUAGE_FRENCH,
		spLANGUAGE_GALICIAN,
		spLANGUAGE_GERMAN,
		spLANGUAGE_GREEK,
		spLANGUAGE_HEBREW,
		spLANGUAGE_ITALIAN,
		spLANGUAGE_JAPANESE,
		spLANGUAGE_PORTUGUESE,
		spLANGUAGE_RUSSIAN,
		spLANGUAGE_SPANISH,
		spLANGUAGE_SWEDISH,
		spLANGUAGE_TURKISH,
		spLANGUAGE_LAST
	};

	#ifdef ENABLE_WXWIDGETS
		#include <wx/intl.h>
	#endif

	/**
		@brief _(), __(), N_() and N__() macros

		Macro _() is usually defined by some frameworks such as wx 
		which usually expect unicode strings (UTF-16 on Win32 or UTF-32 on 
		Unix), so we define our own translation macro __() for UTF-8 strings.
		The N_ and N__ macros are used to mark string to be extracted but not
		translated when found.
	*/
	#ifndef _
		#define _(STRING)	spGettext(STRING)
	#endif

	#ifndef N_
		#define N_(STRING)	(STRING)
	#endif
	
	#define __(STRING)	spGettext(STRING)
	#define N__(STRING)	(STRING)

	/**
		@brief Attempt to translate a text string into user's native language, by looking
		looking all domains (in order) provided by spBindTextDomain
		@param[in]	msgid	string to translate
		@return If a translation was found, it is converted  to  the  locale's codeset 
		and returned. The resulting string is statically allocated and must not be modified 
		or freed. Otherwise msgid is returned.
	*/
	SPEXPORT_FUNCTION
	const char * spGettext (const char * msgid);

	/**
		@brief Attempt to translate a text string into user's native language, by looking
		a specific domain
		@param[in]	domainname	name of the domain to look
		@param[in]	msgid		string to translate
		@return If a translation was found, it is converted  to  the  locale's codeset 
		and returned. The resulting string is statically allocated and must not be modified 
		or freed. Otherwise msgid is returned.
	*/
	SPEXPORT_FUNCTION
	const char * spDGettext (const char * domainname, const char * msgid);

	/**
		@brief Set locale configuration given a language identifier (spLanguage enum value)
		@param[in]	id	Language identifier.
		@return 0 if ok, -1 if failed.

		Caveats: this funtion should be called once, otherwise the behaviour is undefined.
	*/
	SPEXPORT_FUNCTION
	int spSetLanguage (int id);

	/**
		@brief Get current locale configuration (spLanguage enum value)
		@return language identifier or -1 if language has not been set.

		The returned value is not suitable for storage purposes. See spGetLocaleId instead.
	*/
	SPEXPORT_FUNCTION
	int spGetLanguage ();


	/**
		@brief Checks if a language id is valid.
		@return true is valid, false otherwise.
	*/
	SPEXPORT_FUNCTION
	bool isLanguageValid (int id);

	/**
		@brief set directory containing message catalogs
		@param[in]	domain	Domain name.
		@param[in]	dirname	Directory name.
		@return 0 if OK, -1 if error.

		Before calling this function the language must be set.
	*/
	SPEXPORT_FUNCTION
	int spBindTextDomain(const char* domain, const char* dirname);

	/**
		@brief Obtains the number of available languages.
		@return the number of available languages.
	*/
	SPEXPORT_FUNCTION
	int spGetAvailableLanguages();

	/**
		@brief Obtains the locale identifier given the language id.
		@param[in]	id	Language identifier. 
		@return a pointer to an string whith the locale name or NULL if error.

		The returned locale identifier can be stored and used later.
	*/
	SPEXPORT_FUNCTION
	const char* spGetLocaleId (int id);

	/**
		@brief Obtains the language identifier given a locale identifier.
		@param[in] locale identifier. 
		@return language identifier or -1 if locale not found.
	*/
	SPEXPORT_FUNCTION
	int spResolveLocaleId (const char* locale);

	/**
		@brief Obtains the language name given its language identifier.
		@param[in]	id	Index of the parameter.
		@param[in]	domain	When not NULL chooses the domain to translate "System 
			default" language into a native version.
		@return a pointer to an string whith the language name encoded as utf8 
				or NULL if error.
	*/
	SPEXPORT_FUNCTION
	const char* spGetLanguageNativeName (int id, const char* domain);

#else
	// If NLS disabled, macros do nothing.
	#define __(STRING)	(STRING)
	#define N__(STRING)	(STRING)
#endif