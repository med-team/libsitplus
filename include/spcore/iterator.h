/**
* @file		iterator.h
* @brief	Iterators implementation.
* @author	Cesar Mauri Loba (cesar at crea-si dot com)
*	
* -------------------------------------------------------------------------
*
* Copyright:   (C) 2010-12 Cesar Mauri Loba - CREA Software Systems
* 
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SPCORE_ITERATOR_H
#define SPCORE_ITERATOR_H

#include "spcore/baseobj.h"
#include <vector>
#include <map>

namespace spcore {

/**
	@brief Interface for iterators

	Based on GOF iterator pattern
**/
template<class T>
class IIterator : public IBaseObject {
protected:
	virtual ~IIterator() {}

public:
	virtual void First() = 0;
	virtual void Next() = 0;
	virtual bool IsDone() const = 0;
	virtual T CurrentItem() const = 0;
};

/**
	@brief Iterator implementation for std::vector
**/
template<class T>
class CIteratorVector : public IIterator<T> {
public:
	CIteratorVector(const std::vector<T> & vector) {
		m_vector= &vector;
		m_iterator= vector.begin();
	}

	virtual ~CIteratorVector() { m_vector= NULL; }

	virtual void First() { m_iterator= m_vector->begin(); }
	virtual void Next() { ++m_iterator; }
	virtual bool IsDone() const { return (m_iterator== m_vector->end()); }
	virtual T CurrentItem() const { return (*m_iterator); }

private:
	const std::vector<T>* m_vector;
	typename std::vector<T>::const_iterator m_iterator;
};

/**
	@brief Iterator implementation for std::map
**/
template<class KEY, class VALUE>
class CIteratorMap : public IIterator<VALUE> {
public:
	CIteratorMap(const std::map<KEY,VALUE> & map) {
		m_map= &map;
		m_iterator= map.begin();
	}

	virtual ~CIteratorMap() { m_map= NULL; }

	virtual void First() { m_iterator= m_map->begin(); }
	virtual void Next() { ++m_iterator; }
	virtual bool IsDone() const { return (m_iterator== m_map->end()); }
	virtual VALUE CurrentItem() const { return (m_iterator->second); }

private:
	const std::map<KEY,VALUE>* m_map;
	typename std::map<KEY,VALUE>::const_iterator m_iterator;
};

} // namespace spcore
#endif
