/////////////////////////////////////////////////////////////////////////////
// File:        testcommon.h
//	Common functions to perform testing
// Author:      Cesar Mauri Loba (cesar at crea-si dot com)
// Copyright:   (C) 2010 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////
#ifndef TESTCOMMON_H
#define TESTCOMMON_H

#ifdef WIN32
#include <windows.h>
#define SLEEP(x) Sleep((x)*1000)   /* Windows version */
#ifdef ENABLE_WXWIDGETS
#include <wx/msw/winundef.h>
#endif
#else
#include <unistd.h>
#define SLEEP(x) sleep((x))   /* Linux version */
#endif 


#include <assert.h>
#include <iostream>
#include <stdio.h>

#include "spcore/component.h"
#include "spcore/coreruntime.h"

void DumpInputPin (spcore::IInputPin& ip);

void DumpOutputPin (spcore::IOutputPin& op);

void DumpComponent (spcore::IComponent &c);

void DumpCoreRuntime(spcore::ICoreRuntime* core);

void DumpTypeInstance (const spcore::CTypeAny& t, const char* indent= "");

void ExitErr (const char* msg);

int getch_no_block(void);

unsigned long get_mili_count();

void calls_per_second();

void sleep_milliseconds(unsigned ms);

#endif
