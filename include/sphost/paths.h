/**
* @file		paths.h
* @brief	Common path utilities.
* @author	Cesar Mauri Loba (cesar at crea-si dot com)
*
* -------------------------------------------------------------------------
*
* Copyright:   (C) 2010-12 Cesar Mauri Loba - CREA Software Systems
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SPHOST_PATHS_H
#define SPHOST_PATHS_H
#include "sphost/libimpexp_sphost.h"

// REMOVE 
#include <string>
// REMOVE 


namespace sphost {

/**
	@brief Class for path reporting 
*/
class SPIMPEXP_CLASS_HOST Paths {
public:
	/**
		@brief Return full path of the command of the calling process
	*/
	static const std::string GetExecPath();

	/**
		@brief Given the full path of the command of the calling process
		tries to guess the base path (installation path)
		@param[in]  exePath full path of the command of the calling process
		@param[out] development environment? (i.e. Debug or Release found on the path)

		@remarks Examples:
		c:\program files\appname\bin\exec.exe -> c:\program files\appname
		c:\program files\appname\bin\Debug\exec.exe -> c:\program files\appname
		c:\program files\appname\bin\Release\exec.exe -> c:\program files\appname
		/usr/bin/appname -> /usr
		/usr/local/bin/appname -> /usr/local
		/home/user/devel/appname/bin/Debug/appname -> /home/user/devel/appname
		/home/user/devel/appname/bin/Release/appname -> /home/user/devel/appname
	*/
	static const std::string GetBasePathFromExec(const std::string& fullPath, bool& devel);

	/**
		@brief Get the name of the directory for a full path
	*/
	static const std::string GetDirName(const std::string& fullPath);

	/**
		@brief Return the directory for the user-dependent application data 
		and configuration files
		The returned value is:
		- Linux: the HOME directory
		- Windows: the contents of the APPDATA usually something like
			C:\Users\username\AppData\appname
		  or like
		    C:\Users\cesar\AppData\Roaming
	*/
	static const std::string GetUserDataDir();	

private:
	Paths() {}
};

} // namespace sphost
#endif
