/////////////////////////////////////////////////////////////////////////////
// File:        mod_score_player.cpp
// Author:      Cesar Mauri Loba (cesar at crea-si dot com)
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////

#include "spcore/libimpexp.h"
#include "spcore/module.h"
#include "scoreplayer.h"
#include "instrselect.h"

using namespace spcore;

namespace mod_score_player {

/* ******************************************************************************
	score player module
*/
class ScorePlayerModule : public spcore::CModuleAdapter {
public:
	ScorePlayerModule() {
		
		RegisterComponentFactory(SmartPtr<spcore::IComponentFactory>(new ScorePlayerFactory(), false));
		RegisterComponentFactory(SmartPtr<spcore::IComponentFactory>(new InstrumentSelectorFactory(), false));
	}
	virtual const char * GetName() const { return "mod_score_player"; }
};

static spcore::IModule* g_module= NULL;
SPEXPORT_FUNCTION spcore::IModule* module_create_instance()
{
	if (g_module== NULL) g_module= new ScorePlayerModule();
	return g_module;
}

};