/////////////////////////////////////////////////////////////////////////////
// Name:        wcameraconfiguration.cpp
// Author:      Cesar Mauri Loba (cesar at crea-si dot com)
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////

#include "wcameraconfiguration.h"
#include "wcamerapanel.h"
#include "mod_camera/iplimagetype.h"

////@begin includes
////@end includes

#include <wx/sizer.h>
#include <wx/statbox.h>
#include <wx/button.h>
#include <wx/icon.h>
#include <wx/msgdlg.h>
#include <stdlib.h>

////@begin XPM images
////@end XPM images
namespace mod_camera {

using namespace spcore;

/*!
 * CCameraConfiguration type definition
 */

IMPLEMENT_DYNAMIC_CLASS( CCameraConfiguration, wxPanel )


/*!
 * CCameraConfiguration event table definition
 */

BEGIN_EVENT_TABLE( CCameraConfiguration, wxPanel )

////@begin CCameraConfiguration event table entries
    EVT_CHOICE( ID_CHOICE_SELECTED_CAMERA, CCameraConfiguration::OnChoiceSelectedCameraSelected )

    EVT_CHOICE( ID_CHOICE_FORMAT, CCameraConfiguration::OnChoiceFormatSelected )

    EVT_CHOICE( ID_CHOICE_FPS, CCameraConfiguration::OnChoiceFpsSelected )

    EVT_BUTTON( ID_BUTTON_DRIVER_SETTINGS, CCameraConfiguration::OnButtonDriverSettingsClick )

    EVT_CHECKBOX( ID_CHECKBOX_MIRROR_IMAGE, CCameraConfiguration::OnCheckboxMirrorImageClick )

    EVT_BUTTON( ID_BUTTON_CLOSE_CCONFIG, CCameraConfiguration::OnButtonCloseCconfigClick )

////@end CCameraConfiguration event table entries

END_EVENT_TABLE()


/*!
 * CCameraConfiguration constructors
 */

CCameraConfiguration::CCameraConfiguration()
{
    Init();
}

CCameraConfiguration::CCameraConfiguration( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name )
{
    Init();
    Create(parent, id, pos, size, style, name);
}


/*!
 * CCameraConfiguration creator
 */

bool CCameraConfiguration::Create( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name )
{
////@begin CCameraConfiguration creation
    SetExtraStyle(wxWS_EX_BLOCK_EVENTS);
    wxPanel::Create( parent, id, pos, size, style, name );

    CreateControls();
    if (GetSizer())
    {
        GetSizer()->SetSizeHints(this);
    }
    Centre();
////@end CCameraConfiguration creation
    return true;
}


/*!
 * CCameraConfiguration destructor
 */

CCameraConfiguration::~CCameraConfiguration()
{
	m_cameraConfig->UnregisterListener (*this);
////@begin CCameraConfiguration destruction
////@end CCameraConfiguration destruction
}


/*!
 * Member initialisation
 */

void CCameraConfiguration::Init()
{
////@begin CCameraConfiguration member initialisation
    m_camPanel = NULL;
    m_choSelectedCamera = NULL;
    m_choFormat = NULL;
    m_choFPS = NULL;
    m_chkMirrorImage = NULL;
////@end CCameraConfiguration member initialisation
}


/*!
 * Control creation for CCameraConfiguration
 */

void CCameraConfiguration::CreateControls()
{
////@begin CCameraConfiguration content construction
    CCameraConfiguration* itemPanel1 = this;

    wxBoxSizer* itemBoxSizer2 = new wxBoxSizer(wxVERTICAL);
    itemPanel1->SetSizer(itemBoxSizer2);

    wxStaticBox* itemStaticBoxSizer3Static = new wxStaticBox(itemPanel1, wxID_ANY, wxEmptyString);
    wxStaticBoxSizer* itemStaticBoxSizer3 = new wxStaticBoxSizer(itemStaticBoxSizer3Static, wxVERTICAL);
    itemBoxSizer2->Add(itemStaticBoxSizer3, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5);

    m_camPanel = new CameraPanel;
    m_camPanel->Create( itemPanel1, ID_PANEL_CAMERA, wxDefaultPosition, wxSize(320, 240), wxSUNKEN_BORDER|wxTAB_TRAVERSAL );
    itemStaticBoxSizer3->Add(m_camPanel, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5);

    wxStaticBox* itemStaticBoxSizer5Static = new wxStaticBox(itemPanel1, wxID_ANY, _("Camera properties"));
    wxStaticBoxSizer* itemStaticBoxSizer5 = new wxStaticBoxSizer(itemStaticBoxSizer5Static, wxVERTICAL);
    itemStaticBoxSizer3->Add(itemStaticBoxSizer5, 0, wxGROW|wxALL, 5);

    wxFlexGridSizer* itemFlexGridSizer6 = new wxFlexGridSizer(0, 2, 0, 0);
    itemFlexGridSizer6->AddGrowableCol(1);
    itemStaticBoxSizer5->Add(itemFlexGridSizer6, 0, wxGROW|wxALL, 5);

    wxStaticText* itemStaticText7 = new wxStaticText;
    itemStaticText7->Create( itemPanel1, wxID_STATIC, _("Selected camera:"), wxDefaultPosition, wxDefaultSize, 0 );
    itemFlexGridSizer6->Add(itemStaticText7, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxArrayString m_choSelectedCameraStrings;
    m_choSelectedCamera = new wxChoice;
    m_choSelectedCamera->Create( itemPanel1, ID_CHOICE_SELECTED_CAMERA, wxDefaultPosition, wxDefaultSize, m_choSelectedCameraStrings, 0 );
    itemFlexGridSizer6->Add(m_choSelectedCamera, 0, wxGROW|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxStaticText* itemStaticText9 = new wxStaticText;
    itemStaticText9->Create( itemPanel1, wxID_STATIC, _("Format:"), wxDefaultPosition, wxDefaultSize, 0 );
    itemFlexGridSizer6->Add(itemStaticText9, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxArrayString m_choFormatStrings;
    m_choFormat = new wxChoice;
    m_choFormat->Create( itemPanel1, ID_CHOICE_FORMAT, wxDefaultPosition, wxDefaultSize, m_choFormatStrings, 0 );
    itemFlexGridSizer6->Add(m_choFormat, 0, wxGROW|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxStaticText* itemStaticText11 = new wxStaticText;
    itemStaticText11->Create( itemPanel1, wxID_STATIC, _("Capture speed:"), wxDefaultPosition, wxDefaultSize, 0 );
    itemFlexGridSizer6->Add(itemStaticText11, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxArrayString m_choFPSStrings;
    m_choFPS = new wxChoice;
    m_choFPS->Create( itemPanel1, ID_CHOICE_FPS, wxDefaultPosition, wxDefaultSize, m_choFPSStrings, 0 );
    itemFlexGridSizer6->Add(m_choFPS, 0, wxGROW|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxStaticText* itemStaticText13 = new wxStaticText;
    itemStaticText13->Create( itemPanel1, wxID_STATIC, _("Driver settings:"), wxDefaultPosition, wxDefaultSize, 0 );
    itemFlexGridSizer6->Add(itemStaticText13, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxButton* itemButton14 = new wxButton;
    itemButton14->Create( itemPanel1, ID_BUTTON_DRIVER_SETTINGS, _("Open driver settings"), wxDefaultPosition, wxDefaultSize, 0 );
    itemFlexGridSizer6->Add(itemButton14, 0, wxGROW|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxStaticText* itemStaticText15 = new wxStaticText;
    itemStaticText15->Create( itemPanel1, wxID_STATIC, _("Image mirror:"), wxDefaultPosition, wxDefaultSize, 0 );
    itemFlexGridSizer6->Add(itemStaticText15, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    m_chkMirrorImage = new wxCheckBox;
    m_chkMirrorImage->Create( itemPanel1, ID_CHECKBOX_MIRROR_IMAGE, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
    m_chkMirrorImage->SetValue(false);
    itemFlexGridSizer6->Add(m_chkMirrorImage, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxButton* itemButton17 = new wxButton;
    itemButton17->Create( itemPanel1, ID_BUTTON_CLOSE_CCONFIG, _("Close"), wxDefaultPosition, wxDefaultSize, 0 );
    itemBoxSizer2->Add(itemButton17, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5);

////@end CCameraConfiguration content construction

	m_cameraConfig= smartptr_dynamic_cast<CameraConfig,IComponent>(
		getSpCoreRuntime()->CreateComponent("camera_config", "camera_config", 0, NULL));

	if (!m_cameraConfig.get()) {
		getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_FATAL, "cannot create camera_config module", "mod_camera");
	}
	m_cameraConfig->RegisterListener (*this);

	PopulateControls();

}

IInputPin* CCameraConfiguration::GetCamerasPin()
{
	IInputPin* iPin= IComponent::FindInputPin(*m_cameraConfig, "cameras");
	if (!iPin) {
		getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, "cameras pin not found", "mod_camera");
		return NULL;
	}

	assert (iPin->GetProperties() & IInputPin::ALLOW_READ);
	return iPin;
}

IInputPin* CCameraConfiguration::GetSelectedCameraPin()
{
	IInputPin* iPin= IComponent::FindInputPin(*m_cameraConfig, "selected_camera");
	if (!iPin) {
		getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, "selected_camera pin not found", "mod_camera");
		return NULL;
	}

	assert ((iPin->GetProperties() & IInputPin::ALLOW_READ) && (iPin->GetProperties() & IInputPin::ALLOW_WRITE));
	return iPin;
}

IInputPin* CCameraConfiguration::GetCaptureParametersPin()
{
	IInputPin* iPin= IComponent::FindInputPin(*m_cameraConfig, "capture_parameters");
	if (!iPin) {
		getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, "capture_parameters pin not found", "mod_camera");
		return NULL;
	}

	assert ((iPin->GetProperties() & IInputPin::ALLOW_READ) && (iPin->GetProperties() & IInputPin::ALLOW_WRITE));
	return iPin;
}

IInputPin* CCameraConfiguration::GetMirrorEffectPin()
{
	IInputPin* iPin= IComponent::FindInputPin(*m_cameraConfig, "mirror_image");
	if (!iPin) {
		getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, "mirror_image pin not found", "mod_camera");
		return NULL;
	}
	assert ((iPin->GetProperties() & IInputPin::ALLOW_READ) && (iPin->GetProperties() & IInputPin::ALLOW_WRITE));

	return iPin;
}

IInputPin* CCameraConfiguration::GetSettingsDialogPin()
{
	IInputPin* iPin= IComponent::FindInputPin(*m_cameraConfig, "settings_dialog");
	if (!iPin) {
		getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, "settings_dialog pin not found", "mod_camera");
		return NULL;
	}
	assert (iPin->GetProperties() & IInputPin::ALLOW_WRITE);

	return iPin;
}



void CCameraConfiguration::PopulateControls()
{
	//
	// cameras
	//
	IInputPin* iPin= GetCamerasPin();
	if (!iPin) return;
	SmartPtr<const CTypeAny> cameras= iPin->Read();
	if (cameras.get()== NULL) {
		getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, "error reading cameras pin", "mod_camera");
		return;
	}

	SmartPtr<IIterator<CTypeAny*> > camerasIt= cameras->QueryChildren();
	if (camerasIt.get()== NULL) {
		getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, "error reading cameras iterator", "mod_camera");
		return;
	}

	m_choSelectedCamera->Clear();
	for(camerasIt->First(); !camerasIt->IsDone(); camerasIt->Next()) {
		SmartPtr<CTypeString> str= sptype_dynamic_cast<CTypeString>(SmartPtr<CTypeAny>(camerasIt->CurrentItem()));
		if (!str.get()) {
			getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, "type mismatch enumerating cameras", "mod_camera");
			return;
		}
		m_choSelectedCamera->Append (wxString(str->getValue(), wxConvUTF8));
	}

	if (m_choSelectedCamera->GetCount()== 0) {
		wxMessageDialog dlg(this, _("Not detected any camera"), _("Camera module"), wxICON_EXCLAMATION | wxOK);
		dlg.ShowModal();
		return;
	}

	//
	// selected camera
	//
	iPin= GetSelectedCameraPin();
	if (!iPin) return;
	SmartPtr<const CTypeInt> selected_camera= sptype_dynamic_cast<const CTypeInt>(SmartPtr<const CTypeAny>(iPin->Read()));
	if (selected_camera.get()== NULL) {
		getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, "error reading selected_camera pin", "mod_camera");
		return;
	}

	if (selected_camera->getValue()>= 0) {
		if (static_cast<unsigned int>(selected_camera->getValue())>= m_choSelectedCamera->GetCount())
			getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, "wrong selected_camera value", "mod_camera");
		else
			m_choSelectedCamera->SetSelection(selected_camera->getValue());
	}

	//
	// capture parameters
	//
	iPin= GetCaptureParametersPin();
	if (!iPin) return;
	SmartPtr<const CTypeAny> capture_parameters= iPin->Read();
	if (capture_parameters.get()== NULL) {
		getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, "error reading capture_parameters pin", "mod_camera");
		return;
	}

	SmartPtr<IIterator<CTypeAny*> > capture_parametersIt= capture_parameters->QueryChildren();
	if (capture_parametersIt.get()== NULL) {
		getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, "error reading capture_parameters iterator", "mod_camera");
		return;
	}

	int width= -1;
	if (!capture_parametersIt->IsDone()) {
		SmartPtr<const CTypeInt> sptr_width= sptype_dynamic_cast<const CTypeInt>(SmartPtr<const CTypeAny>(capture_parametersIt->CurrentItem()));
		if (sptr_width.get()== NULL) {
			getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, "error reading capture_parameters width", "mod_camera");
			return;
		}
		width= sptr_width->getValue();
	}
	else {
		getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, "unexpected capture_parameters message format (1)", "mod_camera");
		return;
	}

	int height= -1;
	capture_parametersIt->Next();
	if (!capture_parametersIt->IsDone()) {
		SmartPtr<const CTypeInt> sptr_height= sptype_dynamic_cast<const CTypeInt>(SmartPtr<const CTypeAny>(capture_parametersIt->CurrentItem()));
		if (sptr_height.get()== NULL) {
			getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, "error reading capture_parameters height", "mod_camera");
			return;
		}
		height= sptr_height->getValue();
	}
	else {
		getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, "unexpected capture_parameters message format (2)", "mod_camera");
		return;
	}

	assert (width> 0 && height> 0);

	// set up pre-defined values for image size
	// TODO: obtain the actual parameters from the camera
	m_choFormat->Clear();
	m_choFormat->Append (_T("160x120"));
	m_choFormat->Append (_T("320x240"));
	m_choFormat->Append (_T("640x480"));

	// choose the closest image size
	int max_diff= abs(640*480 - width*height);
	int choice= 2;

	if (abs(320*240 - width*height)< max_diff) {
		max_diff= abs(320*240 - width*height);
		choice= 1;
	}

	if (abs(160*120 - width*height)< max_diff) {
		choice= 0;
	}
	m_choFormat->SetSelection(choice);

	// frame rate
	int fps= -1;
	capture_parametersIt->Next();
	if (!capture_parametersIt->IsDone()) {
		SmartPtr<const CTypeInt> sptr_fps= sptype_dynamic_cast<const CTypeInt>(SmartPtr<const CTypeAny>(capture_parametersIt->CurrentItem()));
		if (sptr_fps.get()== NULL) {
			getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, "error reading capture_parameters fps", "mod_camera");
			return;
		}
		fps= sptr_fps->getValue();
	}
	else {
		getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, "unexpected capture_parameters message format (3)", "mod_camera");
		return;
	}
	assert (fps> 0);

	// set up pre-defined values for frame rate
	// TODO: obtain the actual parameters from the camera
	m_choFPS->Clear();
	m_choFPS->Append (_T("5"));
	m_choFPS->Append (_T("10"));
	m_choFPS->Append (_T("15"));
	m_choFPS->Append (_T("20"));
	m_choFPS->Append (_T("25"));
	m_choFPS->Append (_T("30"));

	// select closest value
	max_diff= abs(30 - fps);
	choice= 5;

	if (abs(25 - fps)< max_diff) {
		max_diff= abs(25 - fps);
		choice= 4;
	}

	if (abs(20 - fps)< max_diff) {
		max_diff= abs(20 - fps);
		choice= 3;
	}

	if (abs(15 - fps)< max_diff) {
		max_diff= abs(15 - fps);
		choice= 2;
	}

	if (abs(10 - fps)< max_diff) {
		max_diff= abs(10 - fps);
		choice= 1;
	}

	if (abs(5 - fps)< max_diff) {
		max_diff= abs(5 - fps);
		choice= 0;
	}

	m_choFPS->SetSelection (choice);

	// mirror effect
	//
	iPin= GetMirrorEffectPin();
	if (!iPin) return;
	SmartPtr<const CTypeBool> mirror_image= sptype_dynamic_cast<const CTypeBool>(SmartPtr<const CTypeAny>(iPin->Read()));
	if (mirror_image.get()== NULL) {
		getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, "error reading mirror_image pin", "mod_camera");
		return;
	}

	m_chkMirrorImage->SetValue(mirror_image->getValue());
}

/*!
 * wxEVT_COMMAND_CHOICE_SELECTED event handler for ID_CHOICE_SELECTED_CAMERA
 */

void CCameraConfiguration::OnChoiceSelectedCameraSelected( wxCommandEvent& event )
{
	IInputPin* iPin= GetSelectedCameraPin();
	if (!iPin) return;

	SmartPtr<CTypeInt> value= CTypeInt::CreateInstance();
	value->setValue(event.GetSelection());
	iPin->Send (value);

    event.Skip(false);
}

/*!
 * wxEVT_COMMAND_CHOICE_SELECTED event handler for ID_CHOICE_FORMAT
 */

void CCameraConfiguration::OnChoiceFormatSelected( wxCommandEvent& event )
{
	IInputPin* iPin= GetCaptureParametersPin();
	if (!iPin) return;
	SmartPtr<const CTypeAny> capture_parameters= iPin->Read();
	if (capture_parameters.get()== NULL) {
		getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, "error reading capture_parameters pin", "mod_camera");
		return;
	}

	SmartPtr<IIterator<CTypeAny*> > capture_parametersIt= capture_parameters->QueryChildren();
	if (capture_parametersIt.get()== NULL) {
		getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, "error reading capture_parameters iterator", "mod_camera");
		return;
	}

	int width= -1, height= -1;
	switch (event.GetSelection()) {
	case 0: width= 160; height= 120; break;
	case 1: width= 320; height= 240; break;
	case 2: width= 640; height= 480; break;
	}

	if (capture_parametersIt->IsDone()) return;

	// TODO: add const iterators which must prevent doing things like this
	sptype_dynamic_cast<CTypeInt>(SmartPtr<CTypeAny>(capture_parametersIt->CurrentItem()))->setValue(width);

	capture_parametersIt->Next();

	if (capture_parametersIt->IsDone()) return;

	sptype_dynamic_cast<CTypeInt>(SmartPtr<CTypeAny>(capture_parametersIt->CurrentItem()))->setValue(height);

	iPin->Send (capture_parameters);

	event.Skip(false);
}

/*!
 * wxEVT_COMMAND_CHOICE_SELECTED event handler for ID_CHOICE_FPS
 */

void CCameraConfiguration::OnChoiceFpsSelected( wxCommandEvent& event )
{
	IInputPin* iPin= GetCaptureParametersPin();
	if (!iPin) return;
	SmartPtr<const CTypeAny> capture_parameters= iPin->Read();
	if (capture_parameters.get()== NULL) {
		getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, "error reading capture_parameters pin", "mod_camera");
		return;
	}

	SmartPtr<IIterator<CTypeAny*> > capture_parametersIt= capture_parameters->QueryChildren();
	if (capture_parametersIt.get()== NULL) {
		getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, "error reading capture_parameters iterator", "mod_camera");
		return;
	}

	int fps= event.GetSelection() * 5 + 5;

	if (capture_parametersIt->IsDone()) return;
	capture_parametersIt->Next();
	if (capture_parametersIt->IsDone()) return;
	capture_parametersIt->Next();
	if (capture_parametersIt->IsDone()) return;

	// TODO: add const iterators which must prevent doing things like this
	sptype_dynamic_cast<CTypeInt>(SmartPtr<CTypeAny>(capture_parametersIt->CurrentItem()))->setValue(fps);

	iPin->Send (capture_parameters);

    event.Skip(false);
}

/*!
 * wxEVT_COMMAND_CHECKBOX_CLICKED event handler for ID_CHECKBOX_MIRROR_IMAGE
 */

void CCameraConfiguration::OnCheckboxMirrorImageClick( wxCommandEvent& event )
{
	IInputPin* iPin= GetMirrorEffectPin();
	if (!iPin) return;

	SmartPtr<CTypeBool> value= CTypeBool::CreateInstance();

	value->setValue(event.IsChecked());

	iPin->Send(value);

    event.Skip(false);
}


/*!
 * wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BUTTON_PROPERTIES
 */

void CCameraConfiguration::OnButtonDriverSettingsClick( wxCommandEvent& event )
{
	IInputPin * iPin= GetSettingsDialogPin();
	if (!iPin) return;

	SmartPtr<CTypeBool> value= CTypeBool::CreateInstance();
	iPin->Send(value);

    event.Skip(false);
}


void CCameraConfiguration::CameraCaptureCallback (SmartPtr<const CTypeIplImage> img)
{
	m_camPanel->DrawCam(img->getImage());
}



/*!
 * wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BUTTON_CLOSE_CCONFIG
 */

void CCameraConfiguration::OnButtonCloseCconfigClick( wxCommandEvent& event )
{
	wxWindow* parent= GetParent();

	assert (parent);
	if (parent) parent->Close();

    event.Skip(false);
}


/*!
 * Should we show tooltips?
 */

bool CCameraConfiguration::ShowToolTips()
{
    return true;
}

/*!
 * Get bitmap resources
 */

wxBitmap CCameraConfiguration::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin CCameraConfiguration bitmap retrieval
    wxUnusedVar(name);
    return wxNullBitmap;
////@end CCameraConfiguration bitmap retrieval
}

/*!
 * Get icon resources
 */

wxIcon CCameraConfiguration::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin CCameraConfiguration icon retrieval
    wxUnusedVar(name);
    return wxNullIcon;
////@end CCameraConfiguration icon retrieval
}




}
