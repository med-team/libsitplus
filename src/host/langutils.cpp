/////////////////////////////////////////////////////////////////////////////
// Name:        langutils.cpp
// Author:      Cesar Mauri Loba
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////

#include "sphost/langutils.h"

#ifdef ENABLE_NLS

#include "sphost/configutils.h"

#include "spcore/coreruntime.h"
#include "spcore/language.h"

#include <wx/choicdlg.h>
#include <vector>

using namespace spcore;
using namespace std;

static std::vector<int> g_available_languages;

namespace sphost {

bool AddAvailableLanguage(int lang)
{	
	if (!isLanguageValid(lang)) return false;

	g_available_languages.push_back(lang);
	
	return true;
}

int LanguageSelectionDialog(int previous)
{
	// Let user choose
	int selection= -1;
	wxArrayString languages;
	std::vector<int>::size_type numLangs= g_available_languages.size();
	for (std::vector<int>::size_type i= 0; i< numLangs; ++i) {
		languages.Add(wxString(::spGetLanguageNativeName(g_available_languages[i], NULL), wxConvUTF8));
		if (previous== g_available_languages[i]) selection= i;
	}

	wxSingleChoiceDialog langDlg(NULL, _("Language"), _("Language"), languages);
	if (selection!= -1) langDlg.SetSelection(selection);
	if (langDlg.ShowModal()== wxID_OK)
		return g_available_languages[langDlg.GetSelection()];		
	
	return -1;
}

int LoadLanguageFromConfiguration(const char* fname)
{
	SmartPtr<spcore::IConfiguration> cfg= LoadConfiguration(fname);
	if (!cfg.get()) return -1;

	const char* langPtr= NULL;
	if (!cfg->ReadString("language", &langPtr)) return -1;

	int id= spResolveLocaleId (langPtr);
	if (id< 0) return -2;

	return id;
}

int SaveLanguageToConfiguration(int lang, const char* fname)
{
	const char* locale= spGetLocaleId (lang);

	if (!locale) return -2;

	SmartPtr<IConfiguration> cfg= getSpCoreRuntime()->GetConfiguration();
	if (!cfg.get()) return -1;

	if (!cfg->WriteString("language", locale)) return -1;
	
	return (SaveConfiguration(*cfg, fname)? 0 : -1);
}

};

#endif