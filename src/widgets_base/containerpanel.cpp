/////////////////////////////////////////////////////////////////////////////
// Name:        wxcontainerpanel.cpp
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     10/05/2011 21:00:30
// RCS-ID:      
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.   
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes

#include "widgets_base/containerpanel.h"

////@begin XPM images
////@end XPM images

namespace widgets_base {

/*
 * ContainerPanel type definition
 */

IMPLEMENT_DYNAMIC_CLASS( ContainerPanel, wxPanel )


/*
 * ContainerPanel event table definition
 */

BEGIN_EVENT_TABLE( ContainerPanel, wxPanel )

////@begin ContainerPanel event table entries
    EVT_SIZE( ContainerPanel::OnSize )

////@end ContainerPanel event table entries

END_EVENT_TABLE()


/*
 * ContainerPanel constructors
 */

ContainerPanel::ContainerPanel()
{
    Init();
}

ContainerPanel::ContainerPanel( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name )
{
    Init();
    Create(parent, id, pos, size, style, name);
}


/*
 * wxContainerPanel creator
 */

bool ContainerPanel::Create( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name  )
{
////@begin ContainerPanel creation
    wxPanel::Create( parent, id, pos, size, style, name );

    CreateControls();
    Centre();
////@end ContainerPanel creation
    return true;
}


/*
 * ContainerPanel destructor
 */

ContainerPanel::~ContainerPanel()
{
////@begin ContainerPanel destruction
////@end ContainerPanel destruction
}


/*
 * Member initialisation
 */

void ContainerPanel::Init()
{
////@begin ContainerPanel member initialisation
////@end ContainerPanel member initialisation
}


/*
 * Control creation for wxContainerPanel
 */

void ContainerPanel::CreateControls()
{    
////@begin ContainerPanel content construction
////@end ContainerPanel content construction
}


/*
 * Should we show tooltips?
 */

bool ContainerPanel::ShowToolTips()
{
    return true;
}

/*
 * Get bitmap resources
 */

wxBitmap ContainerPanel::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin ContainerPanel bitmap retrieval
    wxUnusedVar(name);
    return wxNullBitmap;
////@end ContainerPanel bitmap retrieval
}

/*
 * Get icon resources
 */

wxIcon ContainerPanel::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin ContainerPanel icon retrieval
    wxUnusedVar(name);
    return wxNullIcon;
////@end ContainerPanel icon retrieval
}


/*
 * wxEVT_SIZE event handler for ID_WXCONTAINERPANEL
 */

void ContainerPanel::OnSize( wxSizeEvent& event )
{
	if (event.GetSize().GetX() || event.GetSize().GetY())
		wxPanel::OnSize(event);
	else {
	//	wxSize sbef= GetSize();
		Layout();
		Fit();
		
		//wxPanel::OnSize(event);
		//InvalidateBestSize();
	//	wxSize safter= GetSize();

	//	if (sbef!= safter) {
			wxSizeEvent evt;
			wxPostEvent (GetParent(), evt);
	//	}
	//	wxPostEvent(GetParent(), event);
		event.Skip(false);
	}
}

/*
Works on windows, Infinite loop on linux

wxPanel::OnSize(event);

	wxPostEvent(GetParent(), event);
	event.Skip(false); 
	*/

}